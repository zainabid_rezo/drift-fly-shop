<?php  
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
if (!isset($_REQUEST["date"])) $_REQUEST["date"] = date("Y-m-d");
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
?>
<?php
$date = $_REQUEST["date"];
$month = $cMonth = $_REQUEST["month"];
$year = $cYear = $_REQUEST["year"];
?>
<?php
$weekdays = '7';//date("t",mktime(0,0,0,$cMonth,1,$cYear));
?>
<?php

function x_week_range($date) {
    $ts = strtotime($date);
    $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
    return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next saturday', $start)), date('W', $ts));
}

$list = list($start_date, $end_date) = x_week_range($_REQUEST["date"]);
$first_day =  $list['0'];
$last_day =  $list['1'];
$week_num =  $list['2'];

?>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
?>
<?php
function create_html_color($shades = "light") {
	if($shades=="light"){
	$validCharacters = "89ABCDEF";
	}elseif($shades=="dark"){
	$validCharacters = "012345678";
	}
    $validCharNumber = strlen($validCharacters);
    $result = ""; 
    for ($i = 0; $i < 6; $i++) {
        $index = mt_rand(0, $validCharNumber - 1);
        $result .= $validCharacters[$index];
    }
    return "#".$result;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Rezo :: <?=SITE_NAME?></title>
<script language="javascript" src="rezo-calendar-save.js"></script>

<style type="text/css">
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	background-color:#666666; /*#cccccc;*/
}

	#dhtmlgoodies_dragDropContainer{	/* Main container for this script */
		width:430px;
		/*height:400px;*/
		border:0px solid #317082;
		/*background-color:#0FF;*/
	}
	#dhtmlgoodies_dragDropContainer ul{	/* General rules for all <ul> */
		margin-top:0px;
		-moz-user-select:none;
		
		float:left; /*abrar*/
		margin-left:0px;
		margin-bottom:0px;
		padding:1px 1px 1px 0;
		
		float:left; /*abrar*/
		
	}


	#dhtmlgoodies_dragDropContainer li,#dragContent li,li#indicateDestination{	/* Movable items, i.e. <LI> */
		list-style-type:none;
		height:50px;
		background-color:#ccc;
		border:1px solid #aaa; /*abrarx*/
		padding:2px;
		margin-bottom:2px;
		font-size:0.9em;
		z-index:5000;
		
		float:left; /*abrar*/
	}

	#dragContent li{
		background-color:#eee;
		border:1px solid #f00; /*abrarx*/
	}

	#dhtmlgoodies_dragDropContainer li:hover{
		background-color:#ff0;
		border:1px solid #aaa;
	}
	li#indicateDestination{	/* Box indicating where content will be dropped - i.e. the one you use if you don't use arrow */
		border:1px solid #317082; /*abrarx*/
		background-color:#FFF;
	}

	#dhtmlgoodies_dragDropContainer .mouseover{	/* Mouse over effect DIV box in right column */
		background-color:#E2EBED;
	}

	/* Start main container CSS */

	div#dhtmlgoodies_mainContainer, #dates{	/* Right column DIV */
		width:430px;
		float:left;
		padding:3px;
		/*background-color:#F0F;*/
	}
	
	#dhtmlgoodies_mainContainer div, #dates div{	/* Parent <div> of small boxes */
		float:left;
		margin-right:1px;
		margin-bottom:1px;
		margin-top:0px;
		border:0px solid #999;
		background-image:url('rezo.png');
		background-repeat:no-repeat;
		/* CSS HACK */
		width: 60px;	/* IE 5.x */
		width/* */:/**/60px;	/* Other browsers */
		width: /**/60px;
	}
	#dates div{
		text-align:center;
		height: 38px;
	}
	#dhtmlgoodies_mainContainer div ul{
		margin-left:1px;
	}

	#dhtmlgoodies_mainContainer ul{	/* Small box in right column ,i.e <ul> */
		width:60px;
		height:60px;
		border:0px;
		margin-bottom:0px;
		overflow:visible;		

	}

	#dragContent{	/* Drag container */
		position:absolute;
		height:50px;
		display:none;
		margin:0px;
		padding:0px;
		z-index:2000;
	}

	#dragDropIndicator{	/* DIV for the small arrow */
		position:absolute;
		width:7px;
		height:10px;
		display:none;
		z-index:1000;
		margin:0px;
		padding:0px;
	}
	</style>
<script type="text/javascript">
	/* VARIABLES YOU COULD MODIFY */
	var boxSizeArray = [
	<?php 
	$result= mysql_query("SELECT * FROM ".GUIDES." ORDER BY lastname ASC") or die(__LINE__.mysql_error()); //
	$num_of_guides = mysql_num_rows($result);
	for($i=1; $i<$weekdays*$num_of_guides; $i++){echo "1,1,";}echo "1,1";
	?>
	];	// Array indicating how many items there is rooom for in the right column ULs

	var verticalSpaceBetweenListItems = 0;	// Pixels space between one <li> and next
											// Same value or higher as margin bottom in CSS for #dhtmlgoodies_dragDropContainer ul li,#dragContent li

	var indicateDestionationByUseOfArrow = true;	// Display arrow to indicate where object will be dropped(false = use rectangle)

	var cloneSourceItems = false;	// Items picked from main container will be cloned(i.e. "copy" instead of "cut").
	var cloneAllowDuplicates = false;	// Allow multiple instances of an item inside a small box(example: drag Student 1 to team A twice

	/* END VARIABLES YOU COULD MODIFY */

	var dragDropTopContainer = false;
	var dragTimer = -1;
	var dragContentObj = false;
	var contentToBeDragged = false;	// Reference to dragged <li>
	var contentToBeDragged_src = false;	// Reference to parent of <li> before drag started
	var contentToBeDragged_next = false; 	// Reference to next sibling of <li> to be dragged
	var destinationObj = false;	// Reference to <UL> or <LI> where element is dropped.
	var dragDropIndicator = false;	// Reference to small arrow indicating where items will be dropped
	var ulPositionArray = new Array();
	var mouseoverObj = false;	// Reference to highlighted DIV

	var MSIE = navigator.userAgent.indexOf('MSIE')>=0?true:false;
	var navigatorVersion = navigator.appVersion.replace(/.*?MSIE (\d\.\d).*/g,'$1')/1;

	var arrow_offsetX = -5;	// Offset X - position of small arrow
	var arrow_offsetY = 0;	// Offset Y - position of small arrow

	if(!MSIE || navigatorVersion > 6){
		arrow_offsetX = -6;	// Firefox - offset X small arrow
		arrow_offsetY = -6; // Firefox - offset Y small arrow
	}

	var indicateDestinationBox = false;
	function getTopPos(inputObj)
	{
	  var returnValue = inputObj.offsetTop;
	  while((inputObj = inputObj.offsetParent) != null){
	  	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetTop;
	  }
	  return returnValue;
	}

	function getLeftPos(inputObj)
	{
	  var returnValue = inputObj.offsetLeft;
	  while((inputObj = inputObj.offsetParent) != null){
	  	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetLeft;
	  }
	  return returnValue;
	}

	function cancelEvent()
	{
		return false;
	}
	function initDrag(e)	// Mouse button is pressed down on a LI
	{
		if(document.all)e = event;
		var st = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
		var sl = Math.max(document.body.scrollLeft,document.documentElement.scrollLeft);

		dragTimer = 0;
		dragContentObj.style.left = e.clientX + sl + 'px';
		dragContentObj.style.top = e.clientY + st + 'px';
		contentToBeDragged = this;
		contentToBeDragged_src = this.parentNode;
		contentToBeDragged_next = false;
		if(this.nextSibling){
			contentToBeDragged_next = this.nextSibling;
			if(!this.tagName && contentToBeDragged_next.nextSibling)contentToBeDragged_next = contentToBeDragged_next.nextSibling;
		}
		timerDrag();
		return false;
	}

	function timerDrag()
	{
		if(dragTimer>=0 && dragTimer<10){
			dragTimer++;
			setTimeout('timerDrag()',10);
			return;
		}
		if(dragTimer==10){

			if(cloneSourceItems && contentToBeDragged.parentNode.id=='allItems'){
				newItem = contentToBeDragged.cloneNode(true);
				newItem.onmousedown = contentToBeDragged.onmousedown;
				contentToBeDragged = newItem;
			}
			dragContentObj.style.display='block';
			dragContentObj.appendChild(contentToBeDragged);
		}
	}

	function moveDragContent(e)
	{
		if(dragTimer<10){
			if(contentToBeDragged){
				if(contentToBeDragged_next){
					contentToBeDragged_src.insertBefore(contentToBeDragged,contentToBeDragged_next);
				}else{
					contentToBeDragged_src.appendChild(contentToBeDragged);
				}
			}
			return;
		}
		if(document.all)e = event;
		var st = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
		var sl = Math.max(document.body.scrollLeft,document.documentElement.scrollLeft);


		dragContentObj.style.left = e.clientX + sl + 'px';
		dragContentObj.style.top = e.clientY + st + 'px';

		if(mouseoverObj)mouseoverObj.className='';
		destinationObj = false;
		dragDropIndicator.style.display='none';
		if(indicateDestinationBox)indicateDestinationBox.style.display='none';
		var x = e.clientX + sl;
		var y = e.clientY + st;
		var width = dragContentObj.offsetWidth;
		var height = dragContentObj.offsetHeight;

		var tmpOffsetX = arrow_offsetX;
		var tmpOffsetY = arrow_offsetY;

		for(var no=0;no<ulPositionArray.length;no++){
			var ul_leftPos = ulPositionArray[no]['left'];
			var ul_topPos = ulPositionArray[no]['top'];
			var ul_height = ulPositionArray[no]['height'];
			var ul_width = ulPositionArray[no]['width'];

			if((x+width) > ul_leftPos && x<(ul_leftPos + ul_width) && (y+height)> ul_topPos && y<(ul_topPos + ul_height)){
				var noExisting = ulPositionArray[no]['obj'].getElementsByTagName('LI').length;
				if(indicateDestinationBox && indicateDestinationBox.parentNode==ulPositionArray[no]['obj'])noExisting--;
				if(noExisting<boxSizeArray[no-1] || no==0){
					dragDropIndicator.style.left = ul_leftPos + tmpOffsetX + 'px';
					var subLi = ulPositionArray[no]['obj'].getElementsByTagName('LI');

					var clonedItemAllreadyAdded = false;
					if(cloneSourceItems && !cloneAllowDuplicates){
						for(var liIndex=0;liIndex<subLi.length;liIndex++){
							if(contentToBeDragged.id == subLi[liIndex].id)clonedItemAllreadyAdded = true;
						}

						if(clonedItemAllreadyAdded)continue;
					}

					for(var liIndex=0;liIndex<subLi.length;liIndex++){
						var tmpTop = getTopPos(subLi[liIndex]);
						if(!indicateDestionationByUseOfArrow){
							if(y<tmpTop){
								destinationObj = subLi[liIndex];
								indicateDestinationBox.style.display='block';
								subLi[liIndex].parentNode.insertBefore(indicateDestinationBox,subLi[liIndex]);
								break;
							}
						}else{
							if(y<tmpTop){
								destinationObj = subLi[liIndex];
								dragDropIndicator.style.top = tmpTop + tmpOffsetY - Math.round(dragDropIndicator.clientHeight/2) + 'px';
								dragDropIndicator.style.display='block';
								break;
							}
						}
					}

					if(!indicateDestionationByUseOfArrow){
						if(indicateDestinationBox.style.display=='none'){
							indicateDestinationBox.style.display='block';
							ulPositionArray[no]['obj'].appendChild(indicateDestinationBox);
						}

					}else{
						if(subLi.length>0 && dragDropIndicator.style.display=='none'){
							dragDropIndicator.style.top = getTopPos(subLi[subLi.length-1]) + subLi[subLi.length-1].offsetHeight + tmpOffsetY + 'px';
							dragDropIndicator.style.display='block';
						}
						if(subLi.length==0){
							dragDropIndicator.style.top = ul_topPos + arrow_offsetY + 'px'
							dragDropIndicator.style.display='block';
						}
					}

					if(!destinationObj)destinationObj = ulPositionArray[no]['obj'];
					mouseoverObj = ulPositionArray[no]['obj'].parentNode;
					mouseoverObj.className='mouseover';
					return;
				}
			}
		}
	}

	/* End dragging
	Put <LI> into a destination or back to where it came from.
	*/
	function dragDropEnd(e)
	{
		if(dragTimer==-1)return;
		if(dragTimer<10){
			dragTimer = -1;
			return;
		}
		dragTimer = -1;
		if(document.all)e = event;


		if(cloneSourceItems && (!destinationObj || (destinationObj && (destinationObj.id=='allItems' || destinationObj.parentNode.id=='allItems')))){
			contentToBeDragged.parentNode.removeChild(contentToBeDragged);
		}else{

			if(destinationObj){
				if(destinationObj.tagName=='UL'){
					destinationObj.appendChild(contentToBeDragged);
				}else{
					destinationObj.parentNode.insertBefore(contentToBeDragged,destinationObj);
				}
				mouseoverObj.className='';
				destinationObj = false;
				dragDropIndicator.style.display='none';
				if(indicateDestinationBox){
					indicateDestinationBox.style.display='none';
					document.body.appendChild(indicateDestinationBox);
				}
				contentToBeDragged = false;
				return;
			}
			if(contentToBeDragged_next){
				contentToBeDragged_src.insertBefore(contentToBeDragged,contentToBeDragged_next);
			}else{
				contentToBeDragged_src.appendChild(contentToBeDragged);
			}
		}
		contentToBeDragged = false;
		dragDropIndicator.style.display='none';
		if(indicateDestinationBox){
			indicateDestinationBox.style.display='none';
			document.body.appendChild(indicateDestinationBox);

		}
		mouseoverObj = false;

	}

	/*
	Preparing data to be saved
	*/
	function saveDragDropNodes()
	{
		var saveString = "";
		var uls = dragDropTopContainer.getElementsByTagName('UL');
		for(var no=0;no<uls.length;no++){	// LOoping through all <ul>
			var lis = uls[no].getElementsByTagName('LI');
			for(var no2=0;no2<lis.length;no2++){
				if(saveString.length>0)saveString = saveString + ";";
				saveString = saveString + uls[no].id + '|' + lis[no2].id;
			}
		}

		//alert(saveString);//abrar
		document.getElementById('saveContent').innerHTML = saveString.replace(/;/g,';<br>');//abrar save to ajax
		SaveRezoAjax();//abrar

	}

	function initDragDropScript()
	{
		dragContentObj = document.getElementById('dragContent');
		dragDropIndicator = document.getElementById('dragDropIndicator');
		dragDropTopContainer = document.getElementById('dhtmlgoodies_dragDropContainer');
		document.documentElement.onselectstart = cancelEvent;;
		var listItems = dragDropTopContainer.getElementsByTagName('LI');	// Get array containing all <LI>
		var itemHeight = false;
		for(var no=0;no<listItems.length;no++){
			listItems[no].onmousedown = initDrag;
			listItems[no].onselectstart = cancelEvent;
			if(!itemHeight)itemHeight = listItems[no].offsetHeight;
			if(MSIE && navigatorVersion/1<6){
				listItems[no].style.cursor='hand';
			}
		}

		var mainContainer = document.getElementById('dhtmlgoodies_mainContainer');
		var uls = mainContainer.getElementsByTagName('UL');
		itemHeight = itemHeight + verticalSpaceBetweenListItems;
		for(var no=0;no<uls.length;no++){
			uls[no].style.height = itemHeight * boxSizeArray[no]  + 'px';
		}

		var leftContainer = document.getElementById('dhtmlgoodies_listOfItems');
		var itemBox = leftContainer.getElementsByTagName('UL')[0];

		document.documentElement.onmousemove = moveDragContent;	// Mouse move event - moving draggable div
		document.documentElement.onmouseup = dragDropEnd;	// Mouse move event - moving draggable div

		var ulArray = dragDropTopContainer.getElementsByTagName('UL');
		for(var no=0;no<ulArray.length;no++){
			ulPositionArray[no] = new Array();
			ulPositionArray[no]['left'] = getLeftPos(ulArray[no]);
			ulPositionArray[no]['top'] = getTopPos(ulArray[no]);
			ulPositionArray[no]['width'] = ulArray[no].offsetWidth;
			ulPositionArray[no]['height'] = ulArray[no].clientHeight;
			ulPositionArray[no]['obj'] = ulArray[no];
		}

		if(!indicateDestionationByUseOfArrow){
			indicateDestinationBox = document.createElement('LI');
			indicateDestinationBox.id = 'indicateDestination';
			indicateDestinationBox.style.display='none';
			document.body.appendChild(indicateDestinationBox);


		}
	}

	window.onload = initDragDropScript;
	</script>
</head>
<body>
<?php
$guests_all = array();
function daysDifference($date_start, $date_end)
{ // done abrar
	return intval((strtotime($date_end) - strtotime($date_start))/86400);
}
?>
<div style="width:950px;">
<div id="calendar_nav" style="width:300px; height:auto; float:left;">
  
  <table width="100%" border="0">
  <tr>
    <td><strong><?=SITE_NAME?></strong>
</td>
  </tr>
  <tr>
    <td><?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-4;
$next_month = $cMonth+4;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>
<a href="<?php echo $_SERVER["PHP_SELF"]."?date=".$date."&month=".$prev_month."&year=".$prev_year; ?>" >Previous</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo $_SERVER["PHP_SELF"]."?date=".$date."&month=".$next_month."&year=".$next_year; ?>" >Next</a></td>
  </tr>
  <tr>
    <td><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		if(strtotime($cToday)==strtotime($date)){$dateColor = "#00ff00";}else{$dateColor = "#ffffff";}

				$link_start = "<a href=\"rezo.php?date=$cToday&month=$month&year=$year\">";
				$link_end = "<a>";
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
  <tr>
    <td><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		if(strtotime($cToday)==strtotime($date)){$dateColor = "#00ff00";}else{$dateColor = "#ffffff";}
				$link_start = "<a href=\"rezo.php?date=$cToday&month=$month&year=$year\">";
				$link_end = "<a>";
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
  <tr>
    <td><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		if(strtotime($cToday)==strtotime($date)){$dateColor = "#00ff00";}else{$dateColor = "#ffffff";}
				$link_start = "<a href=\"rezo.php?date=$cToday&month=$month&year=$year\">";
				$link_end = "<a>";
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
  <tr>
    <td><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		if(strtotime($cToday)==strtotime($date)){$dateColor = "#00ff00";}else{$dateColor = "#ffffff";}
				$link_start = "<a href=\"rezo.php?date=$cToday&month=$month&year=$year\">";
				$link_end = "<a>";
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
</table>

</div>


<div id="guides" style="width:200px; float:left;">
  <div style="border: 1px solid #fff; width:195px; height:30px; padding:2px; margin-top:2px; clear:left;">
  <em>GUIDES</em>
  </div>
  <?php
  $i = '1';
  $result= mysql_query("SELECT * FROM ".GUIDES." ORDER BY lastname ASC") or die(__LINE__.mysql_error()); //
  while($row = mysql_fetch_array($result)){
  if($row['status']=='1'){$color='#000000';}else{$color='#000000';}
  ?>
  <div style="border: 1px solid #eee; width:195px; height:113px; padding:2px; margin-top:2px; clear:left; color:<?=$color?>"><?=stripslashes($row['lastname'])?>, <?=stripslashes($row['firstname'])?><br /></div>
  
  <?php
  $i++;
  }
  ?>
</div>

<div id="calendar" style="width:430px; float:right">

<div id="dates" style="height:30px;">
<?php for($i=0; $i<$weekdays; $i++){?>
<div <?php if($i===1) echo 'style="margin-left:1px;"';?>>
<?=date ("n/j/y", strtotime ("+$i day", strtotime($first_day)))?><br />
<?=date ("D", strtotime ("+$i day", strtotime($first_day)))?>
</div>
<?php } ?>
</div>
<div id="dhtmlgoodies_dragDropContainer">
  <div id="dhtmlgoodies_mainContainer">
	<!-- ONE <UL> for each "room" -->
	<?php
		$sql_chkdr = "SELECT * FROM ".RESERVATIONS." WHERE date >= '".date("Y-m-d",strtotime($first_day))."' AND date <= '".date("Y-m-d",strtotime($last_day))."'";
		$result_chkcdr= mysql_query($sql_chkdr) or die(__LINE__.mysql_error());
		while($result_fetch = mysql_fetch_array($result_chkcdr)){
		$sel_guidee = mysql_query("select * from reservation_guide where reserv_id = '".$result_fetch['id']."'") or die(mysql_error());
				
		mysql_num_rows($sel_guidee);
		if(mysql_num_rows($sel_guidee)=='0'){
		$no_bookings = true;
		}
		}
	?>
	<?php
	$result= mysql_query("SELECT * FROM ".GUIDES." ORDER BY lastname ASC") or die(__LINE__.mysql_error()); //
	while($row = mysql_fetch_array($result)){
	?>
	<?php for($i=1; $i<=$weekdays; $i++){ $thisdate = date("Y-m-d", strtotime("+".$i-1 ." days", strtotime($first_day)));?>
	<div<?php if($i===1) echo ' style="clear:left"';?>>
	  <ul id="guide=<?=$row['id']?>|date=<?=$thisdate?>">
	  <?php
		$sqlG_2 = "SELECT * FROM ".GUIDESCALENDAR." WHERE guideid = '".$row['id']."' AND date = '".$thisdate."' AND morning_status = 'N'";
		$resultG_2= mysql_query($sqlG_2) or die(__LINE__.mysql_error());
		if(mysql_num_rows($resultG_2)>"0"){
		echo '<img src="rezo-blocked.png" />';
		}
		?>
		<?php
		if(isset($no_bookings)){
		?>
		<li id="guide=00" style="background-color:<?=create_html_color("light")?>; width:220px;">
		<span style="float:left">
		No customer found for this week!
		</span>
		</li>
		<?php
		unset($no_bookings);
		}else{
		$sql_02 = "SELECT * FROM ".RESERVATIONS." WHERE date = '".$thisdate."'";
		$result_02= mysql_query($sql_02) or die(__LINE__.mysql_error());
		while($row_02 = mysql_fetch_array($result_02)){
			$sel_guidee = mysql_query("select * from reservation_guide where reserv_id = '".$row_02['id']."'  and guide_id = '".$row['id']."'") or die(mysql_error());
				while($row_giudd = mysql_fetch_array($sel_guidee)){
		$guest_days = 1;
			if($row_giudd['guide_id']==$row['id']){$guide_type = 'a';}
			/*elseif($row_02['guide_id_2']==$row['id']){$guide_type = 'b';}
			elseif($row_02['guide_id_3']==$row['id']){$guide_type = 'c';}*/
		
		?>
		<?php
		$guest_name = stripslashes($row_02['firstname']).' '.stripslashes($row_02['lastname']);
		//$guest_date_start = stripslashes(get_guest_date_start($row_02['guest_id']));
		//$guest_date_end = stripslashes(get_guest_date_end($row_02['guest_id']));
		//$guest_detail_rezo = stripslashes(guest_detail_rezo($row_02['guest_id']));
		$guest_days =  '1';
		$guest_name_substr = substr($guest_name, 0, $guest_days*50);
		?>
		<?php
		if($row_02['morning']=="1" AND $row_02['afternoon']=="0"){$wade = '1';}
		elseif($row_02['morning']=="0" AND $row_02['afternoon']=="1"){$wade = '2';}
		elseif($row_02['morning']=="1" AND $row_02['afternoon']=="1"){$wade = '3';}
		?>
		<?php if($wade=='1' OR $wade=='3'){ ?>
		<li id="reservation=<?=$row_02['id']?>|guide_type=<?=$guide_type?>" style=" <?php if($guest_date_start==$thisdate){echo 'cursor:move;';}?> width:56px;" title="<?=$guest_detail_rezo?>">
		<div style="background-color:<?=create_html_color("light")?>; <?php if($wade=='3') echo 'height:112px;'; else echo 'height:56px;';?> margin:-2px;">
		
		<span style="float:left; margin:2px 1px 0 0;">
		<a href="reservations.php?editid=<?=$row_02['id']?>" target="_blank"><img src="rezo-edit-pencil.png" width="13" height="12" border="0" /></a>
		<?php if($row_02['number_of_people']>'3'){?>
		<img src="rezo-multi-guides.png" width="12" height="12" border="0" />
		<?php } ?>
		</span>
		
		<span style="float:left">
		<?php
		echo $guest_name_substr;
		if(strlen($guest_name)>strlen($guest_name_substr)) echo '..';
		//echo "#".$row_02['guest_id']
		?>
		</span>
		</div>
		</li>
		<?php } ?>
		<?php
		}
		unset($thisdate);
		} }
		?>
	  </ul>
	</div>
	<?php } ?>
	<!---afternoon----------------------------------------->
	<?php for($i=1; $i<=$weekdays; $i++){ $thisdate = date("Y-m-d", strtotime("+".$i-1 ." days", strtotime($first_day)));?>
	<div<?php if($i==1) echo ' style="clear:left"';?>>
	  <ul id="guide=<?=$row['id']?>|date=<?=$thisdate?>">
	  <?php
		$sqlG_2 = "SELECT * FROM ".GUIDESCALENDAR." WHERE guideid = '".$row['id']."' AND date = '".$thisdate."' AND afternoon_status = 'N'";
		$resultG_2= mysql_query($sqlG_2) or die(__LINE__.mysql_error());
		if(mysql_num_rows($resultG_2)>"0"){
		echo '<img src="rezo-blocked.png" />';
		}
		?>
		<?php
		$sql_02 = "SELECT * FROM ".RESERVATIONS." WHERE date = '".$thisdate."'";
		$result_02= mysql_query($sql_02) or die(__LINE__.mysql_error());
		while($row_02 = mysql_fetch_array($result_02)){
			$sel_guidee = mysql_query("select * from reservation_guide where reserv_id = '".$row_02['id']."' and guide_id = '".$row['id']."'") or die(mysql_error());
				while($row_giudd = mysql_fetch_array($sel_guidee)){
		$guest_days = 1;
			if($row_giudd['guide_id']==$row['id']){$guide_type = 'a';}
			/*elseif($row_02['guide_id_2']==$row['id']){$guide_type = 'b';}
			elseif($row_02['guide_id_3']==$row['id']){$guide_type = 'c';}*/
		?>
		<?php
		$guest_name = stripslashes($row_02['firstname']).' '.stripslashes($row_02['lastname']);
		//$guest_date_start = stripslashes(get_guest_date_start($row_02['guest_id']));
		//$guest_date_end = stripslashes(get_guest_date_end($row_02['guest_id']));
		//$guest_detail_rezo = stripslashes(guest_detail_rezo($row_02['guest_id']));
		$guest_days =  '1';
		$guest_name_substr = substr($guest_name, 0, $guest_days*50);
		?>
		<?php
		if($row_02['morning']=="1" AND $row_02['afternoon']=="0"){$wade = '1';}
		elseif($row_02['morning']=="0" AND $row_02['afternoon']=="1"){$wade = '2';}
		elseif($row_02['morning']=="1" AND $row_02['afternoon']=="1"){$wade = '3';}
		?>
		<?php if($wade=='2'){ ?>
		<li id="reservation=<?=$row_02['id']?>|guide_type=<?=$guide_type?>" style=" <?php if($guest_date_start==$thisdate){echo 'cursor:move;';}?> width:56px;" title="<?=$guest_detail_rezo?>">
		
		<div style="background-color:<?=create_html_color("light")?>; height:56px; margin:-2px;">
		
		<span style="float:left; margin:2px 1px 0 0;">
		<a href="reservations.php?editid=<?=$row_02['id']?>" target="_blank"><img src="rezo-edit-pencil.png" width="13" height="12" border="0" /></a>
		<?php if($row_02['number_of_people']>'3'){?>
		<img src="rezo-multi-guides.png" width="12" height="12" border="0" />
		<?php } ?>
		</span>

		<span style="float:left">
		<?php
		echo $guest_name_substr;
		if(strlen($guest_name)>strlen($guest_name_substr)) echo '..';
		//echo "#".$row_02['guest_id']
		?>
		</span>
		</div>
		</li>
		<?php } ?>
		<?php
		}
		unset($thisdate);
		?>
	  
	<?php } ?> </ul>
	</div> <?php } ?>
	<div style="clear:left; width:430px; height:1px; background-image:none; border-top:#ffffff dotted 1px;"></div>
	<?php 
	} ?>
  </div>
  <div id="dhtmlgoodies_listOfItems">
	<div>
	  <ul id="allItems">
	  </ul>
	</div>
  </div>
</div>

</div>

</div>

<div id="submit" style="clear:both" align="center">
  <form action="" method="post">
    <input type="button" onclick="saveDragDropNodes()" value="Save"><input type="button" value="Refresh" onClick="window.location.reload()">
  </form>
</div>
<ul id="dragContent">
</ul>
<div id="dragDropIndicator"></div>
<div id="saveContent" align="center" style="display:none;">
</div>
</body>
</html>