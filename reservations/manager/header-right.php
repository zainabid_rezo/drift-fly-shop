<div id="top">
  <!-- Logo -->
  <div class="logo">
    <h1>
      <a href="index.php" style="color:white; text-decoration:none;"><?=SITE_NAME?></a>
    </h1>
  </div>
  <!-- End of Logo -->
  <!-- Meta information -->
  <div class="meta">
    <p>Welcome,
      <?=ADMIN_FIRSTNAME?>
      <?=ADMIN_LASTNAME?>
      !
      <?php if(isset($num_system_messages) && $num_system_messages>"0"){?>
      <a href="home.php" title="<?=$num_system_messages?> new private messages from System!" class="tooltip">
      <?=$num_system_messages?>
      new messages!</a></p>
    <?php } ?>
    <ul>
      <li><a href="logout.php" title="" class="tooltip"><span class="ui-icon ui-icon-power"></span>Logout</a></li>
      <?php if(isset($_SESSION["admin_type"]) && $_SESSION["admin_type"] == 1){ ?>
      <?php
	  //echo TEST_MODE;
	  if(TEST_MODE == 1){
	  ?>
      <li><span style="color:red;">IN TEST MODE</span></li>
      <?php } } ?>
    </ul>
  </div>
  <!-- End of Meta information -->
</div>
