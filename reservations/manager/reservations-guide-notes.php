<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$imgwidth = "480";
$imgheight = "640";
?>
<?php
if(isset($_POST['btnEditDo'])){
$success = "";
$error = "";

	$editid		=	addslashes($_POST['editid']);

	$date		=	date('Y-m-d',strtotime($_POST['date']));
	if(empty($date))$error .= "Please enter date.<br/>";
	$river_id		=	htmlentities($_POST['river_id'], ENT_QUOTES);
	if(empty($river_id))$error .= "Please select river.<br/>";
	$entry		=	htmlentities($_POST['entry'], ENT_QUOTES);
	$exxit		=	htmlentities($_POST['exxit'], ENT_QUOTES);
	$cfs		=	htmlentities($_POST['cfs'], ENT_QUOTES);
	$weather		=	htmlentities($_POST['weather'], ENT_QUOTES);
	$frontend_show = $_POST['frontend_show'] ? '1' : '0';

	$general_notes		=	htmlentities($_POST['general_notes'], ENT_QUOTES);
	$guide_notes		=	htmlentities($_POST['guide_notes'], ENT_QUOTES);
	
	if(empty($error)){
	$sql		=	"UPDATE ".FISHINGREPORTS." SET date = '$date', river_id = '$river_id', entry = '$entry', exxit = '$exxit', cfs = '$cfs', weather = '$weather', frontend_show = '$frontend_show', general_notes = '$general_notes', guide_notes = '$guide_notes' WHERE id = '$editid'";//exxit = '$exxit', 
	mysql_query($sql) or die(mysql_error());
	$success 	= "Updated successfuly.";
	}

if(empty($error) AND !isset($_GET['reservation_id'])){unset($_GET);}

}
?>
<?php
if(isset($_GET['deleteid'])){
	$sql		=	"DELETE FROM ".FISHINGREPORTS." WHERE id = '$_GET[deleteid]'";
	mysql_query($sql) or die(mysql_error());
	$success	= "Successfully deleted.<br/>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<!---->
<style type="text/css"> 
@import "calendar_popup/jquery.datepick.css";
.img {
background-color:#90654B;
display:inline;
float:left;
margin:10px;
padding:5px;
}
</style>

<script type="text/javascript" src="calendar_popup/jquery.datepick.js"></script>
<script type="text/javascript"> 
$(function() {

$('#date').datepick();

$('#date_start,#date_end').datepick({onSelect: customRange, 
    showOn: 'both', buttonImageOnly: true, buttonImage: 'calendar_popup/calendar.gif'}); 
 function customRange(dateStr, date) { 
    if (this.id == 'date_start') { 
        $('#date_end').datepick('option', 'minDate', date); 
    } 
    else { 
        $('#date_start').datepick('option', 'maxDate', date); 
    } 
}
 
});

</script>
<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
<?php if(isset($done)){ ?>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("parent.parent.GB_hide();",2000);
	</SCRIPT>
<?php } ?>
</head>
<body>
<!------------------------------------------------------------------------------>
<?php include"messages-display.php";?>
<?php
if(isset($_GET['reservation_id'])){
$sql = "SELECT * FROM ".FISHINGREPORTS." WHERE reservation_id = '$_GET[reservation_id]'";
$result= mysql_query($sql);
if(mysql_num_rows($result)=='0') die("No reports added by guide!");
}
?>
<?php if(isset($_GET['editid']) OR isset($_GET['reservation_id'])){ ?>
<h2>Guide Report / Fishing Report </h2>
<?php

$result= mysql_query("SELECT * FROM ".FISHINGREPORTS." WHERE reservation_id = '$_GET[reservation_id]'");
while($row = mysql_fetch_array($result)){
$id = $_GET['editid'] = $row['id'];

?>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
    <tr>
      <th scope="row">Date<font color="#FF0000">*</font></th>
      <td ><input name="date" type="text" id="date" value="<?=date('m/d/Y',strtotime($row['date']))?>" size="12" />
      <img src="calendar_popup/calendar.gif" width="17" height="14" /></td>
      <td ><!--<label>
		    <input name="frontend_show" type="checkbox" value="1" <?php if(isset($_POST['frontend_show']) OR $row['frontend_show']=='1') echo 'checked="checked"';?> />
		    Report is the best		  </label>--></td>
    </tr>
    <tr>
      <th width="100" scope="row">River<font color="#FF0000">*</font></th>
      <td colspan="2" ><select name="river_id" id="river_id">
          <option value="0" <?php if(!isset($row['river_id']) OR $row['river_id']=="0"){echo 'selected="selected"';}?> > - select - </option>
          <?php
					
					$resultW= mysql_query("SELECT * FROM ".RIVERS." ORDER BY title ASC");
						while($rowW = mysql_fetch_array($resultW)){
						
						echo '<option value="'.$rowW['id'].'"';
						if(isset($row['river_id']) AND $row['river_id']==$rowW['id']){echo 'selected="selected"';}
						echo '>'.stripslashes($rowW['title']).'</option>';
						
						}
					
					?>
        </select></td>
    </tr>
    <tr>
      <th width="100" scope="row">Entry</th>
      <td colspan="2" ><input name="entry" type="text" id="entry" value="<?=stripslashes($row['entry'])?>" size="30" /></td>
    </tr>
    <tr>
      <th width="100" scope="row">Exit</th>
      <td colspan="2" ><input name="exxit" type="text" id="exxit" value="<?=stripslashes($row['exxit'])?>" size="30" /></td>
    </tr>
    <tr>
      <th width="100" scope="row">CFS</th>
      <td colspan="2" ><input name="cfs" type="text" id="cfs" value="<?=stripslashes($row['cfs'])?>" size="5" /></td>
    </tr>
    <tr>
      <th width="100" scope="row">Weather</th>
      <td colspan="2" ><input name="weather" type="text" id="weather" value="<?=stripslashes($row['weather'])?>" size="25" /></td>
    </tr>
    <tr>
      <th width="100" scope="row">General Notes</th>
      <td colspan="2" ><textarea name="general_notes" rows="5" id="general_notes" style="width:800px; height:120px;"><?=stripslashes($row['general_notes'])?></textarea></td>
    </tr>
    <tr>
      <th width="100" scope="row">Guide Notes</th>
      <td colspan="2" ><textarea name="guide_notes" rows="5" id="guide_notes" style="width:800px; height:120px;"><?=stripslashes($row['guide_notes'])?></textarea></td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td colspan="2"><input type="submit" name="btnEditDo" class="button" value="Update" />
        <input name="editid" type="hidden" value="<?php echo $row['id']; ?>" /></td>
    </tr>
  </table>
</form>
<br />
<?php } ?>
<?php }else{?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
<thead>
  <tr>
    <th width="200">Date</th>
    <th width="200">River </th>
    <td width="50">Edit</td>
    <td width="50">Delete</td>
  </tr>
</thead>
<tbody>
<?php
$result= mysql_query("SELECT * FROM ".FISHINGREPORTS." WHERE guide_id = '$_SESSION[guide_userid]' ORDER BY id DESC") or die(__LINE__.mysql_error());
while($row = mysql_fetch_array($result)){
?>
  <tr>
    <td width="150"><?=date('m/d/Y',strtotime($row['date']))?></td>
    <td width="150"><?php
	$resultW= mysql_query("SELECT * FROM ".RIVERS." WHERE id = '$row[river_id]'");
	while($rowW = mysql_fetch_array($resultW)){
	echo stripslashes($rowW['title']);
	}
	?></td>
    <td width="50"><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" /></a></td>
    <td width="50"><a href="?deleteid=<?=$row['id']?>" onclick="return confirm('Are you sure you want to delete?')"><img src="assets/delete.png" width="16" height="16" /></a></td>
  </tr>
  <?php } ?>
  </tbody>
</table>
<?php } ?>
</body>
</html>