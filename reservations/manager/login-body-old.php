<h1>Administrator Login!</h1>
<?php include"messages-display.php";?>
<table class="form-table">
  <form id="login-form" name="login-form" method="post" action="">
    <tr>
      <th>Login</th>
      <td><input type="text" name="admin_login" />      </td>
    </tr>
    <tr>
      <th>Password</th>
      <td><input type="password" name="admin_pass" /></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td><label>
        <input type="checkbox" name="remember_me" value="1" />
      Remember me</label></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td><input type="submit" name="btnLogin" value="Login" class="button" />      </td>
    </tr>
  </form>
</table>
