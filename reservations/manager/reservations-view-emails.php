<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
<?php if(isset($done)){ ?>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("parent.parent.GB_hide();",2000);
	</SCRIPT>
<?php } ?>
</head>
<body>

<h2>Emails Sent Out!</h2>
<?php
if(isset($_GET['view'])){ ?>

<?php
$id = $_GET['view'];
$result= mysql_query("SELECT * FROM ".EMAILARCHIVES." WHERE id = '$id' AND user != ''");
while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
    <tr>
      <th>Email</th>
      <td><?=stripslashes($row['user'])?></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th>Subject</th>
        <td><?=stripslashes($row['subject'])?></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th>Message</th>
        <td><?=stripslashes($row['body'])?></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th>Status</th>
        <td><?=stripslashes($row['status'])?></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th>Attachment</th>
        <td><?=stripslashes($row['attachment'])?></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <th>Time</th>
        <td><?=date('m/d/Y h:i a',strtotime($row['sent_time']))?></td>
    </tr>
  </table>
</form>
<?php } ?>
<a href="reservations-view-emails.php?guest_id=<?=$_GET['guest_id'];?>">Back</a>
<?php }else{ ?>
<br /><br />
<h2>Guest Emails</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="150">Guest Email
        </th>
      <th width="150">Guest
        </th>
      <th width="150">Subject
        </th>
      <th width="50">Status
        </th>
      <th width="50">Attachment
        </th>
      <th width="50">Sent Time
        </th>
      <td>
        </td>
    </tr>
  </thead>
  <?php
	$sql = "SELECT * FROM ".EMAILARCHIVES." WHERE guest_id = '".(int)$_GET['guest_id']."' AND user != '' AND position = 'Customer' ORDER BY sent_time DESC";//
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	if(mysql_num_rows($result)=="0"){
	?>
  <tr>
    <td colspan="8">No sent emails in the record!</td>
  </tr>
  <?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
  <tr>
    <td><?=stripslashes($row['user'])?>
    </td>
    <td><?=stripslashes(get_guest_name($row['guest_id']))?></td>
    <td><?=stripslashes($row['subject'])?>
    </td>
    <td><?=stripslashes($row['status'])?>
    </td>
    <td><?=stripslashes($row['attachment'])?>
    </td>
    <td><?=date("m/d/Y H:i:s",strtotime($row['sent_time']))?></td>
    <td><a href="?guest_id=<?=$_GET['guest_id']?>&view=<?=$row['id']?>">view</a></td>
	
	
  </tr>
  <?php } ?>
  <?php } ?>
</table>

	<br /><br />
<h2>Emails Sent to ADMIN</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="150">ADMIN Email
        </th>
      <th width="150">ADMIN
        </th>
      <th width="150">Subject
        </th>
      <th width="50">Status
        </th>
      <th width="50">Attachment
        </th>
      <th width="50">Sent Time
        </th>
      <td>
        </td>
    </tr>
  </thead>
  <?php
	$sql = "SELECT * FROM ".EMAILARCHIVES." WHERE guest_id = '".(int)$_GET['guest_id']."' AND user != '' AND position = 'ADMIN' ORDER BY sent_time DESC";//
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	if(mysql_num_rows($result)=="0"){
	?>
  <tr>
    <td colspan="8">No sent emails in the record!</td>
  </tr>
  <?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
  <tr>
    <td><?=stripslashes($row['user'])?>
    </td>
    <td><?=ADMIN_FIRSTNAME.''.ADMIN_LASTNAME?></td>
    <td><?=stripslashes($row['subject'])?>
    </td>
    <td><?=stripslashes($row['status'])?>
    </td>
    <td><?=stripslashes($row['attachment'])?>
    </td>
    <td><?=date("m/d/Y H:i:s",strtotime($row['sent_time']))?></td>
    <td><a href="?guest_id=<?=$_GET['guest_id']?>&view=<?=$row['id']?>">view</a></td>
	
	
  </tr>
  <?php } ?>
  <?php } ?>
</table>
<br /><br />
<h2>Emails Sent to GUIDE</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="150">Guide Email
        </th>
      <th width="150">ADMIN
        </th>
      <th width="150">Subject
        </th>
      <th width="50">Status
        </th>
      <th width="50">Attachment
        </th>
      <th width="50">Sent Time
        </th>
      <td>
        </td>
    </tr>
  </thead>
  <?php
	$sql = "SELECT * FROM ".EMAILARCHIVES." WHERE guest_id = '".(int)$_GET['guest_id']."' AND user != '' AND position = 'Guide' ORDER BY sent_time DESC";//
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	if(mysql_num_rows($result)=="0"){
	?>
  <tr>
    <td colspan="8">No sent emails in the record!</td>
  </tr>
  <?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
  <tr>
    <td><?=stripslashes($row['user'])?>
    </td>
    <td><?=ADMIN_FIRSTNAME.''.ADMIN_LASTNAME?></td>
    <td><?=stripslashes($row['subject'])?>
    </td>
    <td><?=stripslashes($row['status'])?>
    </td>
    <td><?=stripslashes($row['attachment'])?>
    </td>
    <td><?=date("m/d/Y H:i:s",strtotime($row['sent_time']))?></td>
    <td><a href="?guest_id=<?=$_GET['guest_id']?>&view=<?=$row['id']?>">view</a></td>
	
	
  </tr>
  <?php } ?>
  <?php } ?>
</table>
<br /><br />
<h2>Emails Sent to Cafe</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="150">Cafe Email
        </th>
      <th width="150">Subject
        </th>
      <th width="50">Status
        </th>
      <th width="50">Attachment
        </th>
      <th width="50">Sent Time
        </th>
      <td>
        </td>
    </tr>
  </thead>
  <?php
	$sql = "SELECT * FROM ".EMAILARCHIVES." WHERE guest_id = '".(int)$_GET['guest_id']."' AND user != '' AND position = 'Cafe' ORDER BY sent_time DESC";//
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	if(mysql_num_rows($result)=="0"){
	?>
  <tr>
    <td colspan="8">No sent emails in the record!</td>
  </tr>
  <?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
  <tr>
    <td><?=stripslashes($row['user'])?>
    </td>
    <td><?=stripslashes($row['subject'])?>
    </td>
    <td><?=stripslashes($row['status'])?>
    </td>
    <td><?=stripslashes($row['attachment'])?>
    </td>
    <td><?=date("m/d/Y H:i:s",strtotime($row['sent_time']))?></td>
    <td><a href="?guest_id=<?=$_GET['guest_id']?>&view=<?=$row['id']?>">view</a></td>
	
	
  </tr>
  <?php } ?>
  <?php } ?>
</table>

<br /><br />


  <?php } ?>
</body>
</html>