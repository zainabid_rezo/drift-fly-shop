<?php 
$error = '';
if(isset($_POST['btnSearch']) AND ($_POST['date_start']=="mm/dd/yyyy" OR $_POST['date_end']=="mm/dd/yyyy") ){
$error = "Please select date range!";
}
$date_start = date("Y-m-d",strtotime($_POST['date_start']));
$date_end = date("Y-m-d",strtotime($_POST['date_end']));
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>

<form name="statistics" method="post" action="">
  <table width="800" class="noprint">
    <tr>
      <td colspan="4"><div class="quickt"><strong>Search:</strong></div></td>
    
    <tr>
      <td width="25%" class="redtext">Select Date:</td>
      <td width="25%"><input name="date_start" type="text" id="date_start" value="<?php if(isset($_POST['date_start'])) echo $_POST['date_start']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
      
    </tr>
    <tr>
      <td class="redtext">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="btnSearch" id="Search" value="Search" class="button" />      </td>
    </tr>
  </table>
</form>

<?php 
//print_r($_POST);
?>
<?php if(isset($_POST['btnSearch']) AND empty($error)){ ?>
<br />
<br />
<table class="noprint">
    <tr><td>
	<input type="button" value="Print this page" onClick="printpage()" class="button" />
	</td></tr>
</table>
<br />
<br />
<div class="hiddenprint">
<h2>Daily Report</h2>

</div>
<?php } ?>
<?php 

$trips_count = 0;
$total_rev = 0;
$ttlcash = 0;
$ttlvisa = 0;
$ttlamex = 0;

if(isset($_POST['btnSearch']) AND empty($error)){
	
		$date_start =  date('Y-m-d',strtotime($_POST['date_start']));
		$sel_query = mysql_query("SELECT * FROM wbooking_reservations_posts WHERE date = '$date_start'") or die(mysql_error());
		$counttrips = mysql_num_rows($sel_query);
		while($row_query = mysql_fetch_array($sel_query)){
			
			
			$total_rev += $row_query['calculated_price'];
			
			$sel_query2 = mysql_query("SELECT * FROM reservation_spli_amount WHERE reservation_id = '".$row_query['id']."'") or die(mysql_error());
			while($row_query2 = mysql_fetch_array($sel_query2)){
				if($row_query2['typee'] == 'cash'){
					$ttlcash += $row_query2['amount'];
				}
				if($row_query2['typee'] == 'visa'){
					$ttlvisa += $row_query2['amount'];
				}
				if($row_query2['typee'] == 'amex'){
					$ttlamex += $row_query2['amount'];
				}
			}
			
		}
	 ?>
<div class="printable">

<table>
<tr>
<td><h2>Total Information</h2></td>
</tr>
<tr><td></td></tr>

<tr><td><b>Trips:</b> <?=$counttrips;?></td></tr>
<tr><td><b>Revenue:</b> $<?=number_format($total_rev,2);?></td></tr>
<tr><td></td></tr>

<tr><td><b>Cash:</b> $<?=number_format($ttlcash,2);?></td></tr>
<tr><td><b>Visa/MasterCard:</b> $<?=number_format($ttlvisa,2);?></td></tr>
<tr><td><b>AMEX:</b> $<?=number_format($ttlamex,2);?></td></tr>
<tr><td></td></tr>

<tr><td></td></tr>
</table>

<table>
<tr>
<td><h2>Detail Information</h2></td>
</tr>
</table>

<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
<thead>


  <tr>
    <th>Customer Name </th>
    <th># of People</th>
    <th>Trip Name</th>
    <th>Private Water Cost</th>
    <th>Total Amount</th>
    <th>Payment Method</th>
    <th>Cash</th>
    <td>Visa / MasterCard </td>
    <td>AMEX </td>
  </tr>
</thead>
<tbody>

		<?php
		$cashh = 0;
		$visaa = 0;
		$amexx = 0;
		
		$sel_query3 = mysql_query("SELECT * FROM wbooking_reservations_posts WHERE date = '$date_start'") or die(mysql_error());
		while($row = mysql_fetch_array($sel_query3)){
			$sel_trip_name = mysql_query("select * from wbooking_trips_posts where id = '".$row['trip_id']."'") or die(mysql_error());
			$row_trip_name = mysql_fetch_array($sel_trip_name);
			$sel_query4 = mysql_query("SELECT * FROM reservation_spli_amount WHERE reservation_id = '".$row['id']."'") or die(mysql_error());
			while($row_query4 = mysql_fetch_array($sel_query4)){
				if($row_query4['typee'] == 'cash'){
					$cashh = $row_query4['amount'];
				}
				if($row_query4['typee'] == 'visa'){
					$visaa = $row_query4['amount'];
				}
				if($row_query4['typee'] == 'amex'){
					$amexx = $row_query4['amount'];
				}
			}
			
		?>
	  <tr>
		<td><?=stripslashes($row['firstname'])?> <?=stripslashes($row['lastname'])?></td>
		<td><?=($row['number_of_people'])?></td>
        <td><?=$row_trip_name['title']?></td>
        <td>$<?=number_format($row['rod_fee'],2)?></td>
        <td>$<?=number_format($row['calculated_price'],2)?></td>
        <td><?php if($row['payment_method'] != '0') echo $row['payment_method']; else echo 'No Payment method selected';?></td>
        <td>
        <?php
		$sel_query4 = mysql_query("SELECT * FROM reservation_spli_amount WHERE reservation_id = '".$row['id']."' and typee = 'cash'") or die(mysql_error());
		$row_query4 = mysql_fetch_array($sel_query4);
		echo '$'.number_format($row_query4['amount'],2);
		?>
        </td>
        <td>
        <?php
		$sel_query5 = mysql_query("SELECT * FROM reservation_spli_amount WHERE reservation_id = '".$row['id']."' and typee = 'visa'") or die(mysql_error());
		$row_query5 = mysql_fetch_array($sel_query5);
		echo '$'.number_format($row_query5['amount'],2);
		?>
        </td>
        <td>
        <?php
		$sel_query6 = mysql_query("SELECT * FROM reservation_spli_amount WHERE reservation_id = '".$row['id']."' and typee = 'amex'") or die(mysql_error());
		$row_query6 = mysql_fetch_array($sel_query6);
		echo '$'.number_format($row_query6['amount'],2);
		?>
        </td>
	  </tr>
      <?php } ?>
	  
  
</tbody>
</table>
</div>
<?php } ?>

</div>