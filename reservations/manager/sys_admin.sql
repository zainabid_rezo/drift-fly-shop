-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 11, 2015 at 03:17 AM
-- Server version: 5.1.73
-- PHP Version: 5.4.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jeep_rental`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_admin`
--

CREATE TABLE IF NOT EXISTS `sys_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_email` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(255) DEFAULT NULL,
  `admin_firstname` varchar(25) DEFAULT NULL,
  `admin_lastname` varchar(25) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `blocked` datetime NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `redirect_url_1` varchar(255) NOT NULL,
  `redirect_url_2` varchar(255) NOT NULL,
  `sales_tax_rate` float NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `smtp_path` varchar(255) NOT NULL,
  `smtp_port` varchar(255) NOT NULL,
  `database_backup_frequency` varchar(50) NOT NULL,
  `gateway_mode` varchar(10) NOT NULL,
  `title_meta` text NOT NULL,
  `keywords_meta` text NOT NULL,
  `description_meta` text NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `country` varchar(25) DEFAULT NULL,
  `postcode` varchar(25) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `registration_date` date NOT NULL,
  `admin_type` int(12) NOT NULL COMMENT '1=super_admin, 2=minor_admin',
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
