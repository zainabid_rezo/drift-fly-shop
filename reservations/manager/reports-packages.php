<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php"); 
?>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_GET["month"])) $_GET["month"] = date("n");
if (!isset($_GET["year"])) $_GET["year"] = date("Y");
?>

<?php
$cMonth = $_GET["month"];
$cYear = $_GET["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>
<?php
$link_previous = date("F",mktime(0,0,0,$prev_month,1,$prev_year));
$link_next = date("F",mktime(0,0,0,$next_month,1,$next_year));
?>
<!DOCTYPE html>
<html>
<head>
<title>Chart</title>
<link class="include" rel="stylesheet" type="text/css" href="chart_src/jquery.jqplot.min.css" />
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="chart_src/excanvas.js"></script><![endif]-->
<script class="include" type="text/javascript" src="chart_src/jquery.min.js"></script>
<script class="include" type="text/javascript" src="chart_src/jquery.jqplot.min.js"></script>

<script class="include" type="text/javascript" src="chart_src/jqplot.pieRenderer.min.js"></script>

<script type="text/javascript" src="chart_src/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.pointLabels.min.js"></script>

</head>
<body>
<table border="0" align="center" style="width:100%">
  <tr>
    <td width="33%" align="right" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $prev_month . "&year=" . $prev_year; ?>" >&lsaquo;&lsaquo;<?=$link_previous?></a></td>
    <td width="33%" align="center" valign="top"><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></td>
    <td width="33%" align="left" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $next_month . "&year=" . $next_year; ?>" ><?=$link_next?>&rsaquo;&rsaquo;</a></td>
  </tr>
</table>


<?php
$package_ids = array();
$sql = "SELECT * FROM ".RENTERSDETAIL." WHERE status = 'confirmed' ORDER BY id ASC";
$result= mysql_query($sql);
while($row = mysql_fetch_array($result)){

if(date("n",strtotime($row['date_start']))==$_GET['month'] AND date("Y",strtotime($row['date_start']))==$_GET['year']){

if(!in_array($row['id'],$package_ids)){array_push($package_ids,$row['id']);}
}
}
?>
<?php if(count($package_ids)=="0"){die("<br/><br/><br/><br/><br/><center>No renters found for this month!</center>");}?>
<?php
$maxdays = date("t",mktime(0,0,0,$cMonth,1,$cYear)); 
?>

<div id="chart-triptypes-pie" style="margin:auto; width:760px; height:350px;"></div>

<script type="text/javascript">$(document).ready(function(){
		  plot2 = jQuery.jqplot('chart-triptypes-pie', 
			[[
			<?php
			$package_ids = array();
			$sql = "SELECT * FROM ".RENTERSDETAIL." WHERE status = 'confirmed' ORDER BY id ASC";
			$result= mysql_query($sql);
			while($row = mysql_fetch_array($result)){
			
			if(date("n",strtotime($row['date_start']))==$_GET['month'] AND date("Y",strtotime($row['date_start']))==$_GET['year']){
			
			if(!in_array($row['package_id'],$package_ids)){array_push($package_ids,$row['package_id']);}
			}
			}
			
				if(count($package_ids)>"0"){
					foreach($package_ids as $package_id){
						
						$total_occurrences = '0';
						$sql = "SELECT * FROM ".RENTERSDETAIL." WHERE package_id = '$package_id' AND status = 'confirmed' ORDER BY id ASC";

						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							if(date("n",strtotime($row['date_start']))==$_GET['month'] AND date("Y",strtotime($row['date_start']))==$_GET['year']){
							$total_occurrences ++;
							}
						}
						echo "['".get_package_name($package_id)."' , ".$total_occurrences."], ";
			
					}
				}else{
					echo "['No renters found for this month', 100]";	
				}
			?>
			]], 
			{
			  title: ' ', 
			  seriesDefaults: {
				shadow: false, 
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: { 
				  startAngle: 180, 
				  sliceMargin: 4, 
				  showDataLabels: true } 
			  }, 
			  legend: { show:true, location: 'w' }
			}
		  );
		});
</script>

</body>
</html>

