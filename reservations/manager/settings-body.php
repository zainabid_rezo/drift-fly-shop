<?php
if(isset($_POST['btnUpdate']))
{

$error = "";
$site_name = addslashes($_POST['site_name']);
if(empty($site_name)) $error .= "Please enter site name.<br/>";
$site_email = $admin_email = addslashes($_POST['site_email']);
if(empty($site_email)) $error .= "Please enter site email.<br/>";
if (!filter_var($site_email, FILTER_VALIDATE_EMAIL)) $error .= "Please enter correct email address.<br/>";
$site_url = addslashes($_POST['site_url']);
if(empty($site_url)) $error .= "Please enter site url.<br/>";
if(!empty($site_url) AND !filter_var($site_url, FILTER_VALIDATE_URL)) $error .="Please enter correct url<br>";
if(substr($site_url, -1) != "/") $site_url = $site_url."/";

$username = addslashes($_POST['username']);
$password = addslashes($_POST['password']);
if(!empty($password) AND (strlen($password)<4)) $error .= "Your password must consist at least 4 characters.<br/>";
$password_confirm = addslashes($_POST['password_confirm']);
if($password != $password_confirm) $error .="Password does not match.<br>";

$admin_firstname = addslashes($_POST['admin_firstname']);
if(empty($admin_firstname)) $error .= "Please enter first name.<br/>";
$admin_lastname = addslashes($_POST['admin_lastname']);
if(empty($admin_lastname)) $error .= "Please enter last name.<br/>";

$max_guides = $_POST['max_guides'];

////////

$sales_tax_rate = 0.00;
$gateway_mode = addslashes($_POST['gateway_mode']);

if(empty($error)){
		
	if(empty($password)){
	
	$sql = "UPDATE ".ADMIN." SET site_name = '$site_name', admin_firstname = '$admin_firstname', admin_lastname = '$admin_lastname', max_guides = '$max_guides', admin_email='$admin_email', site_email = '$site_email', site_url='$site_url', sales_tax_rate='$sales_tax_rate', gateway_mode='$gateway_mode', username='$username' WHERE id = '1'";
	if(mysql_query($sql)){$success = "Successfully updated.";}else{ $error = "Error: ".__LINE__.": ".mysql_error();};

	}else{
	
	$sql = "UPDATE ".ADMIN." SET site_name = '$site_name', admin_firstname = '$admin_firstname', admin_lastname = '$admin_lastname', max_guides = '$max_guides', admin_email='$admin_email', site_email = '$site_email', site_url='$site_url', username='$username', getmein='$password', gateway_mode='$gateway_mode', sales_tax_rate='$sales_tax_rate' WHERE id = '1'";
	if(mysql_query($sql)){$success = "Successfully updated.";}else{ $error = "Error: ".__LINE__.": ".mysql_error();};
	
	}
	
	
	$sql = "UPDATE ".ADMIN." SET site_url='$site_url', max_guides = '$max_guides', sales_tax_rate='$sales_tax_rate' WHERE id = '1'";
	if(mysql_query($sql)){$success = "Successfully updated.";}else{ $error = "Error: ".__LINE__.": ".mysql_error();};
	
}

}
?>


<div style="float:left;">
<h1>Settings!</h1>
</div>


<div style="float:right;"><h2>Flyguide Version: 1.0(b)</h2><br /><br />
<b>Testing Mode:</b> <a href="settings.php?on">On</a> | <a href="settings.php?off">Off</a>
	

</div>

<div style="clear:both;"></div>
<?php include"messages-display.php";?>

<?php
$sql = "SELECT * FROM ".ADMIN." WHERE id = '1'";
$res = mysql_query($sql) or die("Error:".__LINE__.mysql_error());
while($row=mysql_fetch_array($res))
{
?>

<form id="update-form" name="update-form" method="post" action="" autocomplete="off">
  <table class="form-table">
    <tr>
      <th width="125" scope="row">Site Name </th>
      <td colspan="2"><input name="site_name" type="text" value="<?=stripslashes($row['site_name'])?>" size="35" />      </td>
    </tr>
    <tr>
      <th scope="row">First Name </th>
      <td colspan="2"><input name="admin_firstname" type="text" value="<?=stripslashes($row['admin_firstname'])?>" size="20" />      </td>
    </tr>
    <tr>
      <th scope="row">Last Name </th>
      <td colspan="2"><input name="admin_lastname" type="text" value="<?=stripslashes($row['admin_lastname'])?>" size="20" />      </td>
    </tr>
    <tr>
      <th scope="row"> Email </th>
      <td colspan="2"><input name="site_email" type="text" value="<?=stripslashes($row['site_email'])?>" size="35" />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="recipients-body-settings.php" rel="gb_page_center[670, 280]" class="button tooltip"><span class="ui-icon ui-icon-newwin"></span>Add More Recipients</a>      </td>
    </tr>
    <tr>
      <th scope="row">Site URL </th>
      <td colspan="2"><input name="site_url" type="text" value="<?=stripslashes($row['site_url'])?>" size="35" /></td>
    </tr>
    <tr>
      <th scope="row">Login</th>
      <td colspan="2"><input name="username" type="text" value="<?=stripslashes($row['username'])?>" size="20" /></td>
    </tr>
    <tr>
      <th scope="row">Password</th>
      <td colspan="2"><input name="password" id="password" type="password" value="" size="20" onkeyup="checkPasswordMatch();" onblur="checkPasswordMatch();" />Only enter a new password if you want to change current password,  if not, leave blank</td>
    </tr>
    <tr>
      <th scope="row">Confirm Password </th>
      <td width="100"><input name="password_confirm" id="password_confirm" type="password" value="" size="20" onkeyup="checkPasswordMatch();" onblur="checkPasswordMatch();" /></td>
      <td><div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
	  <script>
		function checkPasswordMatch() {
			var password = $("#password").val();
			var confirmPassword = $("#password_confirm").val();
		
			if (password != confirmPassword)
				$("#divCheckPasswordMatch").html("<font color='#ff0000'>Password does not match!</font>");
			else
				$("#divCheckPasswordMatch").html("<img src='ok.gif'>");
		}
		</script></td>
    </tr>
    
    <tr>
      <th scope="row">Max # of Guides</th>
      <td colspan="2"><input name="max_guides" id="max_guides" type="text" value="<?=$row['max_guides']?>" size="5" /></td>
    </tr>

    
	<!--<tr>
      <th scope="row">Sales Tax </th>
      <td colspan="2"><input name="sales_tax_rate" type="text" value="<?=stripslashes($row['sales_tax_rate'])?>" size="5" />
      %</td>
    </tr>-->
    <tr>
      <th>Gateway Mode </th>
      <td colspan="2"><label>
        <input name="gateway_mode" type="radio" value="live" <?php if($row['gateway_mode']=="live") echo 'checked="checked"';?> />
        Live</label>
        <label>
        <input name="gateway_mode" type="radio" value="demo" <?php if($row['gateway_mode']=="demo") echo 'checked="checked"';?> />
        Demo </label></td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2"><a href="settings-smtp.php" rel="gb_page_center[400, 400]">SMTP Settings</a> </td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
    <td></td>
	<td colspan="2"><a href="api-settings.php" rel="gb_page_center[400, 400]" style="font-family: Tahoma, Verdana, Arial, sans-serif; font-size:12px;">API Settings</a> </td>
	</tr>
     <tr>
      <td></td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2"><a href="settings-database-backup.php" rel="gb_page_center[400, 400]">Database Backup Settings</a> </td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2">&nbsp;</td>
    </tr>
    

	<tr>
    <td>&nbsp;</td>
	<td scope="row"><a href="reset-block.php" rel="gb_page_center[400, 340]" style="font-family: Tahoma, Verdana, Arial, sans-serif; font-size:12px;">Reset Block</a> </td>
	</tr>
    <tr>
      <td></td>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td colspan="2"><input type="submit" name="btnUpdate" value="Update" class="button" /></td>
    </tr>
  </table>
</form>

<?php } ?>