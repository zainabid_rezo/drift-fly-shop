<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_GET["month"])) $_GET["month"] = date("n");
if (!isset($_GET["year"])) $_GET["year"] = date("Y");
?>

<?php
$cMonth = $_GET["month"];
$cYear = $_GET["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear - 1;
$next_year = $cYear + 1;
?>
<?php
$link_previous = date("Y",mktime(0,0,0,$prev_month,1,$prev_year));
$link_next = date("Y",mktime(0,0,0,$next_month,1,$next_year));

$search_from = $_GET['year'].'-01-01';
$search_to = date('Y-m-d');
?>
<!DOCTYPE html>
<html>
<head>
<title>Chart</title>
<link class="include" rel="stylesheet" type="text/css" href="chart_src/jquery.jqplot.min.css" />
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="chart_src/excanvas.js"></script><![endif]-->
<script class="include" type="text/javascript" src="chart_src/jquery.min.js"></script>
<script class="include" type="text/javascript" src="chart_src/jquery.jqplot.min.js"></script>

<script class="include" type="text/javascript" src="chart_src/jqplot.pieRenderer.min.js"></script>

<script type="text/javascript" src="chart_src/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.pointLabels.min.js"></script>

</head>
<body>
<table border="0" align="center" style="width:100%">
  <tr>
    <td width="90%" align="center" valign="top"><?=date('m/d/Y',strtotime($search_from))?> 
      through 
    <?=date('m/d/Y',strtotime($search_to))?> </td>
    <td width="10%" align="center" valign="top"><a href="reports-guides.php">Monthly</a></td>
  </tr>
</table>

<?php
$trip_ids = array();
$sql = "SELECT * FROM ".RESERVATIONS." WHERE date >= '".$search_from."' AND date <= '".$search_to."' AND cancelled != '1' AND status = '1' ORDER by trip_id ASC";
$result= mysql_query($sql);
while($row = mysql_fetch_array($result)){
if(!in_array($row['trip_id'],$trip_ids)){array_push($trip_ids,$row['trip_id']);}
}
?>
<?php if(count($trip_ids)=="0"){die("<br/><br/><br/><br/><br/><center>No trips found for this year!</center>");}?>
<?php
$maxdays = date("t",mktime(0,0,0,$cMonth,1,$cYear)); 
?>

<div id="chart-guides-pie" style="margin:auto; width:760px; height:350px;"></div>

<script type="text/javascript">$(document).ready(function(){
		  plot2 = jQuery.jqplot('chart-guides-pie', 
			[[
			<?php
			$guide_ids = array();
			$sql = "SELECT * FROM ".RESERVATIONS." WHERE date >= '".$search_from."' AND date <= '".$search_to."' AND cancelled != '1' AND status = '1' ORDER by guide_id ASC";
			$result= mysql_query($sql);
			while($row = mysql_fetch_array($result)){
			
			
			if($row['guide_id']<>'0' AND !in_array($row['guide_id'],$guide_ids)){array_push($guide_ids,$row['guide_id']);}
			if($row['guide_id_2']<>'0' AND !in_array($row['guide_id_2'],$guide_ids)){array_push($guide_ids,$row['guide_id_2']);}
			if($row['guide_id_3']<>'0' AND !in_array($row['guide_id_3'],$guide_ids)){array_push($guide_ids,$row['guide_id_3']);}
			
			}
			
				if(count($trip_ids)>"0"){
					foreach($guide_ids as $guide_id){
						
						$total_jobs = '0';
						$sql = "SELECT * FROM ".RESERVATIONS." WHERE date >= '".$search_from."' AND date <= '".$search_to."' AND (guide_id = '$guide_id' OR guide_id_2 = '$guide_id' OR guide_id_3 = '$guide_id') AND status = '1'";
						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							$total_jobs ++;
						}
						echo "['".get_guide_name($guide_id)." &nbsp;&nbsp;&nbsp;&nbsp; Jobs: ".$total_jobs."' , ".$total_jobs."], ";
			
					}
				}else{
					echo "['No reservations found for this year', 100]";	
				}
			?>
			]], 
			{
			  title: ' ', 
			  seriesDefaults: {
				shadow: false, 
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: { 
				  startAngle: 180, 
				  sliceMargin: 4, 
				  showDataLabels: true } 
			  }, 
			  legend: { show:true, location: 'w' }
			}
		  );
		});
</script>

<br>
<br>

</body>
</html>

