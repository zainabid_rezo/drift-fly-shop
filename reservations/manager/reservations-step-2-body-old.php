<style>
#modal {
	display:none;
	width:500px;
	/*min-height:478px;*/
	padding:8px;

	background:#fcfcfc;

	-webkit-border-radius:8px;
	-moz-border-radius:8px;
	border-radius:8px;

	position:absolute !important;
	top:22% !important;
	left:50% !important;
	margin-top:-94px !important;
	margin-left:-250px !important;
	z-index:101;
}



.reveal-modal-bg { 
	position: fixed; 
	height: 100%;
	width: 100%;
	z-index: 100;
	display: none;
	top: 0;
	left: 0; 
}

.opaci{
	background:#000;
	opacity:0.2;
	width:100%;
	height:100%;
}
</style>
<script type="text/javascript">
    
    
	
	function unloadPopupBox() {    // TO Unload the Popupbox
            $('#modal').fadeOut("fast");
			$('#opacit').removeClass('opaci');
           
        }    
        
        function loadPopupBox() {    // To Load the Popupbox
            $('#modal').fadeIn("slow");
			$('#opacit').addClass('opaci');
                     
        }
		
		$(document).ready(function(e) {
            $('.reveal-modal-bg').click( function() {            
            unloadPopupBox();
			$('.reveal-modal-bg').css('display','none');
        });
		
		$('#popupBoxClose').click( function() {            
            unloadPopupBox();
			$('.reveal-modal-bg').css('display','none');
        });
        
        $('#container').click( function() {
            unloadPopupBox();
        });
        });
		</script>
<?php
if(isset($_POST['btnContinue'])){

$dates = array();

for ($i=0; $i<count($_POST['datestring']);$i++) {

		$datecheckbox = $_POST['datestring'][$i];
		$datestring = str_replace("_", "-", $datecheckbox);
		$datestring = date("Y-m-d",strtotime($datestring));
		if(isset($_POST["date_$datecheckbox"])){
			array_push($dates, $datestring);
		}
		
}



$dates = implode(',',$dates);

$count_guide_for_date = get_guide_total_for_date($dates);

if($count_guide_for_date >= MAX_GUIDES){
?>
<script type="text/javascript">
	$(document).ready( function() {
    
	//alert('1'); return false;
        // When site loaded, load the Popupbox First
        loadPopupBox();
    
      	
    });
</script>
<?php
}else{
	echo '<script>window.location="reservations.php?trip_id='.$_GET['trip_id'].'&date='.$dates.'";</script>';
}

/*echo '<script>window.location="reservations.php?trip_id='.$_GET['trip_id'].'&date='.$dates.'";</script>';*/
}
?>


<div id="modal">    
	<!--<span class="close" style="position:absolute; margin-left:487px; margin-top:-12px;"><a id="popupBoxClose" style=" cursor: pointer;"> <img src="close.png" alt="" /></a>
    </span>-->
    
    <div style="width:350px; height:120px; color:black;">
    
    <div style="width:480px; margin-left:5px; margin-top:40px; color:black; float:left;">
	<h2>This day is Fully Booked with <?=MAX_GUIDES?> Guides</h2>
    <br />
    <a href="javascript:void(0);" id="popupBoxClose" style=" cursor: pointer;"><input type="button" value="Cancel" class="button" /></a>
    &nbsp;&nbsp;&nbsp;
    <a href="reservations.php?trip_id=<?=$_GET['trip_id']?>&date=<?=$dates?>" style=" cursor: pointer;"><input type="button" value="Book Anyway" class="button" /></a>
    </div>
    
    </div>
    
    
    
</div><!--</div>
</div>-->
<div class="reveal-modal-bg" id="opacit" style="display: block;"></div>

<h2>Select a date</h2>
<?php $trip_id = $_GET['trip_id'];?>

<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
?>

<?php
$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-4;
$next_month = $cMonth+4;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>
<form action="" method="post">

<table border="0" align="center" style="width:620px">
  <tr>
    <td align="center" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"]."?trip_id=".$trip_id."&month=".$prev_month."&year=".$prev_year; ?>" >Previous</a></td>
    <td align="center" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"]."?trip_id=".$trip_id."&month=".$next_month."&year=".$next_year; ?>" >Next</a></td>
  </tr>
  <tr>
    <td align="center" valign="top"><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);	
		
		if($row_trip_check['rc_year'] != ''){	

			if(day_availability($trip_id,$cToday)!==true){
				$dateColor = "#ffffff";
				$status = 'disabled';
			}else{
				$dateColor = "#00aa00";
				$status = '';
			}
		}else{
			$dateColor = "#00aa00";
				$status = '';
		}
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo $cDay;
		echo "<input name=\"date_$cTodayId\" type=\"checkbox\" value=\"YES\" $status/>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" $status/>";
		echo "</td>\n";
	
		

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;		
		
			$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);	
		
		if($row_trip_check['rc_year'] != ''){	

			if(day_availability($trip_id,$cToday)!==true){
				$dateColor = "#ffffff";
				$status = 'disabled';
			}else{
				$dateColor = "#00aa00";
				$status = '';
			}
		}else{
			$dateColor = "#00aa00";
				$status = '';
		}
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo $cDay;
		echo "<input name=\"date_$cTodayId\" type=\"checkbox\" value=\"YES\" $status/>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" $status/>";
		echo "</td>\n";
	
		

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><input type="submit" name="btnContinue" class="button" value="Continue" /></td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;		
		
			$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);	
		
		if($row_trip_check['rc_year'] != ''){	

			if(day_availability($trip_id,$cToday)!==true){
				$dateColor = "#ffffff";
				$status = 'disabled';
			}else{
				$dateColor = "#00aa00";
				$status = '';
			}
		}else{
			$dateColor = "#00aa00";
				$status = '';
		}
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo $cDay;
		echo "<input name=\"date_$cTodayId\" type=\"checkbox\" value=\"YES\" $status/>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" $status/>";
		echo "</td>\n";
	
		

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;		
			
			$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);	
		
		if($row_trip_check['rc_year'] != ''){	

			if(day_availability($trip_id,$cToday)!==true){
				$dateColor = "#ffffff";
				$status = 'disabled';
			}else{
				$dateColor = "#00aa00";
				$status = '';
			}
		}else{
			$dateColor = "#00aa00";
				$status = '';
		}
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo $cDay;
		echo "<input name=\"date_$cTodayId\" type=\"checkbox\" value=\"YES\" $status/>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" $status/>";
		echo "</td>\n";
	
		

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
  <tr>
    <td valign="top"><table border="0">
      <tr>
        <td width="20" bgcolor="#00aa00">&nbsp;</td>
        <td>Dates in Green have trips available</td>
      </tr>
    </table>
      <table border="0">
        <tr>
          <td width="20" style="border:#333333 solid 1px; background-color:#ffffff">&nbsp;</td>
          <td>Dates in white have no trips available</td>
        </tr>
    </table></td>
    
  </tr>
</table>

</form>