<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_GET["year"])) $_GET["year"] = date("Y");
?>

<?php
$cYear = $_GET["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear - 1;
$next_year = $cYear + 1;
?>
<?php
$link_previous = date("Y",mktime(0,0,0,$prev_month,1,$prev_year));
$link_next = date("Y",mktime(0,0,0,$next_month,1,$next_year));


$search_from = $_GET['year'].'-01-01';
$search_to = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
<title>Chart</title>
<link class="include" rel="stylesheet" type="text/css" href="chart_src/jquery.jqplot.min.css" />
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="chart_src/excanvas.js"></script><![endif]-->
<script class="include" type="text/javascript" src="chart_src/jquery.min.js"></script>
<script class="include" type="text/javascript" src="chart_src/jquery.jqplot.min.js"></script>

<script class="include" type="text/javascript" src="chart_src/jqplot.pieRenderer.min.js"></script>

<script type="text/javascript" src="chart_src/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.pointLabels.min.js"></script>

</head>
<body>
<table border="0" align="center" style="width:100%">
  <tr>
    <td width="90%" align="center" valign="top"><?=date('m/d/Y',strtotime($search_from))?> through <?=date('m/d/Y',strtotime($search_to))?> </td>
    <td width="10%" align="center" valign="top"><a href="reports-trips.php">Monthly</a></td>
  </tr>
</table>

<?php
$trip_ids = array();
$sql = "SELECT * FROM ".RESERVATIONS." WHERE date >= '".$search_from."' AND date <= '".date('Y-m-d')."' AND status = '1' ORDER by trip_id ASC";
$result= mysql_query($sql);
while($row = mysql_fetch_array($result)){

if(!in_array($row['trip_id'],$trip_ids)){array_push($trip_ids,$row['trip_id']);}

}
?>
<?php if(count($trip_ids)=="0"){die("<br/><br/><br/><br/><br/><center>No trips found for this year!</center>");}?>
<?php
$maxdays = date("t",mktime(0,0,0,$cMonth,1,$cYear)); 
?>

<div id="chart-triptypes-pie" style="margin:auto; width:760px; height:350px;"></div>

<script type="text/javascript">$(document).ready(function(){
		  plot2 = jQuery.jqplot('chart-triptypes-pie', 
			[[
			<?php
			$trip_ids = array();
			$sql = "SELECT * FROM ".RESERVATIONS." WHERE date >= '".$search_from."' AND date <= '".date('Y-m-d')."' AND status = '1' ORDER by trip_id ASC";
			$result= mysql_query($sql);
			while($row = mysql_fetch_array($result)){
			
			
			if(!in_array($row['trip_id'],$trip_ids)){array_push($trip_ids,$row['trip_id']);}

			}
			
				if(count($trip_ids)>"0"){
					foreach($trip_ids as $trip_id){
						
						$total_occurrences = '0';
						$sql = "SELECT * FROM ".RESERVATIONS." WHERE trip_id = '$trip_id' AND status = '1'";
						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							$total_occurrences ++;
						}
						echo "['".get_trip_name($trip_id)." &nbsp;&nbsp;&nbsp;&nbsp; Occurrences: ".$total_occurrences."' , ".$total_occurrences."], ";
			
					}
				}else{
					echo "['No reservations found for this month', 100]";	
				}
			?>
			]], 
			{
			  title: ' ', 
			  seriesDefaults: {
				shadow: false, 
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: { 
				  startAngle: 180, 
				  sliceMargin: 4, 
				  showDataLabels: true } 
			  }, 
			  legend: { show:true, location: 'w' }
			}
		  );
		});
</script>

</body>
</html>

