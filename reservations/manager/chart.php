<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
?>

<?php
$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>
<?php
$link_previous = date("F",mktime(0,0,0,$prev_month,1,$prev_year));
$link_next = date("F",mktime(0,0,0,$next_month,1,$next_year));
?>
<!DOCTYPE html>
<html>
<head>
<title>Chart</title>
<link class="include" rel="stylesheet" type="text/css" href="chart_src/jquery.jqplot.min.css" />
<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="chart_src/excanvas.js"></script><![endif]-->
<script class="include" type="text/javascript" src="chart_src/jquery.min.js"></script>
<script class="include" type="text/javascript" src="chart_src/jquery.jqplot.min.js"></script>

<script class="include" type="text/javascript" src="chart_src/jqplot.pieRenderer.min.js"></script>

<script type="text/javascript" src="chart_src/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="chart_src/jqplot.pointLabels.min.js"></script>

</head>
<body>
<table border="0" align="center" style="width:1000px">
  <tr>
    <td width="33%" align="right" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $prev_month . "&year=" . $prev_year; ?>" >&lsaquo;&lsaquo;<?=$link_previous?></a></td>
    <td width="33%" align="center" valign="top"><h2><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></h2></td>
    <td width="33%" align="left" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $next_month . "&year=" . $next_year; ?>" ><?=$link_next?>&rsaquo;&rsaquo;</a></td>
  </tr>
</table>



<h3 align="center">Estimated Income</h3>
<div id="chart-estimated-income-bar" style="margin:auto; width:1100px; height:650px;"></div>

<?php
$trip_ids = array();
$sql = "SELECT * FROM ".RESERVATIONS." WHERE status = '1' ORDER by trip_id ASC";
$result= mysql_query($sql);
while($row = mysql_fetch_array($result)){

if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){

if(!in_array($row['trip_id'],$trip_ids)){array_push($trip_ids,$row['trip_id']);}
}
}
?>
<?php
$maxdays = date("t",mktime(0,0,0,$cMonth,1,$cYear)); 
?>
<script type="text/javascript">
$(document).ready(function(){

	<?php
	if(count($trip_ids)>"0"){
	foreach($trip_ids as $trip_id){
		echo "var s".$trip_id." = [";
		
			for ($i=0; $i<$maxdays; $i++) {
			$D = date("Y-m-d",strtotime($cYear."-".$cMonth."-".($i+1)));
			$total_today = '0';
			$sql = "SELECT * FROM ".RESERVATIONS." WHERE trip_id = '$trip_id' AND status = '1' AND date='$D' ";
			$result= mysql_query($sql) or die(__LINE__."$sql <br/>");
			while($row = mysql_fetch_array($result)){
				$total_today += $row['calculated_price'];
			}
			
			
			echo "$total_today, ";
			}

		echo "];
		";
	}
	}
	?>
	
	
    // Can specify a custom tick Array.
    // Ticks should match up one for each y value (category) in the series.
    var ticks = [<?php for ($i=0; $i<$maxdays; $i++) { echo $i+1; echo ", "; }?>];
     
    var plot1 = $.jqplot('chart-estimated-income-bar', [<?php if(count($trip_ids)>"0"){foreach($trip_ids as $trip_id){echo "s".$trip_id.", ";}}?>], {

        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
			
			<?php
			if(count($trip_ids)>"0"){
					foreach($trip_ids as $trip_id){
						
						$total_price = '0';
						$sql = "SELECT * FROM ".RESERVATIONS." WHERE trip_id = '$trip_id' AND status = '1'";
						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
							$total_price += $row['calculated_price'];
							}
						}
						echo "{label:'".get_trip_name($trip_id)." &nbsp;&nbsp;&nbsp;&nbsp; \$".$total_price."'}, 
						";
			
					}
				}else{
					echo "{label: 'No reservations found for this month'}";	
				}
			?>
	
        ],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid',
			location: 'n',
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                tickOptions: {formatString: '$%d'}
            }
        }
    });
});
</script>


<div id="chart-estimated-income-pie" style="margin:auto; width:760px; height:350px;"></div>


<script type="text/javascript">$(document).ready(function(){
		  plot2 = jQuery.jqplot('chart-estimated-income-pie', 
			[[
			<?php
			$trip_ids = array();
			$sql = "SELECT * FROM ".RESERVATIONS." WHERE status = '1' ORDER by trip_id ASC";
			$result= mysql_query($sql);
			while($row = mysql_fetch_array($result)){
			
			if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
			
			if(!in_array($row['trip_id'],$trip_ids)){array_push($trip_ids,$row['trip_id']);}
			}
			}
			
				if(count($trip_ids)>"0"){
					foreach($trip_ids as $trip_id){
						
						$total_price = '0';
						$sql = "SELECT * FROM ".RESERVATIONS." WHERE trip_id = '$trip_id' AND status = '1'";
						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
							$total_price += $row['calculated_price'];
							}
						}
						echo "['".get_trip_name($trip_id)." &nbsp;&nbsp;&nbsp;&nbsp; \$".$total_price."' , ".$total_price."], ";
			
					}
				}else{
					echo "['No reservations found for this month', 100]";	
				}
			?>
			]], 
			{
			  title: ' ', 
			  seriesDefaults: {
				shadow: false, 
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: { 
				  startAngle: 180, 
				  sliceMargin: 4, 
				  showDataLabels: true } 
			  }, 
			  legend: { show:true, location: 'w' }
			}
		  );
		});
</script>

<hr/>


<h3 align="center">Trip Types</h3>

<div id="chart-triptypes-pie" style="margin:auto; width:760px; height:350px;"></div>


<script type="text/javascript">$(document).ready(function(){
		  plot2 = jQuery.jqplot('chart-triptypes-pie', 
			[[
			<?php
			$trip_ids = array();
			$sql = "SELECT * FROM ".RESERVATIONS." WHERE status = '1' ORDER by trip_id ASC";
			$result= mysql_query($sql);
			while($row = mysql_fetch_array($result)){
			
			if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
			
			if(!in_array($row['trip_id'],$trip_ids)){array_push($trip_ids,$row['trip_id']);}
			}
			}
			
				if(count($trip_ids)>"0"){
					foreach($trip_ids as $trip_id){
						
						$total_occurrences = '0';
						$sql = "SELECT * FROM ".RESERVATIONS." WHERE trip_id = '$trip_id' AND status = '1'";
						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
							$total_occurrences ++;
							}
						}
						echo "['".get_trip_name($trip_id)." &nbsp;&nbsp;&nbsp;&nbsp; Occurrences: ".$total_occurrences."' , ".$total_occurrences."], ";
			
					}
				}else{
					echo "['No reservations found for this month', 100]";	
				}
			?>
			]], 
			{
			  title: ' ', 
			  seriesDefaults: {
				shadow: false, 
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: { 
				  startAngle: 180, 
				  sliceMargin: 4, 
				  showDataLabels: true } 
			  }, 
			  legend: { show:true, location: 'w' }
			}
		  );
		});
</script>

<hr/>

<h3 align="center">Guides</h3>

<div id="chart-guides-pie" style="margin:auto; width:760px; height:350px;"></div>


<script type="text/javascript">$(document).ready(function(){
		  plot2 = jQuery.jqplot('chart-guides-pie', 
			[[
			<?php
			$guide_ids = array();
			$sql = "SELECT * FROM ".RESERVATIONS." WHERE status = '1' ORDER by guide_id ASC";
			$result= mysql_query($sql);
			while($row = mysql_fetch_array($result)){
			
			if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
			
			if(!in_array($row['guide_id'],$guide_ids)){array_push($guide_ids,$row['guide_id']);}
			}
			}
			
				if(count($trip_ids)>"0"){
					foreach($guide_ids as $guide_id){
						
						$total_jobs = '0';
						$sql = "SELECT * FROM ".RESERVATIONS." WHERE guide_id = '$guide_id' AND status = '1'";
						$result= mysql_query($sql);
						while($row = mysql_fetch_array($result)){
							if(date("n",strtotime($row['date']))==$_GET['month'] AND date("Y",strtotime($row['date']))==$_GET['year']){
							$total_jobs ++;
							}
						}
						echo "['".get_guide_name($guide_id)." &nbsp;&nbsp;&nbsp;&nbsp; Jobs: ".$total_jobs."' , ".$total_jobs."], ";
			
					}
				}else{
					echo "['No reservations found for this month', 100]";	
				}
			?>
			]], 
			{
			  title: ' ', 
			  seriesDefaults: {
				shadow: false, 
				renderer: jQuery.jqplot.PieRenderer, 
				rendererOptions: { 
				  startAngle: 180, 
				  sliceMargin: 4, 
				  showDataLabels: true } 
			  }, 
			  legend: { show:true, location: 'w' }
			}
		  );
		});
</script>

<br>
<br>

</body>
</html>

