<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php"); 
?>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
body,td,th {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px
}
body {
	 background:#fff;
	 margin:20px;
}

@media print {
.noPrint {
    display:none;
}
</style>


<script type="text/javascript">
function printpage()
{
window.print();
}
</script>
</head>
<body link="#99CC00" id="top">

<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
?>

<?php
$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-4;
$next_month = $cMonth+4;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>

<?php if(!isset($_GET['date'])){?>

<table border="0" align="center" style="width:620px">
  <tr>
    <td valign="top"><h2>Select a date</h2>
    </td>
    <td align="center" valign="top"><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		

				$dateColor = "#dddddd";
				$link_start = "<a href=\"reports-daily.php?date=$cToday\">";
				$link_end = "<a>";
				
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		

				$dateColor = "#dddddd";
				$link_start = "<a href=\"reports-daily.php?date=$cToday\">";
				$link_end = "<a>";
				
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
  <tr>
    <td valign="bottom"><a href="<?php echo $_SERVER["PHP_SELF"]."?trip_id=".$trip_id."&numberofpeople=".$numberofpeople."&month=".$prev_month."&year=".$prev_year; ?>" >Previous</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo $_SERVER["PHP_SELF"]."?trip_id=".$trip_id."&numberofpeople=".$numberofpeople."&month=".$next_month."&year=".$next_year; ?>" >Next</a></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		

				$dateColor = "#dddddd";
				$link_start = "<a href=\"reports-daily.php?date=$cToday\">";
				$link_end = "<a>";
				
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		

				$dateColor = "#dddddd";
				$link_start = "<a href=\"reports-daily.php?date=$cToday\">";
				$link_end = "<a>";
				
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
</table>

<?php
}else{
?>

<div style="float:back" class="noPrint" ><a href="reports-daily.php">&lsaquo;&lsaquo;Back</a></div>
<div style="float:right"><input type="button" value="Print this page" onClick="printpage()" class="noPrint button" /></div>

<h2><?=date("m/d/Y",strtotime($_GET['date']))?></h2>
<h3>Daily Report</h3>

<table width="400" border="0">
  <tr>
    <td width="20%" bgcolor="#DDDDDD">Package</td>
    <td width="20%" bgcolor="#DDDDDD">Qty Rented</td>
  </tr>
<?php
$sqlCAT = "SELECT * FROM ".CATEGORIES." ORDER BY sortby ASC";
$resultCAT= mysql_query($sqlCAT);
while($rowCAT = mysql_fetch_array($resultCAT)){
$sqlp = "SELECT * FROM ".PACKAGES." WHERE category_id = '$rowCAT[id]' ORDER BY sortby ASC";
$resp = mysql_query($sqlp) or die(__LINE__.mysql_error());
while($rowp=mysql_fetch_array($resp))
{
$sql = "SELECT * FROM ".RENTERSDETAIL." WHERE package_id = '$rowp[id]' AND status = 'confirmed' AND date_start <= '".date("Y-m-d",strtotime($_GET['date']))."' AND date_end >= '".date("Y-m-d",strtotime($_GET['date']))."' ";
$res = mysql_query($sql) or die(__LINE__.mysql_error());
$qty_rented = mysql_num_rows($res);
if($qty_rented>'0')
{
?>
  <tr>
    <td><?=stripslashes($rowp['title'])?></td>
    <td><?=$qty_rented?></td>
  </tr>
<?php
}
}
}
?>
</table>
<?php

}
?>
</body>
</html>