<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
?>

<?php
$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];
?>
<?php
function create_html_color($shades = "dark") {
	if($shades=="light"){
	$validCharacters = "89ABCDEF";
	}elseif($shades=="dark"){
	$validCharacters = "012345678";
	}
    $validCharNumber = strlen($validCharacters);
    $result = ""; 
    for ($i = 0; $i < 6; $i++) {
        $index = mt_rand(0, $validCharNumber - 1);
        $result .= $validCharacters[$index];
    }
    return "#".$result;
}
?>
<?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-1;
$next_month = $cMonth+1;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>
<?php
$link_previous = date("F",mktime(0,0,0,$prev_month,1,$prev_year));
$link_next = date("F",mktime(0,0,0,$next_month,1,$next_year));
?>

<?php
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
$date_start = date('Y-m-d',$timestamp);
$date_end = date('Y-m',$timestamp).'-'.$maxday;

$guides_list = array();
$sqlResv = "SELECT * FROM ".RESERVATIONS." WHERE status = '1' AND date >= '".$date_start."' AND date <= '".$date_end."'";
$resultResv= mysql_query($sqlResv) or die(mysql_error());
while($rowResv = mysql_fetch_array($resultResv)){
	$sel_guide = mysql_query("select * from reservation_guide where reserv_id = '".$rowResv['id']."'") or die(mysql_error());
	while($row_giud = mysql_fetch_array($sel_guide)){
	
	$guide_id = $row_giud['guide_id'];
	/*$guide_id_2 = $rowResv['guide_id_2'];
	$guide_id_3 = $rowResv['guide_id_3'];*/
	
	if(!in_array($guide_id,$guides_list)){
		array_push($guides_list,$guide_id);
	}
}
	/*if(!in_array($guide_id_2,$guides_list)){
		array_push($guides_list,$guide_id_2);
	}
	if(!in_array($guide_id_3,$guides_list)){
		array_push($guides_list,$guide_id_3);
	}*/
	
	
}


?>
<table>
<tr>
<td>
<?php
foreach($guides_list as $guide_id){
	
	if($guide_id<>'0'){
	$guide_name = get_guide_name($guide_id);
	$guide_color = get_guide_color($guide_id);
	echo "<div style='clear:left; padding:3px;'><div style='background-color:$guide_color; height:16px; width:16px; float:left; margin:3px;'></div>".$guide_name."</div>";
	}
}
?>
</td>
<td><table border="0" align="center" style="width:1000px;">
  <tr>
    <td width="33%" align="right" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $prev_month . "&year=" . $prev_year; ?>" >&lsaquo;&lsaquo;<?=$link_previous?></a></td>
    <td width="33%" align="center" valign="top"><h2><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></h2></td>
    <td width="33%" align="left" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $next_month . "&year=" . $next_year; ?>" ><?=$link_next?>&rsaquo;&rsaquo;</a></td>
  </tr>
  <tr>
    <td align="center" valign="top" colspan="3">
	
	
	<table border="1" cellpadding="2" cellspacing="1" bordercolor="#666666" style="width:1000px">
      <tr style='height:25px'>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>SU</strong></td>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>MO</strong></td>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>TU</strong></td>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>WE</strong></td>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>TH</strong></td>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>FR</strong></td>
        <td align="center" bgcolor="#313419" style="color:#FFFFFF" width="14%"><strong>SA</strong></td>
      </tr>
      <?php 
		
		for ($i=0; $i<($maxday+$startday); $i++) {
			if(($i % 7) == 0 ) {echo "<tr style='height:40px' >\n";}
			if($i < $startday) {echo "<td></td>\n";}
			else {
		
				$cDay = ($i - $startday + 1);
				$cToday = $cYear."-".$cMonth."-".$cDay;
				$cTodayId = $cYear."_".$cMonth."_".$cDay;
	
				
				
				echo '<td align="center" valign="middle">'; // bgcolor="#cccccc"
				echo $cDay;
				echo "<br/>";
				$resultResv= mysql_query("SELECT * FROM ".RESERVATIONS." WHERE status = '1' AND date = '".$cToday."'") or die(mysql_error());
				while($rowResv = mysql_fetch_array($resultResv)){
					$trip_id = $rowResv['trip_id'];
					$reservation_id = $rowResv['id'];
					$guest_name = get_guest_name($rowResv['id']);
					$notes  = $rowResv['notes'];
					
					if($rowResv['notes'] == ''){
						$sel_notes = mysql_query("select * from reservation_notes where reserv_id = '$reservation_id'") or die(mysql_error());
						$row_notes = mysql_fetch_array($sel_notes);
						
						$notes = $row_notes['notes'];
					}
					
					$sel_guidee = mysql_query("select * from reservation_guide where reserv_id = '".$reservation_id."'") or die(mysql_error());
				while($row_giudd = mysql_fetch_array($sel_guidee)){
					if($row_giudd['guide_id']<>'0'){
					$guide_color = get_guide_color($row_giudd['guide_id']);
					echo "<a href=\"reservations-view.php?id=$reservation_id\" class=\"button tooltip\" style=\"background-color:$guide_color; width:100px; text-align:center; margin:2px;\" title=\"".
					"Customer: ".$guest_name."<br/>".
					"# of people: ".$rowResv['number_of_people']."<br/>".
					"Trip Type: ".get_trip_name($rowResv['trip_id'])."<br/>".
					"Time: ".$rowResv['meeting_time']."<br/>".
					"Location: ".get_location_name($rowResv['meeting_location'])."<br/>".
					"Guide: ".get_guide_name($row_giudd['guide_id'])."<br/>".
					"Notes: ".nl2br(stripslashes($notes))."<br/>".
					
					"\">";
					echo "<span style=\"width:105px; height:20px;\">";
					echo '<span class="ui-icon ui-icon-person" style="background-color:'.$guide_color.'"></span>';
					echo $guest_name;
					echo "</span>";
					echo "</a>";
					echo "<br/>";
					}
				}
					/*if($rowResv['guide_id_2']<>'0'){
					$guide_color = get_guide_color($rowResv['guide_id_2']);
					echo "<a href=\"reservations-view.php?id=$reservation_id\" class=\"button tooltip\" style=\"background-color:$guide_color; width:100px; text-align:center; margin:2px;\" title=\"".
					"Customer: ".$guest_name."<br/>".
					"# of people: ".$rowResv['number_of_people']."<br/>".
					"Trip Type: ".get_trip_name($rowResv['trip_id'])."<br/>".
					"Time: ".$rowResv['meeting_time']."<br/>".
					"Location: ".get_location_name($rowResv['meeting_location'])."<br/>".
					"Guide: ".get_guide_name($rowResv['guide_id_2'])."<br/>".
					"Notes: ".nl2br(stripslashes($notes))."<br/>"
					."\">";
					echo "<span style=\"width:105px; height:20px;\">";
					echo '<span class="ui-icon ui-icon-person" style="background-color:'.$guide_color.'"></span>';
					echo $guest_name;
					echo "</span>";
					echo "</a>";
					echo "<br/>";
					}
					if($rowResv['guide_id_3']<>'0'){
					$guide_color = get_guide_color($rowResv['guide_id_3']);
					echo "<a href=\"reservations-view.php?id=$reservation_id\" class=\"button tooltip\" style=\"background-color:$guide_color; width:100px; text-align:center; margin:2px;\" title=\"".
					"Customer: ".$guest_name."<br/>".
					"# of people: ".$rowResv['number_of_people']."<br/>".
					"Trip Type: ".get_trip_name($rowResv['trip_id'])."<br/>".
					"Time: ".$rowResv['meeting_time']."<br/>".
					"Location: ".get_location_name($rowResv['meeting_location'])."<br/>".
					"Guide: ".get_guide_name($rowResv['guide_id_3'])."<br/>".
					"Notes: ".nl2br(stripslashes($notes))."<br/>"
					."\">";
					echo "<span style=\"width:105px; height:20px;\">";
					echo '<span class="ui-icon ui-icon-person" style="background-color:'.$guide_color.'"></span>';
					echo $guest_name;
					echo "</span>";
					echo "</a>";
					echo "<br/>";
					}*/
					
					
					
				}
				echo "</td>\n";
		
				unset($cDayStatus);
				unset($dateColor);
				unset($owner);
				unset($cToday);
				unset($cTodayId);
		
			
			}
			if(($i % 7) == 6 ) {echo "</tr>\n";}
		}
		?>
	  </table>	</td>
  </tr>
</table></td>
</tr>
</table>




