<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body,td,th {
	font-family:Arial, Helvetica, sans-serif;
	font-size:11px
}
@media print {
.noPrint {
    display:none;
}
}
-->
</style>
<script type="text/javascript">
function printpage()
{
window.print();
}
</script>
</head>
<body id="top">


<script type="text/javascript">
function printpage()
{
window.print();
}
</script>
<div align="right"><input type="button" value="Print this page" onClick="printpage()" class="noPrint" /></div>

<h2>Trip List for: <?=date("m/d/y",strtotime($_GET['date']))?></h2>

<table border="0" cellpadding="5" cellspacing="0" bordercolor="#999999">
<thead>
  <tr>
    <th width="200" align="left" bgcolor="#999999"><strong>Client Name</strong></th>
    <th width="100" align="left" bgcolor="#999999"><strong># in Trip</strong></th>
    <th width="300" align="left" bgcolor="#999999"><strong>Trip Type </strong></th>
    <th width="200" align="left" bgcolor="#999999"><strong>Guide</strong></th>
    <th width="300" align="left" bgcolor="#999999"><strong>Notes</strong></th>
  </tr>
</thead>
<tbody>
<?php
$sql = "SELECT * FROM ".RESERVATIONS." WHERE date = '".date("Y-m-d",strtotime($_GET['date']))."' ORDER BY date DESC";
$result= mysql_query($sql);
while($row = mysql_fetch_array($result)){
?>
  <tr>
    <td><?=stripslashes($row['firstname'])?> <?=stripslashes($row['lastname'])?></td>
    <td><?=stripslashes($row['number_of_people'])?></td>
    <td><?=get_trip_name($row['trip_id'])?></td>
    <td><?php if($row['guide_id']=="0"){echo "Not assigned";}else{echo get_guide_name($row['guide_id']);}?></td>
    <td><?=nl2br(stripslashes($row['notes']))?></td>
  </tr>
  <?php } ?>
  </tbody>
</table>
<br />
<br />

<a href="reservations-print-calendar.php" class="noPrint">&lsaquo;&lsaquo;Back to calendar</a>

