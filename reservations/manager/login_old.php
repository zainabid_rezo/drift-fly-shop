<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
//redirect('../../manager/login.php?referrer=http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); exit;
if($_SESSION['error_count']>'3') die('Access Blocked!'); //new

$cookie_name = preg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower(SITE_NAME)))).'admin'; //config

if(isSet($_COOKIE[$cookie_name]))
	{
	
		parse_str($_COOKIE[$cookie_name]);
		
		$sql = "SELECT * FROM ".ADMIN." WHERE id='$id'";
		$res = mysql_query($sql) or die("Error:".__LINE__.mysql_error());
		while($row=mysql_fetch_array($res)){
		
		if(($usr == $row['username']) && ($psw == md5($row['getmein'])))
			{
			$_SESSION["admin_loggedin"]="true";
			$_SESSION["admin_username"]=$row['username'];
			$_SESSION["admin_firstname"]=$row['admin_firstname'];
			$_SESSION["admin_lastname"]=$row['admin_lastname'];
			$_SESSION["admin_userid"]=$row['id'];
			
				if(isset($_GET['referrer'])){
				redirect($_GET['referrer']);
				}else{
				redirect('index.php');
				}
			
			}
		}
	
	}

?>
<?php
if(isset($_POST['btnLogin']))
{
 $username=addslashes(trim($_POST['admin_login']));
 $password=addslashes(trim($_POST['admin_pass']));
 $remember_me=$_POST['remember_me'] ? '1' : '0';
	
		$sql = "SELECT * FROM ".ADMIN." WHERE username='$username' AND getmein='$password' AND blocked < '".date('Y-m-d H:i:s')."'";
		$res = mysql_query($sql) or die("Error:".__LINE__.mysql_error());
		while($row=mysql_fetch_array($res))
		{
		$_SESSION["admin_loggedin"]="true";
		$_SESSION["admin_username"]=$row['username'];
		$_SESSION["admin_firstname"]=$row['admin_firstname'];
		$_SESSION["admin_lastname"]=$row['admin_lastname'];
		$_SESSION["admin_userid"]=$row['id'];
		
			if($remember_me == 1)
			{
			setcookie ($cookie_name, 'id='.$row['id'].'&usr='.$row['username'].'&psw='.md5($row['getmein']).'&fname='.$row['admin_firstname'].'&lname='.$row['admin_lastname'], time() + (3600 * 24 * 30));
			}else{
			setcookie($cookie_name, '', time()-3600);
			}
		
		
				if(isset($_GET['referrer'])){
				redirect($_GET['referrer']);
				}else{
				redirect('index.php');
				}
		}
		
		$error = "Invalid username or password.";
		
		#start admin blocker
		$log_file = 'login_offends.txt';
		$contents = file_get_contents($log_file).'Time: '.date('m/d/Y H:i:s').' IP: '.$_SERVER["REMOTE_ADDR"].' Login: '.$username.' Password: '.$password."\n";
		file_put_contents($log_file, $contents);
		
		if(isset($_SESSION['error_count'])) $_SESSION['error_count']++; else $_SESSION['error_count']='1';
		if($_SESSION['error_count']>'3'){
		$sql = "UPDATE ".ADMIN." SET blocked = '".date('Y-m-d H:i:s',strtotime("+2 hours"))."' WHERE id = '1'"; //admin will be blocked for next 2 hours.
		if(mysql_query($sql)){
		$url = explode("/",$_SERVER['HTTP_HOST'].$_SERVER["PHP_SELF"]);
		$str = $url[count($url)-1];
		$log_file_path = 'http://'.str_replace($str,'',$_SERVER['HTTP_HOST'].$_SERVER["PHP_SELF"]).$log_file;		
		mail(ADMIN_EMAIL,'Failed Attempted Login','Please contact webmaster!! '.$log_file_path, 'From: Wolf Web Development <programmer@wolfwebdevelopment.com>');//failed attempt login alert emailed to admin.
		};
		die('');
		}
		#end admin blocker

}
?>
<!DOCTYPE html>
<html>
<head>
<?php include"head-include.php";?>
</head>
<body>
<!-- Container -->
<div id="container">
  <!-- Header -->
  <div id="header">
    <!-- Top -->
    <div class="logo">
      <h1>
        <?=SITE_NAME?>
      </h1>
    </div>
    <!-- End of Top-->
    <!-- The navigation bar -->
    <div id="navbar">
      <?php //include"navigation.php";?>
    </div>
    <!-- End of navigation bar" -->
  </div>
  <!-- End of Header -->
  <!-- Background wrapper -->
  <div id="bgwrap">
    <!-- Main Content -->
    <div id="content">
      <div id="main">
        <?php include"login-body.php";?>
      </div>
    </div>
    <!-- End of Main Content -->
  </div>
  <!-- End of bgwrap -->
</div>
<!-- End of Container -->
<!-- Footer -->
<div id="footer">
  <?php include"footer.php";?>
</div>
<!-- End of Footer -->
</body>
</html>
