<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="-1">
<!-- End of Meta -->
  
<!-- Page title -->
<title><?=SITE_NAME?></title>
<!-- End of Page title -->

<!-- Libraries -->
<script type="text/javascript">
	var GB_ROOT_DIR = "./greybox/greybox/";
</script>

<script type="text/javascript" src="greybox/greybox/AJS.js"></script>
<script type="text/javascript" src="greybox/greybox/AJS_fx.js"></script>
<script type="text/javascript" src="greybox/greybox/gb_scripts.js"></script>
<link href="greybox/greybox/gb_styles.css" rel="stylesheet" type="text/css" media="all" />

<link type="text/css" href="css/layout.css" rel="stylesheet" />	

<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

<!-- End of Libraries -->	

<!--[if IE 6]>
	<link type="text/css" href="css/ie6_width_1600.css" rel="stylesheet" />
	<link type="text/css" href="css/ie6.css" rel="stylesheet" />
	<script src="js/DD_belatedPNG.js"></script>
	<script>
		$(function(){
			DD_belatedPNG.fix('.logo img, #search, ul.dash li img, .message');
			//$('.logo').supersleight({shim: 'assets/x.gif'});
			//$('#search').supersleight({shim: 'assets/x.gif'});
			//$('ul.dash li img').supersleight({shim: 'assets/x.gif'});
			//$('.message').supersleight({shim: 'assets/x.gif'});
		});
	</script>	
<![endif]-->


<script type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "exact",
		theme : "advanced",
		elements : "contents,description,description_long,email_body",
		plugins : "advimage,autolink,lists,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
//
	relative_urls : false,
	remove_script_host : true,
    document_base_url : "<?=SITE_URL?>",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		//
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "css/layout.css",


	});
</script>

<script type="text/javascript">
function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete")) {
    document.location = delUrl;
  }
}
</script>

<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>


<script type="text/javascript">
function activestatus(id) {
   	id = "status_" + id;
	value = document.getElementById(id).checked = true ;
}
</script>


<script type="text/javascript">
var keylist="abcdefghijklmnopqrstuvwxyz123456789"
var passw=''

function generatepass(){
passw='';
for (i=0;i<5;i++)
passw+=keylist.charAt(Math.floor(Math.random()*keylist.length));
return passw;
}

function populatePassword(){
document.getElementById('code').value=generatepass();
}
</script>



<!---->
<style type="text/css"> 
@import "calendar_popup/jquery.datepick.css";
.img {
background-color:#90654B;
display:inline;
float:left;
margin:10px;
padding:5px;
}
</style>

<script type="text/javascript" src="calendar_popup/jquery.datepick.js"></script>
<script type="text/javascript"> 
$(function() {

$('#date').datepick();

$('#date_start,#date_end').datepick({onSelect: customRange, 
    showOn: 'both', buttonImageOnly: true, buttonImage: 'calendar_popup/calendar.gif'}); 
 function customRange(dateStr, date) { 
    if (this.id == 'date_start') { 
        $('#date_end').datepick('option', 'minDate', date); 
    } 
    else { 
        $('#date_start').datepick('option', 'maxDate', date); 
    } 
}
 
});

</script>



<!------------------------------------------------------------------------------------->

<link href="timepicker/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css" />
<link href="timepicker/jquery.ui.accordion.css" rel="stylesheet" type="text/css" />
<link href="timepicker/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" />
<link href="timepicker/jquery.ptTimeSelect.css" rel="stylesheet" type="text/css" />
<!--<script language="javascript" src="timepicker/jquery-1.6.1.min.js"></script>-->
<script language="javascript" src="timepicker/jquery-ui-1.8.13.custom.min.js"></script>
<script language="javascript" src="timepicker/jquery.ui.accordion.js"></script>
<script language="javascript" src="timepicker/jquery.ui.datepicker.js"></script>
<script language="javascript" src="timepicker/jquery.ptTimeSelect.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
	  	$('#time_start').ptTimeSelect();		
	  	$('#time_end').ptTimeSelect();		



		$("#trip_month").live( 'click', function(){				
			$("#tripmonth").show();
			$("#triprange").hide();
			$("#tripyear").show();
			$(".checkboxMonth").attr('checked', false); 
		});
		$("#trip_year").live( 'click', function(){
			$("#tripmonth").hide();
			$("#triprange").hide();
			$("#tripyear").show();
			$(".checkboxMonth").attr('checked', true); 
		});		
		$("#trip_range").live( 'click', function(){
			$("#tripmonth").hide();
			$("#triprange").show();
			$("#tripyear").hide();
			$(".checkboxMonth").attr('checked', false); 
		});


	});
</script>
