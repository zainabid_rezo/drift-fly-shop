<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include"head-include.php";?>
<?php if(isset($_GET['reservation_id'])){?>
<style type="text/css">
body {
	 background:#fff;
	 margin:5px;
}
</style>
<?php } ?>
</head>
<body id="top">
<!-- Container -->
<div id="container">
  <!-- Header -->
  <?php if(!isset($_GET['reservation_id'])){?>
  <div id="header">
    <!-- Top -->
    <?php include"header-right.php";?>
    <!-- End of Top-->
    <!-- The navigation bar -->
    <div id="navbar">
      <?php include"navigation.php";?>
    </div>
    <!-- End of navigation bar" -->
  </div>
  <?php } ?>
  <!-- End of Header -->
  <!-- Background wrapper -->
  <div id="bgwrap">
    <!-- Main Content -->
    <div id="content">
      <div id="main">
        <?php
		include"fishing-reports-body.php";
		?>
      </div>
    </div>
    <!-- End of Main Content -->
  </div>
  <!-- End of bgwrap -->
</div>
<!-- End of Container -->
<!-- Footer -->
<?php if(!isset($_GET['reservation_id'])){?>
<div id="footer">
  <?php include"footer.php";?>
</div>
<?php } ?>
<!-- End of Footer -->
</body>
</html>
