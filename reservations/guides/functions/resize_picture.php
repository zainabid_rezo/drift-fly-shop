<?php

function resize_picture($inputfile, $outputfile, $twidth, $theight, $method="smooth")
{
   $format = strtolower(substr(strrchr($inputfile,"."),1));
   switch($format)
   {
       case 'gif' :
           $type ="gif";
           $img = imagecreatefromgif($inputfile);
           break;
       case 'png' :
           $type ="png";
           $img = imagecreatefrompng($inputfile);
           break;
       case 'jpg' :
           $type ="jpg";
           $img = imagecreatefromjpeg($inputfile);
           break;
       case 'jpeg' :
           $type ="jpg";
           $img = imagecreatefromjpeg($inputfile);
           break;
       default :
           echo ("ERROR; UNSUPPORTED IMAGE TYPE");
           return false;
		   break;
   }
  
   //list($org_width, $org_height) = getimagesize($inputfile);
   
   	  $currwidth = imagesx($img);
      $currheight = imagesy($img);
   
   if($currwidth>$twidth || $currheight>$theight){
	  
		  if ($currheight > $currwidth) {
			 $ratio = $currwidth / $currheight;
			 $newheight = $theight;
			 $newwidth = ceil($theight * $ratio);
				 if($newwidth > $twidth){
				 $newwidth = $twidth;
				 $ratio = $currheight / $currwidth;
				 $newheight = ceil($twidth * $ratio);
				 }
			 
		  } else { 
			$ratio = $currheight / $currwidth;
			$newwidth = $twidth;
			$newheight = ceil($twidth * $ratio);
				 if($newheight > $theight){
				 $newheight = $theight;
				 $ratio = $currwidth / $currheight;
				 $newwidth = ceil($theight * $ratio);
				 }
		  }

 
	  }else{
	  $newwidth = $currwidth;
	  $newheight = $currheight;
	  }
   
   

   $img_n = imagecreatetruecolor ($newwidth, $newheight);   
   
   //imagecopyresampled($img_n, $img, 0, 0, $xoffset, $yoffset, $width, $height, $org_width, $org_height);
	
	if(function_exists('imagecopyresampled'))
      {
	  	imagecopyresampled($img_n, $img, 0, 0, 0, 0, $newwidth, $newheight, $currwidth, $currheight);
	  }else{
	  	imagecopyresized($img_n, $img, 0, 0, 0, 0, $newwidth, $newheight, $currwidth, $currheight);
	  }
	
	
   if($type=="gif")
   {
       imagegif($img_n, $outputfile);
   }
   elseif($type=="jpg")
   {
       imagejpeg($img_n, $outputfile);
   }
   elseif($type=="png")
   {
       imagepng($img_n, $outputfile);
   }
   elseif($type=="bmp")
   {
       imagewbmp($img_n, $outputfile);
   }
   return true;
}
?>