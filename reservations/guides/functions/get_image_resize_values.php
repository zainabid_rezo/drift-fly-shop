<?php
function get_image_resize_values($image, $twidth, $theight)
{
   
   $size = getimagesize($image);
   	  $currwidth = $size[0];
      $currheight = $size[1];
   
   if($currwidth>$twidth || $currheight>$theight){
	  
		  if ($currheight > $currwidth) {
			 $ratio = $currwidth / $currheight;
			 $newheight = $theight;
			 $newwidth = ceil($theight * $ratio);
				 if($newwidth > $twidth){
				 $newwidth = $twidth;
				 $ratio = $currheight / $currwidth;
				 $newheight = ceil($twidth * $ratio);
				 }
			 
		  } else { 
			$ratio = $currheight / $currwidth;
			$newwidth = $twidth;
			$newheight = ceil($twidth * $ratio);
				 if($newheight > $theight){
				 $newheight = $theight;
				 $ratio = $currwidth / $currheight;
				 $newwidth = ceil($theight * $ratio);
				 }
		  }

 
	  }else{
	  $newwidth = $currwidth;
	  $newheight = $currheight;
	  }
	  return array($newwidth,$newheight);

}
?>