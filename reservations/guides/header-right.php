<div id="top">
  <!-- Logo -->
  <div class="logo">
    <h1>
      <?=SITE_NAME?>
    </h1>
  </div>
  <!-- End of Logo -->
  <!-- Meta information -->
  <div class="meta">
    <p>Welcome,
      <?=$_SESSION['guide_firstname']?>
      <?=$_SESSION['guide_lastname']?>
      !
      <?php if($num_system_messages>"0"){?>
      <a href="home.php" title="<?=$num_system_messages?> new private messages from System!" class="tooltip">
      <?=$num_system_messages?>
      new messages!</a></p>
    <?php } ?>
    <ul>
      <li><a href="logout.php" title="" class="tooltip"><span class="ui-icon ui-icon-power"></span>Logout</a></li>
      <li><a href="account.php" title="" class="tooltip"><span class="ui-icon ui-icon-person"></span>My account</a></li>
    </ul>
  </div>
  <!-- End of Meta information -->
</div>