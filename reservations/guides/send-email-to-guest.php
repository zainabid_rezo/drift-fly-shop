<?php
function send_email_to_guest_final_step($to_email,$guest_name,$trip_name,$trip_date,$guide_name){

		get_email_template(2);

			$searchvariables  = array('[site_name]','[guest_name]','[trip_name]','[trip_date]','[guide_name]');
			$replacevariables = array(SITE_NAME,$guest_name,$trip_name,$trip_date,$guide_name);
			$msg1 = str_replace($searchvariables, $replacevariables, EMAIL_BODY_FIXED);
			$msg1 = nl2br($msg1);
			$msg2 = str_replace($searchvariables, $replacevariables, EMAIL_BODY);
			$msg = $msg1."<br/><br/>".$msg2;
			$msg = html_entity_decode($msg);

		if (smtpmailerDynamic($to_email, EMAIL_FROM, EMAIL_FROM_NAME, EMAIL_SUBJECT, $msg)) {
						
					if (smtpmailerDynamic(ADMIN_EMAIL, EMAIL_FROM, EMAIL_FROM_NAME, EMAIL_SUBJECT, $msg)) {
						return true;
					} else {
						return false;
					}
					
		} else {
			return false;
		}

	}
?>
<?php
function send_email_to_guest($reservation_id){
$result= mysql_query("SELECT * FROM ".RESERVATIONS." WHERE id = '$reservation_id'") or die(mysql_error());
while($row = mysql_fetch_array($result)){

$to_email = $row['email']; //abrar
$guest_name = $row['firstname']." ".$row['lastname'];
$trip_name = get_trip_name($row['trip_id']);
$guide_name = get_guide_name($row['guide_id']);
$trip_date = date("m/d/Y",strtotime($row['date']));

	if(send_email_to_guest_final_step($to_email,$guest_name,$trip_name,$trip_date,$guide_name)){
	return true;
	}else{
	return false;
	}

}

}
?>
<?php
function send_activation_email_to_guest_final_step($to_email,$guest_name,$activation_link,$trip_name,$trip_date,$guide_name){

		get_email_template_06(6);
		
			$searchvariables  = array('[site_name]','[guest_name]','[activation_link]','[trip_name]','[trip_date]','[guide_name]');
			$replacevariables = array(SITE_NAME_06,$guest_name,$activation_link,$trip_name,$trip_date,$guide_name);
			$msg1 = str_replace($searchvariables, $replacevariables, EMAIL_BODY_FIXED_06);
			$msg1 = nl2br($msg1);
			$msg2 = str_replace($searchvariables, $replacevariables, EMAIL_BODY_06);
			$msg = $msg1."<br/><br/>".$msg2;
			$msg = html_entity_decode($msg);

		if (smtpmailerDynamic_06($to_email, EMAIL_FROM_06, EMAIL_FROM_NAME_06, EMAIL_SUBJECT_06, $msg)) {
			return true;
		} else {
			return false;
		}

	}
?>
<?php
function send_activation_email_to_guest($reservation_id){
$result= mysql_query("SELECT * FROM ".RESERVATIONS." WHERE id = '$reservation_id'") or die(mysql_error());
while($row = mysql_fetch_array($result)){

$to_email = $row['email']; //abrar
$guest_name = $row['firstname']." ".$row['lastname'];
$activation_link = SITE_URL."reservations/customer-activation.php?id=".md5($reservation_id);

$trip_name = get_trip_name($row['trip_id']);
$guide_name = get_guide_name($row['guide_requested']);
$trip_date = date("m/d/Y",strtotime($row['date']));

	if(send_activation_email_to_guest_final_step($to_email,$guest_name,$activation_link,$trip_name,$trip_date,$guide_name)){
	return true;
	}else{
	return false;
	}

}

}
?>