<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
$_GET['guide'] = $_SESSION["guide_userid"];
?>
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><style type="text/css">
<!--
body,td,th {
	font-family:Arial, Helvetica, sans-serif;
	font-size:9px
}
-->
</style>
</head>
<body id="top">
<?php
echo "<h2 style=\"text-align:center;\">";
echo get_guide_name($_GET['guide']);
echo "</h2>";
?>
<?php
$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 
"August", "September", "October", "November", "December");
?>

<?php
if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");
if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");
?>

<?php
$cMonth = $_REQUEST["month"];
$cYear = $_REQUEST["year"];
?>

<?php
// FOR Navigation ////
$prev_year = $cYear;
$next_year = $cYear;
$prev_month = $cMonth-4;
$next_month = $cMonth+4;

if ($prev_month == 0 ) {
	$prev_month = 12;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -1 ) {
	$prev_month = 11;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -2 ) {
	$prev_month = 10;
	$prev_year = $cYear - 1;
}elseif ($prev_month == -3 ) {
	$prev_month = 9;
	$prev_year = $cYear - 1;
}
////////////////////////////
if ($next_month == 13 ) {
	$next_month = 1;
	$next_year = $cYear + 1;
}elseif ($next_month == 14 ) {
	$next_month = 2;
	$next_year = $cYear + 1;
}elseif ($next_month == 15 ) {
	$next_month = 3;
	$next_year = $cYear + 1;
}elseif ($next_month == 16 ) {
	$next_month = 4;
	$next_year = $cYear + 1;
}
?>

<form action="" method="post">

<?php
if(isset($_POST['Submit'])){

for ($i=0; $i<count($_POST['datestring']);$i++) {

		$datecheckbox = $_POST['datestring'][$i];
		$datestring = str_replace("_", "-", $datecheckbox);
		$datestring = date("Y-m-d",strtotime($datestring));
		if(isset($_POST["morning_$datecheckbox"])){
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '$datestring' AND guideid = '$_GET[guide]'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows==0){
			$sql		=	"INSERT INTO ".GUIDESCALENDAR."(guideid, date, morning_status, morning_createdby) VALUES ('$_GET[guide]', '$datestring', 'N', 'owner')";
			mysql_query($sql) or die(__LINE__.mysql_error());
			}else{
			$sql		=	"UPDATE ".GUIDESCALENDAR." SET morning_status='N', morning_createdby='owner' WHERE guideid = '$_GET[guide]' AND date = '$datestring' AND  morning_createdby!='system'";
			mysql_query($sql) or die(__LINE__.mysql_error());
			}
		}
		if(!isset($_POST["morning_$datecheckbox"])){
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '$datestring' AND guideid = '$_GET[guide]' AND morning_createdby = 'owner'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows!=0){
			$sql		=	"UPDATE ".GUIDESCALENDAR." SET morning_status='', morning_createdby='' WHERE guideid = '$_GET[guide]' AND date = '$datestring' AND  morning_createdby!='system'";
			mysql_query($sql) or die(__LINE__.mysql_error());
		}
		}
		////////////////////////////
		if(isset($_POST["afternoon_$datecheckbox"])){
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '$datestring' AND guideid = '$_GET[guide]'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows==0){
			$sql		=	"INSERT INTO ".GUIDESCALENDAR."(guideid, date, afternoon_status, afternoon_createdby) VALUES ('$_GET[guide]', '$datestring', 'N', 'owner')";
			mysql_query($sql) or die(__LINE__.mysql_error());
			}else{
			$sql		=	"UPDATE ".GUIDESCALENDAR." SET afternoon_status='N', afternoon_createdby='owner' WHERE guideid = '$_GET[guide]' AND date = '$datestring' AND  afternoon_createdby!='system'";
			mysql_query($sql) or die(__LINE__.mysql_error());
			}
		}
		if(!isset($_POST["afternoon_$datecheckbox"])){
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '$datestring' AND guideid = '$_GET[guide]' AND afternoon_createdby = 'owner'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows!=0){
			$sql		=	"UPDATE ".GUIDESCALENDAR." SET afternoon_status='', afternoon_createdby='' WHERE guideid = '$_GET[guide]' AND date = '$datestring' AND  afternoon_createdby!='system'";
			mysql_query($sql) or die(__LINE__.mysql_error());
		}
		}
		
		
		if(!isset($_POST["morning_$datecheckbox"]) AND !isset($_POST["afternoon_$datecheckbox"]))
		{
		$sql		=	"DELETE FROM ".GUIDESCALENDAR." WHERE guideid = '$_GET[guide]' AND date = '$datestring' AND morning_createdby <> 'system' AND afternoon_createdby <> 'system'";
		mysql_query($sql) or die(__LINE__.mysql_error());
		}
		
}

}
?>

<table border="0" align="center" style="width:500px">
  <tr>
    <td align="center" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $prev_month . "&year=" . $prev_year . "&guide=" . $_GET['guide']; ?>" >Previous</a></td>
    <td align="center" valign="top"><a href="<?php echo $_SERVER["PHP_SELF"] . "?month=". $next_month . "&year=" . $next_year . "&guide=" . $_GET['guide']; ?>" >Next</a></td>
  </tr>
  <tr>
    <td align="center" valign="top">
	<table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '".$cToday."' AND guideid = '$_GET[guide]'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows==0){
			$dateColor = "#ffffff";
			}else{
				
				while($rowcDay = mysql_fetch_array($resultcDay)){
				$cDayStatusMorning = $rowcDay['morning_status'];
				$cDayStatusAfternoon = $rowcDay['afternoon_status'];
				$morning_createdby = $rowcDay['morning_createdby'];
				$afternoon_createdby = $rowcDay['afternoon_createdby'];
				}
				
				if($cDayStatusMorning=="B"){
					$dateColorMorning = "#ff0000";
				}elseif($cDayStatusMorning=="P"){
					$dateColorMorning = "#ffff00";
				}elseif($cDayStatusMorning=="N"){
					$dateColorMorning = "#bbbbbb";
				}
				
				if($cDayStatusAfternoon=="B"){
					$dateColorAfternoon = "#ff0000";
				}elseif($cDayStatusAfternoon=="P"){
					$dateColorAfternoon = "#ffff00";
				}elseif($cDayStatusAfternoon=="N"){
					$dateColorAfternoon = "#bbbbbb";
				}
				
			}
				
		echo '<td align="center" valign="middle">';

		echo $cDay;
		echo "<div style=\"background-color:$dateColorMorning\">";
		echo "<input name=\"morning_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusMorning<>'') echo "checked=\"checked\" ";
				if($morning_createdby=="system") echo "disabled ";
		echo "/>";
				if($morning_createdby=="system") echo "<input name=\"morning_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<div style=\"background-color:$dateColorAfternoon\">";
		echo "<input name=\"afternoon_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusAfternoon<>'') echo "checked=\"checked\" ";
				if($afternoon_createdby=="system") echo "disabled ";
		echo "/>";
				if($afternoon_createdby=="system") echo "<input name=\"afternoon_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" />"; 


		echo "</td>\n";

		unset($cDayStatusMorning);
		unset($cDayStatusAfternoon);
		unset($dateColorMorning);
		unset($dateColorAfternoon);
		unset($morning_createdby);
		unset($afternoon_createdby);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table>
	</td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?>
      <table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '".$cToday."' AND guideid = '$_GET[guide]'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows==0){
			$dateColor = "#ffffff";
			}else{
				
				while($rowcDay = mysql_fetch_array($resultcDay)){
				$cDayStatusMorning = $rowcDay['morning_status'];
				$cDayStatusAfternoon = $rowcDay['afternoon_status'];
				$morning_createdby = $rowcDay['morning_createdby'];
				$afternoon_createdby = $rowcDay['afternoon_createdby'];
				}
				
				if($cDayStatusMorning=="B"){
					$dateColorMorning = "#ff0000";
				}elseif($cDayStatusMorning=="P"){
					$dateColorMorning = "#ffff00";
				}elseif($cDayStatusMorning=="N"){
					$dateColorMorning = "#bbbbbb";
				}
				
				if($cDayStatusAfternoon=="B"){
					$dateColorAfternoon = "#ff0000";
				}elseif($cDayStatusAfternoon=="P"){
					$dateColorAfternoon = "#ffff00";
				}elseif($cDayStatusAfternoon=="N"){
					$dateColorAfternoon = "#bbbbbb";
				}
				
			}
				
		echo '<td align="center" valign="middle">';

		echo $cDay;
		echo "<div style=\"background-color:$dateColorMorning\">";
		echo "<input name=\"morning_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusMorning<>'') echo "checked=\"checked\" ";
				if($morning_createdby=="system") echo "disabled ";
		echo "/>";
				if($morning_createdby=="system") echo "<input name=\"morning_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<div style=\"background-color:$dateColorAfternoon\">";
		echo "<input name=\"afternoon_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusAfternoon<>'') echo "checked=\"checked\" ";
				if($afternoon_createdby=="system") echo "disabled ";
		echo "/>";
				if($afternoon_createdby=="system") echo "<input name=\"afternoon_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" />"; 


		echo "</td>\n";

		unset($cDayStatusMorning);
		unset($cDayStatusAfternoon);
		unset($dateColorMorning);
		unset($dateColorAfternoon);
		unset($morning_createdby);
		unset($afternoon_createdby);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table>
	
	</td>
  </tr>
  <tr>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?>
      <table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '".$cToday."' AND guideid = '$_GET[guide]'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows==0){
			$dateColor = "#ffffff";
			}else{
				
				while($rowcDay = mysql_fetch_array($resultcDay)){
				$cDayStatusMorning = $rowcDay['morning_status'];
				$cDayStatusAfternoon = $rowcDay['afternoon_status'];
				$morning_createdby = $rowcDay['morning_createdby'];
				$afternoon_createdby = $rowcDay['afternoon_createdby'];
				}
				
				if($cDayStatusMorning=="B"){
					$dateColorMorning = "#ff0000";
				}elseif($cDayStatusMorning=="P"){
					$dateColorMorning = "#ffff00";
				}elseif($cDayStatusMorning=="N"){
					$dateColorMorning = "#bbbbbb";
				}
				
				if($cDayStatusAfternoon=="B"){
					$dateColorAfternoon = "#ff0000";
				}elseif($cDayStatusAfternoon=="P"){
					$dateColorAfternoon = "#ffff00";
				}elseif($cDayStatusAfternoon=="N"){
					$dateColorAfternoon = "#bbbbbb";
				}
				
			}
				
		echo '<td align="center" valign="middle">';

		echo $cDay;
		echo "<div style=\"background-color:$dateColorMorning\">";
		echo "<input name=\"morning_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusMorning<>'') echo "checked=\"checked\" ";
				if($morning_createdby=="system") echo "disabled ";
		echo "/>";
				if($morning_createdby=="system") echo "<input name=\"morning_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<div style=\"background-color:$dateColorAfternoon\">";
		echo "<input name=\"afternoon_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusAfternoon<>'') echo "checked=\"checked\" ";
				if($afternoon_createdby=="system") echo "disabled ";
		echo "/>";
				if($afternoon_createdby=="system") echo "<input name=\"afternoon_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" />"; 


		echo "</td>\n";

		unset($cDayStatusMorning);
		unset($cDayStatusAfternoon);
		unset($dateColorMorning);
		unset($dateColorAfternoon);
		unset($morning_createdby);
		unset($afternoon_createdby);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table>
	  
	  </td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?>
	  
	  <table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#FFFFFF"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#FFFFFF"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$resultcDay= mysql_query("SELECT * FROM ".GUIDESCALENDAR." WHERE date = '".$cToday."' AND guideid = '$_GET[guide]'");
		$numRows = mysql_num_rows($resultcDay);
			if($numRows==0){
			$dateColor = "#ffffff";
			}else{
				
				while($rowcDay = mysql_fetch_array($resultcDay)){
				$cDayStatusMorning = $rowcDay['morning_status'];
				$cDayStatusAfternoon = $rowcDay['afternoon_status'];
				$morning_createdby = $rowcDay['morning_createdby'];
				$afternoon_createdby = $rowcDay['afternoon_createdby'];
				}
				
				if($cDayStatusMorning=="B"){
					$dateColorMorning = "#ff0000";
				}elseif($cDayStatusMorning=="P"){
					$dateColorMorning = "#ffff00";
				}elseif($cDayStatusMorning=="N"){
					$dateColorMorning = "#bbbbbb";
				}
				
				if($cDayStatusAfternoon=="B"){
					$dateColorAfternoon = "#ff0000";
				}elseif($cDayStatusAfternoon=="P"){
					$dateColorAfternoon = "#ffff00";
				}elseif($cDayStatusAfternoon=="N"){
					$dateColorAfternoon = "#bbbbbb";
				}
				
			}
				
		echo '<td align="center" valign="middle">';

		echo $cDay;
		echo "<div style=\"background-color:$dateColorMorning\">";
		echo "<input name=\"morning_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusMorning<>'') echo "checked=\"checked\" ";
				if($morning_createdby=="system") echo "disabled ";
		echo "/>";
				if($morning_createdby=="system") echo "<input name=\"morning_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<div style=\"background-color:$dateColorAfternoon\">";
		echo "<input name=\"afternoon_$cTodayId\" type=\"checkbox\" value=\"OWNER\"";
				if($cDayStatusAfternoon<>'') echo "checked=\"checked\" ";
				if($afternoon_createdby=="system") echo "disabled ";
		echo "/>";
				if($afternoon_createdby=="system") echo "<input name=\"afternoon_$cTodayId\" type=\"hidden\" value=\"OWNER\" />"; 
		echo "</div>";
		echo "<input name=\"datestring[]\" type=\"hidden\" value=\"$cTodayId\" />"; 


		echo "</td>\n";

		unset($cDayStatusMorning);
		unset($cDayStatusAfternoon);
		unset($dateColorMorning);
		unset($dateColorAfternoon);
		unset($morning_createdby);
		unset($afternoon_createdby);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table>
	  </td>
  </tr>
  <tr>
    <td align="right" valign="top"><table border="0">
      <tr>
        <td>Booked</td>
        <td width="20" bgcolor="#ff0000">&nbsp;</td>
      </tr>
    </table>
      <table border="0">
        <tr>
          <td>Not Available </td>
          <td width="20" bgcolor="#bbbbbb">&nbsp;</td>
        </tr>
      </table>    </td>
    <td align="center" valign="middle"><input type="submit" name="Submit" class="button" value="Update" /></td>
  </tr>
  <tr>
    <td colspan="2" valign="top">Click on any date and click &quot;Update&quot; to remove the guide from that date.</td>
    </tr>
</table>
</form>
</body>
</html>