<?php
include_once("phpmailer/class.phpmailer.php");
?>
<?php
function smtpmailerDynamic($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false, $attachment01 = '', $attachment02 = '', $attachment03 = '', $attachment04 = '', $attachment05 = '', $attachment06 = '') {

	/*		$headers = "MIME-Version: 1.0" . "\r\n";
 			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= "From: $from_name <$from>" . "\r\n";
			mail($to,$subject,$body,$headers);
			return true;
	*/

	global $emailwarning;
	global $emailsuccess;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML($ishtml);
	$mail->SMTPAuth = true; 
	if ($is_gmail) {
		$mail->SMTPSecure = 'ssl'; 
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;  
		$mail->Username = 'wolfwebdevelopment@gmail.com';  
		$mail->Password = 'password';   
	} else {
		$mail->Host = SMTPSERVER;
		$mail->Username = SMTPUSER;  
		$mail->Password = SMTPPWD;
		$mail->Port = SMTPPORT;  
	}        
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	if(!empty($attachment01)) $mail->AddAttachment("../pictures/".$attachment01);
	if(!empty($attachment02)) $mail->AddAttachment("../pictures/".$attachment02);
	if(!empty($attachment03)) $mail->AddAttachment("../pictures/".$attachment03);
	if(!empty($attachment04)) $mail->AddAttachment("../pictures/".$attachment04);
	if(!empty($attachment05)) $mail->AddAttachment("../pictures/".$attachment05);
	if(!empty($attachment06)) $mail->AddAttachment("../pictures/".$attachment06);
	
	$mail->ClearAddresses();
	$mail->AddAddress($to);
	if(!$mail->Send()) {
		$emailwarning .= 'Mail error: '.$mail->ErrorInfo;
		return false;
	} else {
		$emailsuccess .= 'Email Message sent!';
		return true;
	}
}

function get_email_template($id){
	$q_email="SELECT * FROM ".EMAILTEMPLATES." WHERE id = '$id' AND active = '1'";
	$sql_email= @mysql_query($q_email);
	if(!$sql_email)
	die('<p>Error performing query: ' . mysql_error() .'</p>');
			while($row_email=mysql_fetch_array($sql_email))
			{
		
			define('SMTPUSER', $row_email['smtp_user']); 
			define('SMTPPWD', $row_email['smtp_password']); 
			define('SMTPSERVER', $row_email['smtp_server']); 
			define('SMTPPORT', $row_email['smtp_port']); 
			
			define('EMAIL_FROM', $row_email['my_display_email']);
			define('EMAIL_FROM_NAME', $row_email['my_display_name']); 
			define('EMAIL_SUBJECT', $row_email['email_subject']); 
			
			define('EMAIL_BODY_FIXED', $row_email['email_body_fixed']); 
			define('EMAIL_BODY', html_entity_decode($row_email['email_body'])); 
			
			}
		}

function smtpmailerDynamic_02($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false) {
	/*		$headers = "MIME-Version: 1.0" . "\r\n";
 			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= "From: $from_name <$from>" . "\r\n";
			mail($to,$subject,$body,$headers);
			return true;
	*/
	global $emailwarning;
	global $emailsuccess;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML($ishtml);
	$mail->SMTPAuth = true; 
	if ($is_gmail) {
		$mail->SMTPSecure = 'ssl'; 
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;  
		$mail->Username = 'wolfwebdevelopment@gmail.com';  
		$mail->Password = 'password';   
	} else {
		$mail->Host = SMTPSERVER_02;
		$mail->Username = SMTPUSER_02;  
		$mail->Password = SMTPPWD_02;
		$mail->Port = SMTPPORT_02;  
	}        
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->ClearAddresses();
	$mail->AddAddress($to);
	if(!$mail->Send()) {
		$emailwarning .= 'Mail error: '.$mail->ErrorInfo;
		return false;
	} else {
		$emailsuccess .= 'Email Message sent!';
		return true;
	}
}

function get_email_template_02($id){
	$q_email="SELECT * FROM ".EMAILTEMPLATES." WHERE id = '$id' AND active = '1'";
	$sql_email= @mysql_query($q_email);
	if(!$sql_email)
	die('<p>Error performing query: ' . mysql_error() .'</p>');
			while($row_email=mysql_fetch_array($sql_email))
			{
		
			define('SMTPUSER_02', $row_email['smtp_user']); 
			define('SMTPPWD_02', $row_email['smtp_password']); 
			define('SMTPSERVER_02', $row_email['smtp_server']); 
			define('SMTPPORT_02', $row_email['smtp_port']); 
			
			define('EMAIL_FROM_02', $row_email['my_display_email']);
			define('EMAIL_FROM_NAME_02', $row_email['my_display_name']); 
			define('EMAIL_SUBJECT_02', $row_email['email_subject']); 
			
			define('EMAIL_BODY_FIXED_02', $row_email['email_body_fixed']); 
			define('EMAIL_BODY_02', html_entity_decode($row_email['email_body'])); 
			
			}
		}


function smtpmailerDynamic_04($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false) {
	/*		$headers = "MIME-Version: 1.0" . "\r\n";
 			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= "From: $from_name <$from>" . "\r\n";
			mail($to,$subject,$body,$headers);
			return true;
	*/
	global $emailwarning;
	global $emailsuccess;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML($ishtml);
	$mail->SMTPAuth = true; 
	if ($is_gmail) {
		$mail->SMTPSecure = 'ssl'; 
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;  
		$mail->Username = 'wolfwebdevelopment@gmail.com';  
		$mail->Password = 'password';   
	} else {
		$mail->Host = SMTPSERVER_04;
		$mail->Username = SMTPUSER_04;  
		$mail->Password = SMTPPWD_04;
		$mail->Port = SMTPPORT_04;  
	}        
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->ClearAddresses();
	$mail->AddAddress($to);
	if(!$mail->Send()) {
		$emailwarning .= 'Mail error: '.$mail->ErrorInfo;
		return false;
	} else {
		$emailsuccess .= 'Email Message sent!';
		return true;
	}
}

function get_email_template_04($id){
	$q_email="SELECT * FROM ".EMAILTEMPLATES." WHERE id = '$id' AND active = '1'";
	$sql_email= @mysql_query($q_email);
	if(!$sql_email)
	die('<p>Error performing query: ' . mysql_error() .'</p>');
			while($row_email=mysql_fetch_array($sql_email))
			{
		
			define('SMTPUSER_04', $row_email['smtp_user']); 
			define('SMTPPWD_04', $row_email['smtp_password']); 
			define('SMTPSERVER_04', $row_email['smtp_server']); 
			define('SMTPPORT_04', $row_email['smtp_port']); 
			
			define('EMAIL_FROM_04', $row_email['my_display_email']);
			define('EMAIL_FROM_NAME_04', $row_email['my_display_name']); 
			define('EMAIL_SUBJECT_04', $row_email['email_subject']); 
			
			define('EMAIL_BODY_FIXED_04', $row_email['email_body_fixed']); 
			define('EMAIL_BODY_04', html_entity_decode($row_email['email_body'])); 
			
			}
		}


function smtpmailerDynamic_06($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false) {
	/*		$headers = "MIME-Version: 1.0" . "\r\n";
 			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= "From: $from_name <$from>" . "\r\n";
			mail($to,$subject,$body,$headers);
			return true;
	*/
	global $emailwarning;
	global $emailsuccess;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML($ishtml);
	$mail->SMTPAuth = true; 
	if ($is_gmail) {
		$mail->SMTPSecure = 'ssl'; 
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;  
		$mail->Username = 'wolfwebdevelopment@gmail.com';  
		$mail->Password = 'password';   
	} else {
		$mail->Host = SMTPSERVER_06;
		$mail->Username = SMTPUSER_06;  
		$mail->Password = SMTPPWD_06;
		$mail->Port = SMTPPORT_06;  
	}        
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->ClearAddresses();
	$mail->AddAddress($to);
	if(!$mail->Send()) {
		$emailwarning .= 'Mail error: '.$mail->ErrorInfo;
		return false;
	} else {
		$emailsuccess .= 'Email Message sent!';
		return true;
	}
}

function get_email_template_06($id){
	$q_email="SELECT * FROM ".EMAILTEMPLATES." WHERE id = '$id' AND active = '1'";
	$sql_email= @mysql_query($q_email);
	if(!$sql_email)
	die('<p>Error performing query: ' . mysql_error() .'</p>');
			while($row_email=mysql_fetch_array($sql_email))
			{
		
			define('SMTPUSER_06', $row_email['smtp_user']); 
			define('SMTPPWD_06', $row_email['smtp_password']); 
			define('SMTPSERVER_06', $row_email['smtp_server']); 
			define('SMTPPORT_06', $row_email['smtp_port']); 
			
			define('EMAIL_FROM_06', $row_email['my_display_email']);
			define('EMAIL_FROM_NAME_06', $row_email['my_display_name']); 
			define('EMAIL_SUBJECT_06', $row_email['email_subject']); 
			
			define('EMAIL_BODY_FIXED_06', $row_email['email_body_fixed']); 
			define('EMAIL_BODY_06', html_entity_decode($row_email['email_body'])); 
			
			}
		}
?>