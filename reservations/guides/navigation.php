<ul class="nav">
  <li<?=currentpage('reservations.php')?>><a href="reservations.php?view">Reservations</a></li>
	<li<?=currentpage('fishing-reports.php')?>><a href="#">Fishing Reports</a>
    <ul>
      <li><a href="fishing-reports.php?add">Add Fishing Report</a></li>
      <li><a href="fishing-reports.php">View Fishing Report</a></li>
    </ul>
  </li>
  <li<?=currentpage('reservations-calendar.php')?><?=currentpage('reservations-view.php')?>><a href="reservations-calendar.php">View Full Calendar</a></li>
  <li<?=currentpage('guides-calendar.php')?>><a href="guides-calendar.php" rel="gb_page_center[640, 600]">Edit My Calendar</a></li>
</ul>