<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");

include_once("send-email-class.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include"head-include.php";?>
</head>
<body id="top">
<!-- Container -->
<div id="container">
  <!-- Header -->
  <div id="header">
    <!-- Top -->
    <?php include"header-right.php";?>
    <!-- End of Top-->
    <!-- The navigation bar -->
    <div id="navbar">
      <?php include"navigation.php";?>
    </div>
    <!-- End of navigation bar" -->
  </div>
  <!-- End of Header -->
  <!-- Background wrapper -->
  <div id="bgwrap">
    <!-- Main Content -->
    <div id="content">
      <div id="main">
        <?php 
		if(isset($_GET['editid']) AND isset($_GET['pickdate'])){
		include"reservations-editdate-body.php";
		}elseif(isset($_GET['editid']) AND isset($_GET['date'])){
		include"reservations-step-3-body.php";
		}elseif(isset($_GET['view']) OR isset($_GET['editid'])){
		include"reservations-step-3-body.php";
		}elseif(!isset($_GET['trip_id']) AND !isset($_GET['date'])){
		include"reservations-step-1-body.php";
		}elseif(isset($_GET['trip_id']) AND !isset($_GET['date'])){
		include"reservations-step-2-body.php";
		}elseif(isset($_GET['trip_id']) AND isset($_GET['date'])){
		$_GET['add'] = true;
		include"reservations-step-3-body.php";
		}
		
		?>
      </div>
    </div>
    <!-- End of Main Content -->
  </div>
  <!-- End of bgwrap -->
</div>
<!-- End of Container -->
<!-- Footer -->
<div id="footer">
  <?php include"footer.php";?>
</div>
<!-- End of Footer -->
</body>
</html>
