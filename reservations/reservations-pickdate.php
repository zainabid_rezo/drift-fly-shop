<?php

if($_SERVER['HTTPS']!="on")

{

$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

header("Location:$redirect");//abrar

}

?><?php

include_once("manager/conn.php");

include_once("manager/db-tables.php");

include_once("manager/site-details.php");

include_once("manager/functions.php");

$trip_id = (int)$_GET['trip_id'];

$numberofpeople = (int)$_GET['numberofpeople'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300italic,400italic,600,600italic,700,300' rel='stylesheet'>
	<link type="text/css" rel="stylesheet" href="../css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="../css/style.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
	<div id="wrapper">
		<div class="container">
			<div class="bg-box">
				<header id="header">
					<div class="phone-info">
						<img src="../images/icon-telephone.png" alt="icon phone">
						<span class="number">(719) 543-3900</span>
					</div>
					<nav id="nav" class="navbar navbar-default">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapsed" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.php"><img src="../images/logo.png" alt=""></a>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="navbar-collapsed">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="../index.php">Home</a></li>
								<li><a href="../our_guides.php">Our Guides</a></li>
								<li><a href="../flyshop.php">Fly Shop</a></li>
								<li><a href="guidedtrips.php">Guided trips</a></li>
								<li><a href="../localwater.php">Local Waters</a></li>
								<li><a href="../picture_gallery.php">Photo Gallery</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="../contact.php">Contact</a></li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</nav>
				</header>
                	<main id="main">
					<div class="content-wrap">
						<div class="row mb40">
							<div class="col-sm-8">
								<section class="text-wrap">



<?php

$monthNames = Array("January", "February", "March", "April", "May", "June", "July", 

"August", "September", "October", "November", "December");

?>



<?php

if (!isset($_REQUEST["month"])) $_REQUEST["month"] = date("n");

if (!isset($_REQUEST["year"])) $_REQUEST["year"] = date("Y");

?>



<?php

$cMonth = htmlentities($_REQUEST["month"]);

$cYear = htmlentities($_REQUEST["year"]);

?>



<?php

// FOR Navigation ////

$prev_year = $cYear;

$next_year = $cYear;

$prev_month = $cMonth-4;

$next_month = $cMonth+4;



if ($prev_month == 0 ) {

	$prev_month = 12;

	$prev_year = $cYear - 1;

}elseif ($prev_month == -1 ) {

	$prev_month = 11;

	$prev_year = $cYear - 1;

}elseif ($prev_month == -2 ) {

	$prev_month = 10;

	$prev_year = $cYear - 1;

}elseif ($prev_month == -3 ) {

	$prev_month = 9;

	$prev_year = $cYear - 1;

}

////////////////////////////

if ($next_month == 13 ) {

	$next_month = 1;

	$next_year = $cYear + 1;

}elseif ($next_month == 14 ) {

	$next_month = 2;

	$next_year = $cYear + 1;

}elseif ($next_month == 15 ) {

	$next_month = 3;

	$next_year = $cYear + 1;

}elseif ($next_month == 16 ) {

	$next_month = 4;

	$next_year = $cYear + 1;

}

?>



<br>

<h2 align="center"><font color="#000000">Select a date</font></h2>

<table border="0" align="center" style="width:620px">
  <tr>
    <td valign="top"><h2><font color="#000000">Select a date</font></h2>
        <table width="250" border="0">
          <tr>
            <td width="20" bgcolor="#00ff00">&nbsp;</td>
            <td><font color="#000000">Dates in Green have trips available</font></td>
          </tr>
        </table>
        <table border="0">
          <tr>
            <td width="20" style="border:#333333 solid 1px; background-color:#FFFFFF">&nbsp;</td>
            <td><font color="#000000">Dates in white have no trips available</font></td>
          </tr>
        </table>
        <br>
        <font color="#000000">Please note, online reservations require 24 hours notice, if you want to book a reservation inside of 24 hours, Please call the shop (719) 543-3900 and we will be happy to accomodate you.</font></td>
    <td align="center" valign="top"><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#000000"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);
		
		if($row_trip_check['rc_year'] != ''){
		
		$resultcDay= mysql_query("SELECT * FROM ".TRIPSRECURRENCE." WHERE trip_id='".$trip_id."' AND date = '".$cToday."' ");
		$numRows = mysql_num_rows($resultcDay);
			
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}elseif($numRows==0){
				$dateColor = "#ffffff";
				$link_start = '';
				$link_end = '';
			}else{
				$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
				
			}
		}else{
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}else{
			
			$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
			}
		}
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#000000"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);
		
		if($row_trip_check['rc_year'] != ''){
		
		$resultcDay= mysql_query("SELECT * FROM ".TRIPSRECURRENCE." WHERE trip_id='".$trip_id."' AND date = '".$cToday."' ");
		$numRows = mysql_num_rows($resultcDay);
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}elseif($numRows==0){
				$dateColor = "#ffffff";
				$link_start = '';
				$link_end = '';
			}else{
				$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
				
			}
		}else{
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}else{
			
			$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
			}
		}
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
  <tr>
    <td valign="bottom"><a href="<?php echo $_SERVER["PHP_SELF"]."?trip_id=".$trip_id."&numberofpeople=".$numberofpeople."&month=".$prev_month."&year=".$prev_year; ?>" >Previous</a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo $_SERVER["PHP_SELF"]."?trip_id=".$trip_id."&numberofpeople=".$numberofpeople."&month=".$next_month."&year=".$next_year; ?>" >Next</a></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#000000"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);
		
		if($row_trip_check['rc_year'] != ''){
		
		$resultcDay= mysql_query("SELECT * FROM ".TRIPSRECURRENCE." WHERE trip_id='".$trip_id."' AND date = '".$cToday."' ");
		$numRows = mysql_num_rows($resultcDay);
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}elseif($numRows==0){
				$dateColor = "#ffffff";
				$link_start = '';
				$link_end = '';
			}else{
				$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
				
			}
		}else{
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}else{
			
			$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
			}
		}
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
    <td align="center" valign="top"><?php $cMonth++; 

if ($cMonth == 13 ) {
	$cMonth = 1;
	$cYear = $cYear + 1;
}elseif ($next_month == 14 ) {
	$cMonth = 2;
	$cYear = $cYear + 1;
}elseif ($next_month == 15 ) {
	$cMonth = 3;
	$cYear = $cYear + 1;
}elseif ($next_month == 16 ) {
	$cMonth = 4;
	$cYear = $cYear + 1;
}
?><table width="200" border="0" cellpadding="2" cellspacing="2">
      <tr align="center">
        <td colspan="7" bgcolor="#999999" style="color:#000000"><strong><?php echo $monthNames[$cMonth-1].' '.$cYear; ?></strong></td>
      </tr>
      <tr>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>M</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>W</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>T</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>F</strong></td>
        <td align="center" bgcolor="#999999" style="color:#000000"><strong>S</strong></td>
      </tr>
      <?php 
$timestamp = mktime(0,0,0,$cMonth,1,$cYear);
$maxday = date("t",$timestamp);
$thismonth = getdate ($timestamp);
$startday = $thismonth['wday'];
for ($i=0; $i<($maxday+$startday); $i++) {
	if(($i % 7) == 0 ) {echo "<tr>\n";}
	if($i < $startday) {echo "<td></td>\n";}
	else {

		$cDay = ($i - $startday + 1);
		$cToday = $cYear."-".$cMonth."-".$cDay;
		$cTodayId = $cYear."_".$cMonth."_".$cDay;
		
		$trips_check = mysql_query("SELECT * FROM wbooking_trips_posts WHERE id = '$trip_id'") or die(mysql_error());
		$row_trip_check = mysql_fetch_array($trips_check);
		
		if($row_trip_check['rc_year'] != ''){
		
		$resultcDay= mysql_query("SELECT * FROM ".TRIPSRECURRENCE." WHERE trip_id='".$trip_id."' AND date = '".$cToday."' ");
		$numRows = mysql_num_rows($resultcDay);
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}elseif($numRows==0){
				$dateColor = "#ffffff";
				$link_start = '';
				$link_end = '';
			}else{
				$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
				
			}
		}else{
			
			if(date('YmdHis',strtotime($cToday.' '.$triptime))<=date('YmdHis',strtotime("+24 hours"))){
				$dateColor = "#eeeeee";
				$link_start = '';
				$link_end = '';
			}else{
			
			$dateColor = "#00ff00";
				$link_start = "<a href=\"reservations-book.php?trip_id=$trip_id&numberofpeople=$numberofpeople&date=$cToday\">";
				$link_end = "<a>";
			}
		}
		
		
		echo '<td align="center" valign="middle" height="20px" bgcolor="'.$dateColor.'">';
		echo "$link_start";
		echo $cDay;
		echo "$link_end";
		echo "</td>\n";

		unset($cDayStatus);
		unset($dateColor);
		unset($owner);
		unset($cToday);
		unset($cTodayId);

	
	}
	if(($i % 7) == 6 ) {echo "</tr>\n";}
}
?>
    </table></td>
  </tr>
</table>



</section>
							</div>
							
						</div>
					</div>
				</main>
				<footer id="footer">
					<div class="row">
						<div class="col-sm-3 col-sm-push-9">
							<a class="facebook" href="#"><img src="../images/facebook.jpg" alt="facebook"></a>
						</div>
						<div class="col-sm-9 col-sm-pull-3">
							<ul class="list-inline footer-links">
								<li><a href="../index.php">Home</a></li>
								<li><a href="../our_guides.php">Our Guides</a></li>
								<li><a href="../flyshop.php">Fly Shop</a></li>
								<li><a href="../guidedtrips.php">Guided trips</a></li>
								<li><a href="../localwater.php">Local Waters</a></li>
								<li><a href="../picture_gallery.php">Photo Gallery</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="../contact.php">Contact</a></li>
							</ul>
							<span class="copyright">&copy; copyrights 2016 The Drift Fly Shop All rights reserved</span>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script>
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
				document.createTextNode(
					'@-ms-viewport{width:auto!important}'
					)
				)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>
</body>
</html>