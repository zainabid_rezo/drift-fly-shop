<?php require('includes/header.php'); ?>
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="js/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="js/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();
		});
		
		</script>
        <style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}
		</style>
				<main id="main">
					<div class="content-wrap">
						<div class="row mb40">
							<div class="col-sm-8" style="width:100%;">
								<section class="text-wrap">
									<h2>Picture Gallery</h2>
									<?php 
									$pic_gallery = mysql_query("SELECT * FROM picture_gallery ORDER BY sortby ASC") or die(mysql_error());
									 while($getpic = mysql_fetch_array($pic_gallery)){ ?>
     <div class="img"  id="gallery" style="float:left; margin-left:15px; margin-top:15px;">
      
            <div style="height:100px; width:120px; border:1px solid grey; padding-top:20px;">
            <!--<a class="group4"  href="gallery/<?php //echo $getpic['file']; ?>" title="<?php //echo $getpic['name']; ?>">-->
            <a class="fancybox" href="gallery/<?php echo $getpic['file']; ?>" data-fancybox-group="gallery" title="<?php echo $getpic['name']; ?>">
            <!--<img src="gallery/<?php //echo $getpic['file']; ?>" border="0" width="100" height="100">-->
            <?php echo print_thumb('gallery/'.$getpic['file'],120,100,$getpic['name'],'margin:0 auto;'); ?>
            </a>
            </div>
        </div>
     
	<?php }  ?>
																		
								</section>
							</div>
						
						</div>
					</div>
				</main>
				<?php require('includes/footer.php'); ?>
	
	<script src="js/bootstrap.min.js"></script>
	<script>
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
				document.createTextNode(
					'@-ms-viewport{width:auto!important}'
					)
				)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>
</body>
</html>