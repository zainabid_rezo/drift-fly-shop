<?php require('includes/header.php'); ?>
				<main id="main">
					<div class="content-wrap">
						<div class="row mb40">
							<div class="col-sm-8">
								<section class="text-wrap">
									<h2>Contact</h2>
									<?php echo htmlspecialchars_decode($row['contents'],ENT_COMPAT); ?>
																		
								</section>
							</div>
							<div class="col-sm-4">
								<section class="latest-news">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3130.7178386134533!2d-104.62350238523148!3d38.30920468900506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8713a225b10a96ed%3A0xccfdd4af827cbc1b!2sThe+Drift+Fly+Shop!5e0!3m2!1sen!2s!4v1462791363586" width="330" height="210" frameborder="0" style="border:0" allowfullscreen></iframe>
								</section>
							</div>
						</div>
					</div>
				</main>
				<?php require('includes/footer.php'); ?>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
				document.createTextNode(
					'@-ms-viewport{width:auto!important}'
					)
				)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>
</body>
</html>