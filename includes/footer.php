<footer id="footer">
					<div class="row">
						<div class="col-sm-3 col-sm-push-9">
							<a class="facebook" href="https://www.facebook.com/thedriftflyshop/" target="_blank"><img src="images/facebook.jpg" alt="facebook"></a>
						</div>
						<div class="col-sm-9 col-sm-pull-3">
							<ul class="list-inline footer-links">
								<li><a href="index.php">Home</a></li>
								<li><a href="our_guides.php">Our Guides</a></li>
								<li><a href="flyshop.php">Fly Shop</a></li>
								<li><a href="guidedtrips.php">Guided trips</a></li>
								<li><a href="localwater.php">Local Waters</a></li>
								<li><a href="picture_gallery.php">Photo Gallery</a></li>
								<li><a href="#">Blog</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
							<span class="copyright">&copy; copyrights 2016 The Drift Fly Shop All rights reserved</span>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>