<?php
$imgwidth = "300";
$imgheight = "225";
?>
<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>
<?php
if(isset($_POST['btnAdd'])){

    $success="";
    $error="";
	
    $title=addslashes($_POST['title']);
	if($title=="")
	{
	 $error .="Please enter a title<br>";
	}
	
	$url=addslashes($_POST['url']);
	if(!empty($url) AND !preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url)){
	$error .="Please enter correct URL.<br>";
	}
	
    $sortby=addslashes($_POST['sortby']);
	if($sortby=="")
	{
	 $error .="Please enter a display order value<br>";
	}

	if($error=="")
	{

	 	mysql_query("INSERT INTO ".SPONSORS." (`title`, `url`, `sortby`, `created`) VALUES ('$title', '$url', '$sortby', NOW())") or die(__LINE__.mysql_error());
	 	$success = "Added successfully.<br/>";
	 
	 $insert_id = mysql_insert_id();
	 $id = $insert_id;
	 
	 $filename1 = $_FILES['file1']['name'];
		
		if(!empty($filename1) && isset($insert_id)){
		$imgext1 = strtolower(substr($filename1, -4));
		
		$title2 = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title))));
		
		
		$filename1 = "sponsor-".$id."-".$title2.$imgext1;
		$savefile1 = "../pictures/".$filename1;
		//upload  
		if(copy($_FILES['file1']['tmp_name'], $savefile1)){
			//echo "....Image uploaded ";
		}else{echo "Failed to upload image! ";}
		chmod("$savefile1",0777);
		
		if(resize_picture("$savefile1","$savefile1","$imgwidth","$imgheight")){
		//echo "....Image resized ";
		
		$image = $filename1;
		
			if(mysql_query("UPDATE ".SPONSORS." SET image='".$image."' WHERE id='".$id."'")){
				//$success .= "Image uploaded.<br/>";
			 } else {die(__LINE__.mysql_error());}
		
		
		}else{echo "Failed to resize image! ";}
			
		
		
		}
	 
	unset($_GET);
	unset($_POST);
	 

	 }
   
}
?>
<?php
if(isset($_POST['btnEditDo'])){

	$success="";
	$error="";

	$id = $_GET['editid'];
    $title=addslashes($_POST['title']);
	if($title=="")
	{
	 $error .="Please enter title<br>";
	}


	$url=addslashes($_POST['url']);
	if(!empty($url) AND !preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url)){
	$error .="Please enter correct URL.<br>";
	}
	
    $sortby=addslashes($_POST['sortby']);
	if($sortby=="")
	{
	 $error .="Please enter a display order value<br>";
	}

	$image = $_POST['image'];

	if($error=="")
	{

		$filename1 = $_FILES['file1']['name'];
		
		if(!empty($filename1)){
		$imgext1 = strtolower(substr($filename1, -4));
		
		$title2 = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title))));
		
		$filename1 = "sponsor-".$id."-".$title2.$imgext1;
		$savefile1 = "../pictures/".$filename1;
		//upload  
		if(copy($_FILES['file1']['tmp_name'], $savefile1)){//echo "....Image uploaded ";
		}else{echo "Failed to upload image! ";}
		chmod("$savefile1",0777);
		
		if(resize_picture("$savefile1","$savefile1","$imgwidth","$imgheight")){
		
			mysql_query("UPDATE ".SPONSORS." SET image='".$filename1."' WHERE id='$id'") or die(__LINE__.mysql_error());
		
		}else{echo "Failed to resize image! ";}


		}
			mysql_query("UPDATE ".SPONSORS." SET title='$title', url='$url', sortby='$sortby' WHERE id='$id'") or die(__LINE__.mysql_error());
			
		$success .= "Updated successfully.";
		unset($_GET);
		unset($_POST);		
		

	}else{
	$_POST['btnEdit'] = $_POST['id'];
	}

}
?>
<?php if(isset($_GET['delete']) AND !empty($_GET['delete'])){

$delid = $_GET['delete'];

	$result= mysql_query("SELECT * FROM ".SPONSORS." WHERE id = '$delid'");
		while($row = mysql_fetch_array($result)){
		if(!unlink("../pictures/".$row['image'])) {$warning = "Can not delete image";}
		}
	$sql		=	"DELETE FROM ".SPONSORS." WHERE id = '$delid'";
	mysql_query($sql) or die(mysql_error());
	$success	= "Successfully deleted.<br/>";
	unset($_GET);

}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php if(isset($_GET['add'])){?>

<fieldset>
<legend>
<h2>Add Sponsor </h2>
</legend>
<form action="" method="post" enctype="multipart/form-data" name="form-add">
  <table class="form-table">
    <tr>
      <td width="100">Title <font color="#FF0000">*</font> </td>
      <td><input type="text" name="title" value="<?=$_POST['title'];?>"></td>
    </tr>
    <tr>
      <td>URL:</td>
      <td><input type="text" name="url" value="<?=$_POST['url'];?>">
        Example: http://www.google.com</td>
    </tr>
    <tr>
      <td>Display Order:<font color="#FF0000">*</font></td>
      <td><input type="text" name="sortby" value="<?=$_POST['sortby'];?>" size="3">
        </td>
    </tr>
    <tr>
      <td>Image:</td>
      <td><input type="file" name="file1"></td>
    </tr>
    <tr>
      <td></td>
      <td>Dimensions:
        <?=$imgwidth?>
        x
        <?=$imgheight?>
        (Max: 2MB)&nbsp;<br />
        JPG format is the one recommended.</td>
    </tr>
    <tr>
      <td></td>
      <td><input name="btnAdd" type="submit" class="button" value="Submit">      </td>
    </tr>
  </table>
</form>
</fieldset>
<?php }
elseif(isset($_GET['editid'])){

$result= mysql_query("SELECT * FROM ".SPONSORS." WHERE id = '".$_GET['editid']."'");
while($row = mysql_fetch_array($result)){
?>
<fieldset>
<legend>
<h2>Edit Sponsor </h2>
</legend>
<form action="" method="post" enctype="multipart/form-data" name="form1">
  <table class="form-table">
    <tr>
      <td width="100">Title: <font color="#FF0000">*</font></td>
      <td><input type="text" name="title" value="<?php echo stripslashes($row['title']);?>"></td>
    </tr>
    <tr>
      <td>URL:</td>
      <td><input type="text" name="url" value="<?php echo stripslashes($row['url']);?>">
        Example: http://www.google.com</td>
    </tr>
	<tr>
      <td>Display Order: <font color="#FF0000">*</font></td>
      <td><input name="sortby" type="text" id="sortby" value="<?=$row['sortby']?>" size="3" /></td>
    </tr>
    <tr>
      <td></td>
      <td><?php print_thumb("../pictures/$row[image]", "100", "100"); ?> </td>
    </tr>
    <tr>
      <td>Image</td>
      <td><input type="file" name="file1"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>Dimensions:
        <?=$imgwidth?>
        x
        <?=$imgheight?>
        (Max: 2MB)&nbsp;<br />
        JPG format is the one recommended.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="btnEditDo" value="Update" class="button">
        <input name="editid" type="hidden" value="<?=$row['id']?>" />      </td>
    </tr>
  </table>
</form>
<?php
} // eof while

}else{ ?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="200">Preview</th>
      <th width="200">Name</th>
      <th width="50" class="headerSortUp">Order</th>
      <td width="50">Edit</td>
      <td width="50">Delete</td>
    </tr>
  </thead>
  <tbody>
	<?php
	$result= mysql_query("SELECT * FROM ".SPONSORS." ORDER BY sortby ASC");
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?php print_thumb("../pictures/$row[image]", "100", "100"); ?></td>
      <td><?=stripslashes($row['title'])?></td>
      <td><?=$row['sortby']?></td>
      <td><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php } ?>
<?php

$create = 'CREATE TABLE IF NOT EXISTS `sponsors_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sortby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;';
mysql_query($create);

?>