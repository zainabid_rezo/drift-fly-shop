-- phpMyAdmin SQL Dump
-- version 3.4.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2013 at 11:40 AM
-- Server version: 5.5.31
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `muddnflo_muddfld`
--

-- --------------------------------------------------------

--
-- Table structure for table `sponsors_posts`
--

DROP TABLE IF EXISTS `sponsors_posts`;
CREATE TABLE IF NOT EXISTS `sponsors_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sortby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `sponsors_posts`
--

INSERT INTO `sponsors_posts` (`id`, `title`, `image`, `url`, `sortby`, `created`) VALUES
(14, 'Black Diamond', 'sponsor-14-blackdiamond.jpg', 'http://blackdiamond-inc.com', 10, '2012-06-25 11:49:18'),
(16, 'Asolo', 'sponsor-16-asolo.gif', '', 6, '2012-08-07 15:24:09'),
(17, 'Atlas', 'sponsor-17-atlas.gif', '', 11, '2012-08-07 15:24:39'),
(18, 'Camelbak', 'sponsor-18-camelbak.gif', '', 9, '2012-08-07 15:25:02'),
(19, 'Fischer', 'sponsor-19-fischer.gif', '', 12, '2012-08-07 15:25:21'),
(20, 'Kuhl', 'sponsor-20-kuhl.gif', '', 3, '2012-08-07 15:25:39'),
(21, 'Marmot', 'sponsor-21-marmot.gif', '', 5, '2012-08-07 15:25:56'),
(22, 'Merrell', 'sponsor-22-merrell.gif', '', 7, '2012-08-07 15:26:13'),
(23, 'Osprey', 'sponsor-23-osprey.gif', '', 8, '2012-08-07 15:26:31'),
(24, 'Patagonia', 'sponsor-24-patagonia.gif', '', 1, '2012-08-07 15:26:47'),
(25, 'Prana', 'sponsor-25-prana.gif', '', 2, '2012-08-07 15:27:04'),
(26, 'Smartwool', 'sponsor-26-smartwool.gif', '', 4, '2012-08-07 15:27:24');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
