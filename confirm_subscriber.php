<?php
require("manager/conn.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>
Confirm Subscriber
</title>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300italic,400italic,600,600italic,700,300' rel='stylesheet'>
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="css/style.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


<script>
    function checkvalue()
    {
        var semail =  document.getElementById('email').value;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		  // var address = document.forms[form_id].elements[email].value;
		   if(reg.test(semail) == false) {
			  document.getElementById('error_se').innerHTML = 'Invalid email address<br />';
			  return false;
		   }
                   else{
                    return true;
                   }
    
    
    }    
</script>

<script type="text/javascript">
		function subscribe(){
	var fname = $('#fname').val();
	if(fname == ''){
		document.getElementById('show_err1').innerHTML='<span style="color:red;">Please enter first name</span>';
		return false;
	}else{
		document.getElementById('show_err1').innerHTML='';
		}
	var lname = $('#lname').val();
	var email = $('#email').val();
	var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
		if(email == '' || atpos< 1 || dotpos<atpos+2 || dotpos+2>=email.length){
			document.getElementById('show_err').innerHTML='<span style="color:red;">Enter email address</span>';
			return false;
		}else{
		document.getElementById('show_err').innerHTML='';
		}
	
	var myData = "email="+email+"&fname="+fname+"&lname="+lname;
	jQuery.ajax({
			type: "POST", // Post / Get method
			url: "subscribe.php", //Where form data is sent on submission
			dataType:"text", // Data type, HTML, json etc.
			data:myData, //Form variables
			success:function(response){
				if(response != 0){
					document.getElementById('show_err3').innerHTML='<span style="color:green;">An email has been sent to your email address.<br> Please click the confirmation link to complete your sign up process.</span>';
					$('#hideall').hide();
				}else{
					document.getElementById('show_err4').innerHTML='<span style="color:red;">This Email already exist</span>';
				}
			},
			error:function (xhr, ajaxOptions, thrownError){
				//On error, we alert user
				alert(thrownError);
			}
			});
}
	</script>

</head>
<body>
	<div id="wrapper">
		<div class="container">
			<div class="bg-box">
				<header id="header">
					<div class="starbrust">
				     	<a href="guidedtrips.php">Book a Trip</a>
				    </div>
					<div class="phone-info">
						<div class="clearfix">
							<img src="images/icon-telephone.png" alt="icon phone">
							<span class="number">(719) 543-3900</span>
						</div>
						<a href="javascript:void(0);" class="newsletter-signup" data-toggle="modal" data-target="#myModal">Newsletter Signup</a>
					</div>
					<nav id="nav" class="navbar navbar-default">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapsed" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="navbar-collapsed">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="index.php">Home</a></li>
								<li><a href="our_guides.php">Our Guides</a></li>
								<li><a href="flyshop.php">Fly Shop</a></li>
								<li><a href="guidedtrips.php">Guided trips</a></li>
                                <li><a href="classes.php">Classes</a></li>
								<li><a href="localwater.php">Local Waters</a></li>
								<li><a href="picture_gallery.php">Photo Gallery</a></li>
								<li><a href="http://ogradyflyfishing.blogspot.com/" target="_blank">Blog</a></li>
								<li><a href="contact.php">Contact</a></li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</nav>
				</header>
                
                <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Newsletter Signup</h4>
        </div>
        <div class="modal-body">
          <span id="show_err3"></span>
	<div id="hideall">
	<table width="400" cellpadding="5" cellspacing="5">
    	
        <tr>
        <td>&nbsp;</td><td><h3>Signup</h3><span id="show_err4"></span></td>
        </tr>
        
        <tr>
        <th>First Name</th><td><input type="text" name="fname" id="fname" /><br /><span id="show_err1"></span></td>
        </tr>
        
        <tr>
        <th>Last Name</th><td><span id="show_err2"></span><input type="text" name="lname" id="lname" /></td>
        </tr>
    	
        <tr>
        <th>Email</th><td><input type="text" name="email" id="email" /><br /><span id="show_err"></span></td>
        </tr>
        
        <tr>
        <td>&nbsp;</td><td><input type="button" onclick="subscribe();" value="Subscribe" name="subscribe" id="subscribe" /></td>
        </tr>
        
    
    </table>
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
 
		
	<main id="main">
					<div class="content-wrap">
						<div class="row mb40">
							<div class="col-sm-8">
								<section class="text-wrap">
									<h2>Contact</h2>
      
<span style="color:#E6E6E6;"><h1 class="boxfont style3">Confirm Subscription</h1></span><br/>
                <?php
$error = '';
if(isset($_GET['updateid'])){
	$check = mysql_query("select * from newsletter_subscribers where id = '".$_GET['updateid']."' and status = 0") or die(mysql_error());
	if(mysql_num_rows($check)>0){
		mysql_query("update newsletter_subscribers set status = 1 where id = '".$_GET['updateid']."'") or die(mysql_error());
	}else{
		$error = 'Link is invalid';
	}
}
?>
		<span style="color:#E6E6E6;"><?php if($error != ''){ ?><h1 class="boxfont style3"><?php echo $error; ?></h1>
		<?php }else{ ?> <h1 class="boxfont style3">Subscription Confirmed.</h1><br />Your subscription to our Newsletter has been confirmed.
		<br /><br />Thanks for subscribing to our Newsletter!<?php } ?></span><br/>
        
        
     </section>
							</div>
							
						</div>
					</div>
				</main>
				<?php require('includes/footer.php'); ?>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script>
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
				document.createTextNode(
					'@-ms-viewport{width:auto!important}'
					)
				)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>

</body>

</html>