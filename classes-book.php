<?php
if($_SERVER['HTTPS']!="on")
{
$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
header("Location:$redirect");
}
?>
<?php
include_once("manager/conn.php");
include_once("manager/db-tables.php");
include_once("manager/site-details.php");
include_once("manager/functions.php");
$class_id = (int)$_GET['class_id'];
?>
<?php
include_once("manager/send-email-class.php");
include_once("manager/send-email-to-admin.php");
include_once("manager/send-email-to-attendee.php");

include_once("reservations/manager/functions/authorize_dot_net.php");
?>
<?php
if(isset($_POST['btnContinue'])){

$success = "";
$error = "";

	$firstname	=	$_SESSION['firstname']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['firstname']))));
	if(empty($firstname))$error .= "Please enter your first name.<br/>";
	
	$lastname	=	$_SESSION['lastname']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['lastname']))));
	if(empty($lastname))$error .= "Please enter your last name.<br/>";
	
	$address1	=	$_SESSION['address1']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['address1']))));
	if(empty($address1))$error .= "Please enter your address line 1.<br/>";
	
	$address2	=	$_SESSION['address2']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['address2']))));
	
	$city		=	$_SESSION['city']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['city']))));
	if(empty($city))$error .= "Please enter your city name.<br/>";
	
	$state		=	$_SESSION['state']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['state']))));
	if(empty($state))$error .= "Please enter your state.<br/>";
	
	$zip		=	$_SESSION['zip']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['zip']))));
	if(empty($zip))$error .= "Please enter your zip code.<br/>";
	
	$phone = mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['phone1']))))."-".mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['phone2']))))."-".mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['phone3']))));
	$_SESSION['phone1'] = mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone1'])));	$_SESSION['phone2'] = mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone2'])));	$_SESSION['phone3']	= mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone3'])));
	if(empty($_POST['phone1']) OR empty($_POST['phone2']) OR empty($_POST['phone3'])) $error .= "Please enter your phone correctly.<br/>";

	$email		=	$_SESSION['email']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['email']))));
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) $error .= "Please enter correct email address.<br/>";
	$emailconfirm		=	$_SESSION['emailconfirm']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['emailconfirm']))));
	if($email !== $emailconfirm) $error .= "The confirmation email address does not match.<br/>";
	
	if(empty($error)){$go_to_form_2 = true;}
	
}

if(isset($_POST['btnReserve'])){

$success = "";
$error = "";

	$class_id	=	$_SESSION['class_id']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['class_id']))));
	if(empty($class_id))$error .= "Please select a class.<br/>";
	
	$dates		=	$_SESSION['dates']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['dates']))));
	if(empty($dates))$error .= "Date of class is missing.<br/>";
	
	$firstname	=	$_SESSION['firstname']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['firstname']))));
	if(empty($firstname))$error .= "Please enter your first name.<br/>";
	
	$lastname	=	$_SESSION['lastname']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['lastname']))));
	if(empty($lastname))$error .= "Please enter your last name.<br/>";
	
	$address1	=	$_SESSION['address1']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['address1']))));
	if(empty($address1))$error .= "Please enter your address line 1.<br/>";
	
	$address2	=	$_SESSION['address2']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['address2']))));
	
	$city		=	$_SESSION['city']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['city']))));
	if(empty($city))$error .= "Please enter your city name.<br/>";
	
	$state		=	$_SESSION['state']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['state']))));
	if(empty($state))$error .= "Please enter your state.<br/>";
	
	$zip		=	$_SESSION['zip']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['zip']))));
	if(empty($zip))$error .= "Please enter your zip code.<br/>";
	
	$phone = mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['phone1']))))."-".mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['phone2']))))."-".mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['phone3']))));
	$_SESSION['phone1'] = mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone1'])));	$_SESSION['phone2'] = mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone2'])));	$_SESSION['phone3']	= mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone3'])));
	if(empty($_POST['phone1']) OR empty($_POST['phone2']) OR empty($_POST['phone3'])) $error .= "Please enter your phone correctly.<br/>";
	
	$email		=	$_SESSION['email']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['email']))));
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $email)) $error .= "Please enter correct email address.<br/>";
	
	$notes		=	$_SESSION['notes']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['notes']))));

	$number_of_people		=	$_SESSION['number_of_people']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['number_of_people']))));
	if(empty($number_of_people))$error .= "Please select number of people.<br/>";

	$sub_total		=	$_SESSION['sub_total']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['sub_total']))));
	

	$sales_tax		=	$_SESSION['sales_tax']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['sales_tax']))));
	

	$total_price		=	$_SESSION['total_price']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['total_price']))));

	$status		=	"2";
	
	if($_POST['free_class'] == 0){
		
		if(empty($sub_total))$error .= "No sub total calculated.<br/>";
		if(empty($sales_tax))$error .= "No sales tax calculated.<br/>";
		if(empty($total_price))$error .= "No total price calculated.<br/>";

	$ccnumber	=	$_SESSION['ccnumber']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['ccnumber']))));
	//if(empty($ccnumber)) $error .= "Please enter credit card number.<br/>";
	$ccexpM		=	$_SESSION['ccexpM']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['ccexpM']))));
	//if(empty($ccexpM)) $error .= "Please select expiration month.<br/>";
	$ccexpY		=	$_SESSION['ccexpY']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['ccexpY']))));
	//if(empty($ccexpY)) $error .= "Please select expiration year.<br/>";
	$cvv		=	$_SESSION['cvv']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['cvv']))));
	//if(empty($cvv)) $error .= "Please enter cvv.<br/>";
	$amount_deposited = $amountbilled =	$_SESSION['amountbilled']	=	mysql_real_escape_string(htmlspecialchars(strip_tags(addslashes($_POST['amountbilled']))));

	if(empty($error)){
	/////////////////////////////////////////////////////

		$firstname=addslashes($firstname);
	$lastname=addslashes($lastname);
	$address1=addslashes($address1);
	$address2=addslashes($address2);
	$city=addslashes($city);
	$state=addslashes($state);
	$zip=addslashes($zip);
	

		$customer_id = time();

	$invoice_number = $customer_id*2;
		
		$customer_profile_id = authorize_create_customer_profile($email,$customer_id);

				

				if(!empty($customer_profile_id)){

				$customer_payment_profile_id = authorize_create_customer_payment_profile($customer_profile_id,$firstname,$lastname,$address1,$city,$state,$zip,$phone,$ccnumber,$ccexpY,$ccexpM);

				

				}
				
				if(!empty($customer_payment_profile_id)){
					
				$transaction_response = authorize_create_transaction($amount_deposited,$customer_profile_id,$customer_payment_profile_id,$invoice_number);
				//$_SESSION['amount_paid'] = $amount;

				}
	
	
	
				if($transaction_response != "Ok"){
					$error .= 'Transaction Failure';
					
					//$error .= $authorize_error;
				}
	}
	
}
	
	//////////////////////////////////////////////////////
	
	if(empty($error)){
	
	$sql		=	"INSERT INTO ".ATTENDEES." (`class_id`, `dates`, `firstname`, `lastname`, `address1`, `address2`, `city`, `state`, `zip`, `phone`, `email`, `number_of_people`, `sub_total`, `sales_tax`, `total_price`, `amount_deposited`, `created`) VALUES ('$class_id', '$dates', '$firstname', '$lastname', '$address1', '$address2', '$city', '$state', '$zip', '$phone', '$email', '$number_of_people', '$sub_total', '$sales_tax', '$total_price', '$amount_deposited', NOW())";
	mysql_query($sql) or die(__LINE__.mysql_error());
	$insert_id =  mysql_insert_id();
	
	//$sql		=	"INSERT INTO newsletter_subscribers (`fname`, `lname`, `email`, `groupid`, `created`) VALUES ('$firstname', '$lastname', '$email','10', NOW())";
	//mysql_query($sql) or die(mysql_error());
	
	$success	= "Your reservation request has been saved successfully.<br />";
	$closewindow = true;
	
			send_email_to_admin($insert_id);
			//$warning = "Error sending email to  ".ADMIN_EMAIL;
			//$warning .= "<br/>".$emailwarning;
			$success .= "Email sent to ".ADMIN_EMAIL;
			/*}else{
			
			}*/
	
			send_email_to_attendee($insert_id);
			$success .= "Email sent to ".ADMIN_EMAIL;
			/*$error .= "Error sending receipt email. Please contact directly at ".ADMIN_EMAIL;
			$error .= "<br/>".$emailwarning;
			}else{
			
			}*/
	
						
			
			//Curl Subscribe
			$ch = curl_init();
			$timeout = 5;
			curl_setopt($ch, CURLOPT_URL, 'http://taosflyshop.com/manager/enews/subscribe-curl.php?fname='.urlencode($firstname).'&lname='.urlencode($lastname).'&email='.urlencode($email));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$newsletter_subscribed = curl_exec($ch);
			curl_close($ch);
			if($newsletter_subscribed=='true') $success	.= "You have been subscribed to newsletter system.<br />";
			
	
	}else{$go_to_form_2 = true;}
	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>
<?=stripslashes(SITE_NAME)?>
</title>
<link type="text/css" href="manager/css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="manager/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="manager/js/easyTooltip.js"></script>
<script type="text/javascript" src="manager/js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="manager/js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="manager/js/hoverIntent.js"></script>
<script type="text/javascript" src="manager/js/superfish.js"></script>
<script type="text/javascript" src="manager/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="manager/js/searchbox.js"></script>
<script type="text/javascript" src="manager/js/custom.js"></script>
<style type="text/css">
<!--
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	background:#303030;
	color:#ffffff;
	margin:20px;
}
h2{
	color:#DDAE68;
}
label{
	color:#0066CC;
}
a{
	color:#0066CC;
}
-->
</style>

</head>
<body id="top">
<!------------------------------------------------------------------------------>
<?php include"manager/messages-display.php";?>
<?php 
if(isset($closewindow)){ ?>
<br>
<br>
<br>
<br>
<br>
<br>
<div align="center">
  <input type="button" onClick="window.close()" value="CLOSE" class="button" >
</div>
<?php }else{ ?>
<?php
$sql = "SELECT * FROM ".CLASSES." WHERE id = '".(int)$_GET['class_id']."'";
$result= mysql_query($sql) or die(mysql_error());
while($row = mysql_fetch_array($result)){
	
	$sel_class = mysql_query("SELECT * FROM ".CLASSES." WHERE id = '".$_GET['class_id']."'") or die(mysql_error());
	  	$row_class = mysql_fetch_array($sel_class);
?>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
    <tr>
      <td width="200" rowspan="8" style="text-align:center"><h1 style="color:#FFCC00"><?=html_entity_decode(stripslashes($row['title']))?></h1>
        <input name="class_id" id="class_id" type="hidden" value="<?=$row['id']?>" />
        <h2><?=html_entity_decode(stripslashes($row['dates']))?></h2>
        <input name="dates" id="dates" type="hidden" value="<?=$row['dates']?>" /></td>
      <td width="200" style="text-align:center"># of people:</td>
      <?php if($row_class['free_class'] == 0){ ?>
      <td width="200" style="text-align:center">Cost Each </td>
      <td width="200" style="text-align:right">Total</td>
      <?php } ?>
    </tr>
    <tr>
      <td style="text-align:center"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_GET['numberofpeople'])))?>
        <input name="number_of_people" type="hidden" value="<?=mysql_real_escape_string(htmlspecialchars(strip_tags($_GET['numberofpeople'])));?>" /></td>
      <?php if($row_class['free_class'] == 0){ ?>
      <td style="text-align:center">$
        <?=money(stripslashes($row['price']))?></td>
      <td style="text-align:right">$<?php echo $price = money(stripslashes($row['price']*mysql_real_escape_string(htmlspecialchars(strip_tags($_GET['numberofpeople'])))))?></td>
    <?php } ?>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align:right">&nbsp;</td>
      <?php if($row_class['free_class'] == 0){ ?>
      <td style="text-align:right">Sub Total:</td>
      <td style="text-align:right">$<?php echo $price = money($price); ?>
      </td>
      <?php } ?>
      <input name="sub_total" type="hidden" id="sub_total" value="<?=$price?>" readonly="readonly"/>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align:right">&nbsp;</td>
      <?php if($row_class['free_class'] == 0){ ?>
      <td style="text-align:right">Sales Tax (
        <?=SALES_TAX_RATE?>
%):</td>
      <td style="text-align:right">$<?php echo $tax_price = money($price*(SALES_TAX_RATE/100)); ?>
	  </td>
      <?php } ?>
      <input name="sales_tax" type="hidden" id="sales_tax" value="<?=$tax_price?>" readonly="readonly"/>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align:right">&nbsp;</td>
      <?php if($row_class['free_class'] == 0){ ?>
      <td style="text-align:right">Total:</td>
      <td style="text-align:right">$<?php echo $total_price = money($price+$tax_price); ?>
        </td>
        <?php } ?>
        <input name="total_price" type="hidden" id="total_price" value="<?=$total_price?>" readonly="readonly"/>
    </tr>
    <tr>
      <td colspan="4" style="text-align:center">&nbsp;</td>
    </tr>
    <?php if($row_class['free_class'] == 0){ ?>
    <tr>
      <td colspan="4" style="text-align:center">We require a 50% deposit to hold your reservation, you will be paying <strong>$<?php echo $amountbilled = money($total_price/2); ?></strong> today. </td>
    </tr>
    <?php } ?>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><table class="form-table">
          <tr>
            <th width="150" scope="row">First name<font color="#FF0000">*</font></th>
            <td width="150"><input name="firstname" type="text" id="firstname" value="<?=$_SESSION['firstname']?>" size="20" required /></td><td width="100"></td>
          </tr>
          <tr>
            <th scope="row">Last name<font color="#FF0000">*</font></th>
            <td><input name="lastname" type="text" id="lastname" value="<?=$_SESSION['lastname']?>" size="20" required /></td><td></td>
          </tr>
          <tr>
            <th scope="row">Address Line 1<font color="#FF0000">*</font></th>
            <td><input name="address1" type="text" id="address1" value="<?=$_SESSION['address1']?>" size="25" required /></td><td></td>
          </tr>
          <tr>
            <th scope="row">Address Line 2 </th>
            <td><input name="address2" type="text" id="address2" value="<?=$_SESSION['address2']?>" size="25" /></td><td></td>
          </tr>
          <tr>
            <th scope="row">City<font color="#FF0000">*</font></th>
            <td><input name="city" type="text" id="city" value="<?=$_SESSION['city']?>" size="25" required /></td><td></td>
          </tr>
          <tr>
            <th scope="row">State<font color="#FF0000">*</font></th>
            <td><select name="state" required >
                <option value="0" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="0"){echo 'selected="selected"';}?> > - select - </option>
                <option value="AL" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="AL"){echo 'selected="selected"';}?> >Alabama</option>
                <option value="AK" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="AK"){echo 'selected="selected"';}?> >Alaska</option>
                <option value="AZ" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="AZ"){echo 'selected="selected"';}?> >Arizona</option>
                <option value="AR" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="AR"){echo 'selected="selected"';}?> >Arkansas</option>
                <option value="CA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="CA"){echo 'selected="selected"';}?> >California</option>
                <option value="CO" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="CO"){echo 'selected="selected"';}?> >Colorado</option>
                <option value="CT" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="CT"){echo 'selected="selected"';}?> >Connecticut</option>
                <option value="DE" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="DE"){echo 'selected="selected"';}?> >Delaware</option>
                <option value="DC" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="DC"){echo 'selected="selected"';}?> >District of Columbia</option>
                <option value="FL" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="FL"){echo 'selected="selected"';}?> >Florida</option>
                <option value="GA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="GA"){echo 'selected="selected"';}?> >Georgia</option>
                <option value="HI" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="HI"){echo 'selected="selected"';}?> >Hawaii</option>
                <option value="ID" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="ID"){echo 'selected="selected"';}?> >Idaho</option>
                <option value="IL" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="IL"){echo 'selected="selected"';}?> >Illinois</option>
                <option value="IN" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="IN"){echo 'selected="selected"';}?> >Indiana</option>
                <option value="IA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="IA"){echo 'selected="selected"';}?> >Iowa</option>
                <option value="KS" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="KS"){echo 'selected="selected"';}?> >Kansas</option>
                <option value="KY" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="KY"){echo 'selected="selected"';}?> >Kentucky</option>
                <option value="LA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="LA"){echo 'selected="selected"';}?> >Louisiana</option>
                <option value="ME" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="ME"){echo 'selected="selected"';}?> >Maine</option>
                <option value="MD" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MD"){echo 'selected="selected"';}?> >Maryland</option>
                <option value="MA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MA"){echo 'selected="selected"';}?> >Massachusetts</option>
                <option value="MI" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MI"){echo 'selected="selected"';}?> >Michigan</option>
                <option value="MN" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MN"){echo 'selected="selected"';}?> >Minnesota</option>
                <option value="MS" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MS"){echo 'selected="selected"';}?> >Mississippi</option>
                <option value="MO" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MO"){echo 'selected="selected"';}?> >Missouri</option>
                <option value="MT" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="MT"){echo 'selected="selected"';}?> >Montana</option>
                <option value="NE" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NE"){echo 'selected="selected"';}?> >Nebraska</option>
                <option value="NV" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NV"){echo 'selected="selected"';}?> >Nevada</option>
                <option value="NH" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NH"){echo 'selected="selected"';}?> >New Hampshire</option>
                <option value="NJ" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NJ"){echo 'selected="selected"';}?> >New Jersey</option>
                <option value="NM" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NM"){echo 'selected="selected"';}?> >New Mexico</option>
                <option value="NY" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NY"){echo 'selected="selected"';}?> >New York</option>
                <option value="NC" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="NC"){echo 'selected="selected"';}?> >North Carolina</option>
                <option value="ND" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="ND"){echo 'selected="selected"';}?> >North Dakota</option>
                <option value="OH" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="OH"){echo 'selected="selected"';}?> >Ohio</option>
                <option value="OK" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="OK"){echo 'selected="selected"';}?> >Oklahoma</option>
                <option value="OR" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="OR"){echo 'selected="selected"';}?> >Oregon</option>
                <option value="PA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="PA"){echo 'selected="selected"';}?> >Pennsylvania</option>
                <option value="RI" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="RI"){echo 'selected="selected"';}?> >Rhode Island</option>
                <option value="SC" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="SC"){echo 'selected="selected"';}?> >South Carolina</option>
                <option value="SD" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="SD"){echo 'selected="selected"';}?> >South Dakota</option>
                <option value="TN" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="TN"){echo 'selected="selected"';}?> >Tennessee</option>
                <option value="TX" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="TX"){echo 'selected="selected"';}?> >Texas</option>
                <option value="UT" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="UT"){echo 'selected="selected"';}?> >Utah</option>
                <option value="VT" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="VT"){echo 'selected="selected"';}?> >Vermont</option>
                <option value="VA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="VA"){echo 'selected="selected"';}?> >Virginia</option>
                <option value="WA" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="WA"){echo 'selected="selected"';}?> >Washington</option>
                <option value="WV" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="WV"){echo 'selected="selected"';}?> >West Virginia</option>
                <option value="WI" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="WI"){echo 'selected="selected"';}?> >Wisconsin</option>
                <option value="WY" <?php if(isset($_SESSION['state']) AND $_SESSION['state']=="WY"){echo 'selected="selected"';}?> >Wyoming</option>
              </select></td><td></td>
          </tr>
          <tr>
            <th scope="row">Zip<font color="#FF0000">*</font></th>
            <td><input name="zip" type="text" id="zip" size="5" value="<?=$_SESSION['zip']?>" required /></td><td></td>
          </tr>
          <tr>
            <th scope="row">Phone<font color="#FF0000">*</font></th>
            <td><script type="text/javascript">
			var isNN = (navigator.appName.indexOf("Netscape")!=-1);
			
			function autoTab(input,len, e) {
			  var keyCode = (isNN) ? e.which : e.keyCode; 
			  var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
			  if(input.value.length >= len && !containsElement(filter,keyCode)) {
				input.value = input.value.slice(0, len);
				input.form[(getIndex(input)+1) % input.form.length].focus();
			  }
			
			  function containsElement(arr, ele) {
				var found = false, index = 0;
				while(!found && index < arr.length)
				if(arr[index] == ele)
				found = true;
				else
				index++;
				return found;
			  }
			
			  function getIndex(input) {
				var index = -1, i = 0, found = false;
				while (i < input.form.length && index == -1)
				if (input.form[i] == input)index = i;
				else i++;
				return index;
			  }
			  return true;
			}
			
			</script>
              <input name="phone1" type="text" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_SESSION['phone1']?>" required />
              <input name="phone2" type="text" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_SESSION['phone2']?>" required />
              <input name="phone3" type="text" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4" value="<?=$_SESSION['phone3']?>" required /></td><td></td>
          </tr>
          <tr>
            <th scope="row">Email<font color="#FF0000">*</font></th>
            <td><input name="email" type="text" id="email" value="<?=$_SESSION['email']?>" size="35" onKeyUp="matchEmail()" onBlur="matchEmail()" required /></td><td></td>
          </tr>
          <tr>
            <th scope="row">Confirm Email<font color="#FF0000">*</font></th>
            <td><input name="emailconfirm" type="text" id="emailconfirm" value="<?=$_SESSION['emailconfirm']?>" size="35" onKeyUp="matchEmail()" onBlur="matchEmail()" required /></td>
            <td><span id="emailmatchmessage"></span>
              <script language="JavaScript" type="text/JavaScript">
				function matchEmail() {
				    if (document.getElementById('emailconfirm').value != ""){
				 
						if (document.getElementById('email').value != document.getElementById('emailconfirm').value)
						{	
							document.getElementById('emailmatchmessage').innerHTML = "<font color=\"#ff0000\">Email does not match.</font>";
							return false;
						} else {
							document.getElementById('emailmatchmessage').innerHTML = "<img src=ok.gif />";
							return true;
						}
					}
				}
				</script></td>
          </tr>
          
        </table></td>
      <td colspan="2" valign="top">
      
      
	  <table class="form-table">
      
      
          <?php
	  	
		
		if($row_class['free_class'] == 0){
	  ?>
          <tr>
            <td>CC Number: </td>
          </tr>
          <tr>
            <td><input type="text" name="ccnumber" value="" />              <label></label></td>
          </tr>
          <tr>
            <td><img src="creditcards.png" width="165" height="27" /></td>
          </tr>
          <tr>
            <td>Expiration:</td>
          </tr>
          <tr>
            <td><select name="ccexpM">
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
              MM
              <select name="ccexpY">
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="20">20</option>
              </select>
              YY</td>
          </tr>
          <tr>
            <td>CVV: </td>
          </tr>
          <tr>
            <td><input name="cvv" type="text" id="cvv" size="5" maxlength="4" /></td>
          </tr>
          <?php } ?>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="text-align:center"><input type="submit" name="btnReserve" class="button" value="Reserve" />
            <input type="hidden" name="free_class" value="<?=$row_class['free_class']?>" />
        
        <input type="hidden" name="total_price" value="<?php echo $total_price;?>" />
        <input type="hidden" name="amountbilled" value="<?php echo $amountbilled;?>" />	  </td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td colspan="4" style="text-align:center">	  	</td>
    </tr>
  </table>
</form>
<?php } ?>
<?php } ?>

</body>
</html>
