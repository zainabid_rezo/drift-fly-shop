-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 08, 2016 at 03:42 PM
-- Server version: 5.1.73
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thereell_SiteCMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `alt1` varchar(100) NOT NULL,
  `sortby` int(11) NOT NULL,
  `pagesid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `description`, `image`, `alt1`, `sortby`, `pagesid`) VALUES
(7, 'Banner 1', '<p>Banner 1</p>', 'employees-7-banner1.jpg', 'Banner 1', 1, 0),
(8, 'Banner 2', '<p>Banner 2</p>', 'employees-8-banner2.jpg', 'Cutthroat', 2, 0),
(15, 'Conejos', '<p>Conejos in the Fall</p>', 'employees-15-conejos.jpg', 'conejos river', 3, 0),
(16, 'Brown Trout', '<p>Big Brown in the net!</p>', 'employees-16-brown-trout.jpg', 'Rio Grande Brown Trout', 4, 0),
(17, 'Tailwalker', '<p>Jumping Trout</p>', 'employees-17-tailwalker.jpg', 'Santa Fe Trout', 5, 0),
(18, 'Brazos River', '<p>Evening hatch on The Brazos</p>', 'employees-18-brazos-river.jpg', 'brazos fly fishing', 6, 0),
(19, 'Pecos Trout', '<p>Pecos</p>', 'employees-19-pecos-trout.jpg', 'pecos river trout', 7, 0),
(20, 'New Mexico Fly Fishing', '<p>NM fish</p>', 'employees-20-new-mexico-fly-fishing.jpg', 'new mexico fly fishing', 8, 0),
(21, 'San Juan Rainbow', '<p>San Juan</p>', 'employees-21-san-juan-rainbow.jpg', 'San juan fly fishing', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `banner_show`
--

CREATE TABLE IF NOT EXISTS `banner_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagesid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `banner_show`
--

INSERT INTO `banner_show` (`id`, `pagesid`) VALUES
(1, 9),
(2, 11);

-- --------------------------------------------------------

--
-- Table structure for table `carriers_posts`
--

CREATE TABLE IF NOT EXISTS `carriers_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `carriers_posts`
--

INSERT INTO `carriers_posts` (`id`, `title`, `domain`, `sortby`) VALUES
(1, '------ USA ------', '0', 1),
(2, '7-11 Speakout', '@cingularme.com', 2),
(3, 'Alltel', '@message.alltel.com', 3),
(4, 'Ameritech', '@paging.acswireless.com', 4),
(5, 'AT&T', '@txt.att.net', 5),
(6, 'Bellsouth', '@sms.bellsouth.com', 6),
(7, 'Boost', '@myboostmobile.com', 0),
(8, 'CellularOne', '@mobile.celloneusa.com', 0),
(9, 'Cellular South', '@csouth1.com', 0),
(10, 'Centennial', '@cwemail.com', 0),
(11, 'Cincinnati Bell', '@gocbw.com', 0),
(12, 'Cingular', '@mobile.mycingular.com', 0),
(13, 'Cricket', '@mycricket.com', 0),
(14, 'Edge Wireless', '@sms.edgewireless.com', 0),
(15, 'Metro PCS', '@mymetropcs.com', 0),
(16, 'Nextel', '@messaging.nextel.com', 0),
(17, 'O2', '@mobile.celloneusa.com', 0),
(18, 'Orange', '@mobile.celloneusa.com', 0),
(19, 'Quest', '@qwestmp.com', 0),
(20, 'Rogers Wireless', '@pcs.rogers.com', 0),
(21, 'Sprint', '@messaging.sprintpcs.com', 0),
(22, 'T-Mobile USA', '@tmomail.com', 0),
(23, 'Teleflip', '@teleflip.com', 0),
(24, 'Telus Mobility', '@msg.telus.com', 0),
(25, 'US Cellular', '@email.uscc.net', 0),
(26, 'Verizon Wireless', '@vtext.com', 0),
(27, 'Virgin Mobile', '@vmobl.com', 0),
(28, '------ CANADA ------', '0', 0),
(29, 'Aliant', '@wirefree.informe.ca', 0),
(30, 'Bell Mobility', '@txt.bellmobility.ca', 0),
(31, 'Fido', '@fido.ca', 0),
(32, 'Koodo Mobile', '@msg.koodomobile.com', 0),
(33, 'MTS Mobility', '@text.mtsmobility.com', 0),
(34, 'Presidents Choice', '@mobiletxt.ca', 0),
(35, 'Rogers Wireless', '@pcs.rogers.com', 0),
(36, 'Sasktel Mobility', '@pcs.sasktelmobility.com', 0),
(37, 'Telus', '@msg.telus.com', 0),
(38, 'Virgin Mobile', '@vmobile.ca', 0),
(39, '------ United Kingdom ------', '0', 0),
(40, 'Orange', '@orange.net', 0),
(41, 'T-Mobile', '@t-mobile.uk.net', 0),
(42, 'Virgin Mobile', '@vxtras.com', 0),
(43, 'Vodafone', '@vodafone.net', 0),
(44, '------ Germany ------', '0', 0),
(45, 'T-Mobile', '@t-d1-sms.de', 0),
(46, 'Vodafone', '@vodafone-sms.de', 0),
(47, 'O2', '@o2online.de', 0),
(48, 'E-Plus', '@smsmail.eplus.de', 0),
(49, '------ Australia ------', '0', 0),
(50, 'T-Mobile/Optus Zoo', '@optusmobile.com.au', 0),
(51, 'SL Interactive', '@slinteractive.com.au', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_posts`
--

CREATE TABLE IF NOT EXISTS `cms_posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `contents` text,
  `title_meta` text NOT NULL,
  `keywords_meta` text NOT NULL,
  `description_meta` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `alt1` varchar(255) NOT NULL,
  `image_2` varchar(255) NOT NULL,
  `alt2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `cms_posts`
--

INSERT INTO `cms_posts` (`id`, `title`, `contents`, `title_meta`, `keywords_meta`, `description_meta`, `image`, `alt1`, `image_2`, `alt2`, `image_3`) VALUES
(1, 'Home', '&lt;p&gt;Welcome to Santa Fe&amp;rsquo;s premier Fly Shop and Guide Service!&amp;nbsp; We specialize in guiding public and private waters throughout northern New Mexico.&amp;nbsp; Our full service fly shop is conveniently located inside the De Vargas Mall and stocks everything you need to make your next fishing trip a success.&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Our staff of patient and professional instructors can help you maximize your time on the water, all the while improving your Fly Fishing skills.&amp;nbsp; We guide full and half day trips on the Pecos, Chama, Rio Grande, Brazos, Jemez and more!&lt;/p&gt;\r\n&lt;p&gt;In March of 2014 The Reel Life was purchased d by Nick Streit and Ivan Valdez and the store was moved to its current location inside the De Vargas Mall at 526 North Guadalupe. &amp;nbsp;Both native New Mexican&#039;s, Nick and Ivan have extensive expireince fly fishing and guiding in the Santa Fe area. &amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Nick also owns the Taos Fly Shop and has built a reputation as one of New Mexico&#039;s top guides. When he was 17 he was on the Junior US Fly Fishing team that placed 2nd in Europe.&amp;nbsp; Nick has guided and fished all over the world, cutting his teeth on rivers from Alaska to Argentina, not to mention the salt flats of the Caribbean. &amp;nbsp;&amp;nbsp;&lt;/p&gt;', 'The Reel Life | Santa Fe''s Premier Fly Fishing Company', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'We provide a complete experience  for the fly fisherman. From beginners to experts, we have what you need to enjoy all the New Mexico has to offer.', NULL, '', '', '', ''),
(9, 'Guided Profile', NULL, 'Guided Profile', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'Guided Profile', '', '', '', '', ''),
(10, 'Photo Gallery', NULL, 'Photo Gallery', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'Photo Gallery', '', '', '', '', ''),
(11, 'Local Water', NULL, 'Local Water', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'Local Water', '', '', '', '', ''),
(12, 'Travel', '&lt;p&gt;Every winter The Reel Life Crew heads south to the wilds of Patagonia to fish for big, wild trout in the streams, river and lakes that drain the Andes. &amp;nbsp;We are not affiliated with any particular lodge, but rather &lt;strong&gt;customize&lt;/strong&gt; our trips for our clients. With decades of expirience in Argentina, we can tailor the trip to our clients specific desires for accomidations, types of fishing (dry fly, streamer, wading, floating, etc.) &amp;nbsp;Email &lt;a title=&quot;nick@thereellife.com&quot; href=&quot;mailto:nick@thereellife.com&quot;&gt;nick@thereellife.com&lt;/a&gt; for more information.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;We look forward to another grand adventure to Alaska this summer. &amp;nbsp;Join us July 3 - 10, 2014 for another unforgettable excursion to Dave and Kim Egdorf&#039;s Camp on the Nushagak River, in the heart of Alaska&#039;s Bristol Bay region. We will be fishing light tackle to large rainbows, dolly varden, grayling as they seek their sustenance from one of the largest runs of salmon known to humankind. This is a true Alaska wilderness experience, replete with wildlife, spectacular views, professional and friendly service from the camp&#039;s highly-skilled staff and, of course, stupendous fishing. Email &lt;a title=&quot;toner@thereellife.com&quot; href=&quot;mailto:toner@thereellife.com&quot;&gt;toner@thereellife.com&lt;/a&gt; for more information.&lt;/p&gt;\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;div&gt;Depending on the time of year, your fish will be vulnerable to a well-fished sculpin, mouse, egg, or flesh pattern. Compared to what we encounter in the lower 48, an Alaska-grown rainbow is unique in its beauty and power; we fish 6 weight rods, mainly because these fish can easily break anything lighter.&lt;/div&gt;\r\n&lt;div&gt;&amp;nbsp;&lt;/div&gt;\r\n&lt;div&gt;Price: $4,595.00 (a bargain for Alaska). Includes all meals, 6 days guided fishing, and an experience you couldn&#039;t forget if you tried.&lt;/div&gt;\r\n&lt;p&gt;&lt;strong&gt;&lt;br /&gt;&lt;/strong&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;The Reel Life&lt;/strong&gt;&lt;span&gt;&amp;nbsp;has been selected by The Fly Shop of Redding, CA, long recognized as the leader in fly fishing travel, to team up to bring you the ultimate in fly fishing travel. The Fly Shop&#039;s Signature Destinations represent the best fly fishing lodges in the world and are exclusive only to customers of The Fly Shop,&amp;nbsp;&lt;/span&gt;&lt;strong&gt;The Reel Life&lt;/strong&gt;&lt;span&gt;&amp;nbsp;and a few other top-tier shops around the country.&lt;/span&gt;&lt;/p&gt;\r\n&lt;div&gt;&lt;span class=&quot;text-header-orange&quot;&gt;&lt;br /&gt;&lt;/span&gt;Signature Destinations are a cross section of the planet&#039;s finest fly fishing lodges, outfitters, and camps. They&#039;re an honor roll of great fishing spots, and the culmination of more than three decades of field exploration, experience, and hands-on involvement by the #1 team of fly fishing consultants in the business.&lt;span class=&quot;text-header-orange&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;br /&gt;Each &#039;Signature Destination&#039; carries the unqualified endorsement of The Fly Shop and&amp;nbsp;&lt;strong&gt;The Reel Life&lt;/strong&gt;. Its our guarantee that the quality of accomodations, experience and angling is exactly as advertised. Signature Destinations are represented exclusively by The Fly Shop and its exclusive network of fine fly shops like&amp;nbsp;&lt;strong&gt;The Reel Life&lt;/strong&gt;.&lt;span style=&quot;font-size: small;&quot;&gt;&lt;span&gt;&lt;br /&gt;&lt;br /&gt;These are not blind endorsements purchased with a checkbook; they are a stamp of approval earned only after they&#039;ve met the highest performance standards in the industry.&lt;/span&gt;&lt;/span&gt;&lt;/div&gt;', 'Travel', 'Travel', 'Travel', 'cms-12-.jpg', '', '', '', ''),
(13, 'Fishing reports', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;04/26/2016&lt;/p&gt;\r\n&lt;p&gt;Runoff!!&lt;/p&gt;\r\n&lt;p&gt;Well good morning fellow long rodders! Fishing has tapered off a bit lately with high flows making their presence known here in Northern New Mexico. I would expect more of the same for at least another month and a half. There is still some good fishing to be had, I would concentrate on some of the tailwaters and stillwaters. Places like the Conejos, San Juan, and Chama rivers are a fishing pretty decent still. Of course flows can change below dams in a moments notice so I advise calling the shop or coming by before you head out.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Pecos River (285 cfs) Mora River (145 cfs)&lt;/p&gt;\r\n&lt;p&gt;The Pecos is high. Use extreme caution when wading. Fishing is a little tough right now on the Pecos. If I was going out to fish it I would concentrate my efforts on the side using a dry dropper set up. Fishing will be best above the confluence with the Mora river.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Conejos River (60 cfs below platoro)&lt;/p&gt;\r\n&lt;p&gt;The Conejos has been fishing really good lately. Flows have come up quite a bit on the lower sections because the tributaries have started their runoff cycle. I would fish the Pinnacles on up. Salvations and worm patterns have been the ticket up there.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Brazos river&lt;/p&gt;\r\n&lt;p&gt;Water is high and rippin&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Rio Chama (216 cfs below Abiquiu, (95 cfs below El Vado), (1000 cfs near La Puente)&lt;/p&gt;\r\n&lt;p&gt;The freestone section is in early runoff. Concentrate your efforts to fishing below the tailwater sections. Streamer fishing has been doing well recently below El Vado and Crane flies, Stonflies and worms below Abiquiu. Flows are still good under the Q, but the Abiquiu section is getting ready to blow out any day now so check the flows before you go!&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Rio Grande:607@ Cerro, 1020 @ TJB&lt;/p&gt;\r\n&lt;p&gt;High and rippin!&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;San Juan River 479 cfs&lt;/p&gt;\r\n&lt;p&gt;Flows have remained steady on the Juan. Fishing has been good out there with many rising fish to be caught. Midge fishing above Texas hole and a combination of midges and beatis below T hole. When you see the noses eating on top switch up to a fore and aft dry and a small comparadun. Water is still a little off color so vary your flourocarbon diameter and use brighter flies.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Area Lakes&lt;/p&gt;\r\n&lt;p&gt;My best bets would be to try Monastary and Santa Cruz lakes in the Santa Fe Area. Smaller streamers and stones for your subsurface patterns, Beatis and caddis for your dries!&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Stop by the shop on your way up north for anything you may need to make a memorible day on the water.We are open at 9am. We have some great new flies and current daily fishing reports.&lt;/p&gt;', 'Fishing reports', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe, new mexico fishing reports, fishing reports santa fe, pecos fishing reports, jemez fishing reports', 'Fishing reports', NULL, '', '', 'fish', ''),
(4, 'Flyshop', '&lt;p&gt;The Reel Life offers the industry&#039;s best products, with a great selection for any budget. &amp;nbsp;We are Santa Fe&#039;s only Orvis dealer. &amp;nbsp;We have everything you need for your next fly fishing adventure and we also carry a extensive line of outdoor clothing great for hiking, boating, hunting, and of course fishing. &amp;nbsp;We also carry an extensive line of fly tying materials and tools. &amp;nbsp;We will also be hosting regular fly tying and fishing clinics hosted at the Fly Shop- free!&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Orvis &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;Termple Fork Outfitters &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Redington&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Patagonia &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Galvan &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;Fishpond&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Yeti Coolers &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;Winston &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Ouray&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Smith Optics &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; USGS Maps &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp; Umpqua&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Coocons &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Solitude &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp; &amp;nbsp; Measure Nets&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Rep Your Water &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Columbia &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; Trout Hunter&lt;/span&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;span&gt;Scientific Anglers &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp; &amp;nbsp;Tiemco &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp; &amp;nbsp; &amp;nbsp;Loon&lt;/span&gt;&lt;/p&gt;', 'Flyshop', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'Flyshop', 'cms-4-.jpg', '', '', '', ''),
(8, 'Guided Trips', '&lt;p&gt;The Reel Life provides professional guide services on hundreds of miles of public water and on several private fisheries to which we hold exclusive fishing rights. Whether you want to fish a small mountain creek or lake, a larger stream for larger fish, or simply a private stretch of water without competition from other anglers, The Reel Life will deliver the angling experience you most desire.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;The goal of The Reel Life&#039;s guiding program is for our clients to come away from their trips as improved anglers. Our talented guides possess an intimate knowledge of every watershed in northern New Mexico. They know the seasons and the hatches. They know where the big fish live and the techniques required to catch them. Plus, all guides are hired based on their ability to teach, to clearly impart their knowledge in a personable and low-pressure fashion to novices and experts alike. Each of our guides is first aid and CPR certified.&lt;/p&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;p&gt;Clients will need a valid New Mexico state fishing license and some personal items for their own comfort. We recommend breathable, non-cotton clothing (including socks) for maximum comfort in our often unpredictable climate. Raingear, sunscreen, a long-sleeved fishing shirt and polarized sunglasses will go a long way towards making your guided trip a comfortable one.&lt;/p&gt;', 'Guided Trips', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'Guided Trips', '', '', '', '', ''),
(3, 'Contact', '&lt;p&gt;The Reel Life is now located inside the De Vargas Mall at 526 North Guadalupe.&lt;/p&gt;\r\n&lt;p&gt;email: info@thereellife.com&lt;/p&gt;\r\n&lt;p&gt;505-995-8114&lt;/p&gt;', 'Contact', 'ContacFly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fet', 'Contact', 'cms-3-.jpg', 'contact', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_posts`
--

CREATE TABLE IF NOT EXISTS `email_templates_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '1',
  `my_display_name` varchar(255) NOT NULL,
  `my_display_email` varchar(255) NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `smtp_server` varchar(255) NOT NULL,
  `smtp_port` varchar(15) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_body_fixed` text NOT NULL,
  `email_body` text NOT NULL,
  `sms_body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `email_templates_posts`
--

INSERT INTO `email_templates_posts` (`id`, `active`, `my_display_name`, `my_display_email`, `smtp_user`, `smtp_password`, `smtp_server`, `smtp_port`, `email_subject`, `email_body_fixed`, `email_body`, `sms_body`) VALUES
(1, 1, 'Grand Adventures', 'info@grandadventures.com', '', '', '', '', 'Winter Park Unguided', '<p>Customer name: {customer_name}<br />Customer Phone: {customer_phone}<br />Date: {date}<br />Time: {time}<br />Snowmobile Trip Name: {trip_name}<br />{snowmobiles_start} <br /> &nbsp;&nbsp;&nbsp;&nbsp; {name}: ${snowmobile_cost} ({quantity})<br /> {snowmobiles_end}</p>\r\n<p>{upgrades_start} <br /> &nbsp;&nbsp;&nbsp;&nbsp; {name}: ${upgrade_cost} ({quantity})<br /> {upgrades_end} <br /><br />Total cost:${total_cost}&nbsp;</p>\r\n<p>{body}</p>', '<p dir="ltr">Please <strong>arrive 45 min early</strong> to check in for your tour. We are located in Beaver Village 79303 US HWY 40, Winter Park Colorado.</p>\r\n<p>Helmets and boots will be provided. Suits are available for $6 if needed. Bring gloves, goggles, and a scarf or facemask if haven&rsquo;t already purchased, they are available onsite for purchase.</p>\r\n<p>Let us know up to <strong>48hrs prior</strong> to your booking if there are any changes or cancellations for a full refund less 15% service charge. After 48hrs it is fully chargeable.</p>\r\n<p>Please know that we require an <strong>authorization of $500 per snowmobile</strong>&nbsp;on a major credit card (no amex or debit), at check-in, for a damage deposit.</p>\r\n<p>In order to operate a snowmobile, customers must have a valid driver&rsquo;s license with their picture on it and be at least <strong>16 years old</strong>. Minors (16-17) must be accompanied by their parent otherwise a waiver must be sent before the tour date, signed by the guardian and notarized.</p>\r\n<p dir="ltr">**PLEASE NOTE NO AMEX/DEBIT CARD ACCEPTED FOR DAMAGE DEPOSIT!</p>\r\n<p dir="ltr">**PLEASE NOTE NO AMEX ACCEPTED-THANK YOU!</p>\r\n<p>Thanks, we look forward to seeing you soon!</p>\r\n<div>&nbsp;</div>\r\n<p>&nbsp;</p>', ''),
(2, 1, 'Trailblazer Tours', 'info@grandadventures.com', '', '', '', '', 'Snowmobile Confirmation', '<p>Customer name: {customer_name}</p>\r\n<p>Customer Phone: {customer_phone}</p>\r\n<p>Date: {date}</p>\r\n<p>Time: {time}</p>\r\n<p>Snowmobile Trip Name: {trip_name}</p>\r\n<p>{snowmobiles_start}</p>\r\n<p>{name}: ${snowmobile_cost} ({quantity})</p>\r\n<p>{snowmobiles_end}</p>\r\n<p>{upgrades_start}</p>\r\n<p>{name}: ${upgrade_cost} ({quantity})</p>\r\n<p>{upgrades_end}</p>\r\n<p>Total cost: ${total_cost}</p>\r\n<p>&nbsp;</p>\r\n<p>{body}</p>', '<div>\r\n<div><strong id="internal-source-marker_0.6038617896847427">Please arrive 45 minutes early for your tours to check in. &nbsp;We are located at the Colorado Adventure Park on&nbsp;</strong></div>\r\n<div><strong><a href="https://maps.google.com/maps?hl=en&amp;tab=wl">566 County Road 721 Fraser, Co 80442</a>. Turn at the road with KFC/Taco Bell. &nbsp;Make a right at the fork. You will see us straight ahead on the left hand side.</strong></div>\r\n<div><strong id="internal-source-marker_0.6038617896847427"><br />Helmets and boots will be provided. &nbsp;Suits are available for $6 if needed. &nbsp;Bring gloves, goggles and a scarf or face mask (available to purchase if required). &nbsp;<br /><br />Let us know up to 48hrs prior to your booking if there are any changes or cancellations. &nbsp;You may cancel up to 48 hrs prior to your booking, less a 15% service charge. After 48hrs, your tour is fully chargeable.<br /><br />Please know that we require a credit card imprint (no debit or amex), at check-in, for a damage deposit on guided tours. &nbsp;<br /><br />In order to operate a snowmobile, customers must have a valid driver&rsquo;s license with their picture on it and be at least 16 years old. &nbsp;Minors (16-17) must be accompanied by their parent otherwise a waiver must be sent before the tour date, signed by the guardian and notarized.<br /><br />Thanks, we look forward to seeing you soon!<br /><br /><br />Best wishes,</strong></div>\r\nGrand Adventures</div>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', ''),
(3, 1, 'Grand Adventures', 'info@grandadventures.com', '', '', '', '', 'Snowmobile Confirmation', '<p>Customer name: {customer_name}</p>\r\n<p>Customer Phone: {customer_phone}</p>\r\n<p>Date: {date}</p>\r\n<p>Time: {time}</p>\r\n<p>Snowmobile Trip Name: {trip_name}</p>\r\n<p>{snowmobiles_start}</p>\r\n<p>{name}: ${snowmobile_cost} ({quantity})</p>\r\n<p>{snowmobiles_end}</p>\r\n<p>{upgrades_start}</p>\r\n<p>{name}: ${upgrade_cost} ({quantity})</p>\r\n<p>{upgrades_end}</p>\r\n<p>Total cost: ${total_cost}</p>\r\n<p>&nbsp;</p>\r\n<p>{body}</p>', '<div><strong id="internal-source-marker_0.3029394675977528">Please arrive 45 minutes early for your tours to check in. &nbsp;We are located at&nbsp;<a href="https://www.google.com/maps/preview?q=79303+us+hwy+40+winter+park+co+80482&amp;ie=UTF-8&amp;hq=&amp;hnear=0x87c28db46133c705:0xd8a910affa9254f5,U.S.+40,+Winter+Park,+CO+80482&amp;gl=us&amp;ei=QctCU9ilN8WdyQGVyIGADg&amp;sqi=2&amp;ved=0CCgQ8gEwAA">79303&nbsp;US Hwy 40, Winter Park, Co 80482</a>.<br /><br />Helmets and boots will be provided. &nbsp;Suits are available for $6 if needed. &nbsp;Bring gloves, goggles and a scarf or face mask (available to purchase onsite if required).<br /><br />Let us know up to 48hrs prior to your booking if there are any changes or cancellations. You may cancel up to 48 hrs prior to your booking, less a 15% service charge. &nbsp;After 48hrs, it is fully chargeable. <br /><br />Please know that we require a credit card imprint (no debit or amex), at check-in, for a damage deposit on guided tours. <br /><br />In order to operate a snowmobile, customers must have a valid driver&rsquo;s license with their picture on it and be at least 16 years old. &nbsp;Minors (16-17) must be accompanied by their parent otherwise a waiver must be sent before the tour date, signed by the guardian and notarized.<br /><br />Thanks, we look forward to seeing you soon!<br /><br />Best wishes,</strong></div>\r\n<p>Grand Adventures</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', ''),
(4, 1, 'wolfweb', 'wolweb@test_template.com', '', '', '', '', 'test temlate', '<p style="text-align: left;"><em><strong>This is </strong></em></p>\r\n<p style="text-align: center;"><span style="font-size: xx-large;"><strong>test template </strong></span></p>\r\n<p style="text-align: right;"><span style="text-decoration: underline;">body fixed</span></p>', '<p><sup>this is lower body</sup>asdfasdf</p>', ''),
(5, 1, 'Grand Adventures', 'ahmed@wolfwebdevelopment.com', '', '', '', '', 'Snowmobile Confirmation', '<p>Customer name: {customer_name}</p>\r\n<p>Customer Phone: {customer_phone}</p>\r\n<p>Date: {date}</p>\r\n<p>Time: {time}</p>\r\n<p>Snowmobile Trip Name: {trip_name}</p>\r\n<p>{snowmobiles_start}</p>\r\n<p>{name}: ${snowmobile_cost} ({quantity})</p>\r\n<p>{snowmobiles_end}</p>\r\n<p>{upgrades_start}</p>\r\n<p>{name}: ${upgrade_cost} ({quantity})</p>\r\n<p>{upgrades_end}</p>\r\n<p>Total cost: ${total_cost}</p>\r\n<p>&nbsp;</p>\r\n<p>{body}</p>', '<div><strong id="internal-source-marker_0.525430710054934">Please arrive 45 &nbsp;minutes early for your tours to check in.&nbsp;<br /><br /></strong></div>\r\n<div><strong id="internal-source-marker_0.525430710054934"><a href="https://maps.google.com/maps?client=safari&amp;oe=UTF-8&amp;q=304+WEST+PORTAL+ROAD++GRAND+LAKE,+CO+80447&amp;ie=UTF-8&amp;hq=&amp;hnear=0x8769819a38249ddb:0x28b0c94b1596cfad,304+W+Portal+Rd,+Grand+Lake,+CO+80447&amp;gl=us&amp;ei=7sdgUK2xC-SZyQH3uoDQCw&amp;ved=0CCEQ8gEwAA">304 WEST PORTAL ROAD </a><a href="https://maps.google.com/maps?client=safari&amp;oe=UTF-8&amp;q=304+WEST+PORTAL+ROAD++GRAND+LAKE,+CO+80447&amp;ie=UTF-8&amp;hq=&amp;hnear=0x8769819a38249ddb:0x28b0c94b1596cfad,304+W+Portal+Rd,+Grand+Lake,+CO+80447&amp;gl=us&amp;ei=7sdgUK2xC-SZyQH3uoDQCw&amp;ved=0CCEQ8gEwAA"><br class="kix-line-break" />GRAND LAKE, CO&nbsp;80447</a><br /><br />We are the first business on the right hand side when you come into town, next to Sombrero Stables and across from the Spirit Lake Snowmobile Dealership.<br />Please wear warm, water proof clothing for a more enjoyable ride! &nbsp;Bring gloves, goggles, scarf or face mask and water (available to purchase if required). Helmets, suits and boots will be provided.<br />There is a $5.00 grooming fee for the Grand Lake area that is not included in our prices.<br /><br />Let us know up to 48hrs prior to your booking if there are any changes or cancellations for a full refund less 15% service charge. &nbsp;After 48hrs it is fully chargeable.<br />Please know that we require an authorization of $500 on a major credit card, at check-in, for a damage deposit.<br />In order to operate a snowmobile, customers must have a valid driver&rsquo;s license with their picture on it and be at least 16 years old. &nbsp;Minors (16-17) must be accompanied by their parent otherwise a waiver must be sent before the tour date, signed by the guardian and notarized.<br /><br />**PLEASE NOTE NO AMEX/DEBIT CARD ACCEPTED FOR DAMAGE DEPOSIT!<br />**PLEASE NOTE NO AMEX ACCEPTED-THANK YOU!<br /><br />We look forward to seeing you soon!<br /><br />Best wishes,</strong></div>\r\n<p>Grand Adventures&nbsp;</p>', ''),
(6, 1, 'Grand Adventures', 'ahmed@wolfwebdevelopment.com', '', '', '', '', 'Reservation For Grand Adventures', '<p>Dear {customer_name}</p>\r\n<p><br />Your reservation has ben successfully made for {trip_name} on {date} at {time}.</p>\r\n<p>The total cost is {total_cost} and with the tax being {trip_tax}</p>\r\n<p><br />You have reserved the following:</p>\r\n<p>{snowmobiles_start}</p>\r\n<ul>\r\n<li>&nbsp;&nbsp;&nbsp;&nbsp; {name} , {snowmobile_cost}, {quantity}</li>\r\n</ul>\r\n<p>{snowmobiles_end}</p>\r\n<p>{upgrades_start}</p>\r\n<ul>\r\n<li>&nbsp;&nbsp;&nbsp;&nbsp; {name} , {upgrade_cost}, {quantity}</li>\r\n</ul>\r\n<p>{upgrades_end}</p>\r\n<p>&nbsp;{body}</p>', '<p>Thank you and we hope to see you again.</p>', ''),
(7, 1, 'Grand Adventures', 'reservations@grandadventures.com', '', '', '', '', 'Reservation Confirmation', '<p>Dear {customer_name}</p>\r\n<p>Thank you for your reservation. We look forward to seeing you on {date} for the trip {trip_name}. Please arrive 15 minutes before the starting time. The trip time is {time}.</p>\r\n<p>Following are the details of your reservation:</p>\r\n<p><strong>Snowmobiles</strong></p>\r\n<p>{snowmobiles_start}</p>\r\n<ul>\r\n<li>{name} ({quantity}):&nbsp; ${snowmobile_cost}</li>\r\n</ul>\r\n<p>{snowmobiles_end}</p>\r\n<p><strong>Add ons<br /></strong></p>\r\n<p>{upgrades_start}</p>\r\n<ul>\r\n<li>{name} ({quantity}):&nbsp; ${upgrade_cost}</li>\r\n</ul>\r\n<p>{upgrades_end}</p>\r\n<p>Fee/tax: ${trip_tax}</p>\r\n<p>Total: ${total_cost}</p>\r\n<p>&nbsp;{body}</p>', '<p>Thank you</p>', ''),
(8, 1, 'Grand Adventures', 'reservations@grandadventures.com', '', '', '', '', 'Failed Transaction', '<p>Dear {customer_name}</p>\r\n<p>This is to inform you that there was a transaction failure to process your credit card for the {time} {trip_name} trip, scheduled for {date}. The total cost of the transaction was ${total_cost}.</p>\r\n<p><br />Please contact our offices at (970)726-9247 and we will see if we can help you get booked on a snowmobile trip.</p>\r\n<p><span style="color: #888888;">&nbsp;</span></p>', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `employees_posts`
--

CREATE TABLE IF NOT EXISTS `employees_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `alt1` varchar(25) NOT NULL,
  `url` varchar(250) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `employees_posts`
--

INSERT INTO `employees_posts` (`id`, `title`, `short_description`, `description`, `image`, `alt1`, `url`, `sortby`, `status`, `created`) VALUES
(6, 'Ivan Valdez', '', '<p><span><span class="text-header-brown2"><span><span><span><span><span><span class="text-common"><span><span>Years Fly-fishing:&nbsp; 30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Years Guiding: 9<br /><br />Other Occupations/interests: Hunting, family, school, and The Broncos.<br /><br />Favorite Waters: Rio Chama for its diversity of big fish, The Conejos for its wild scenery, and the Rio Grande for all of its well kept secrets.<br /><br />Favorite Species: Big native Browns.<br /><br />Most Exotic Trip: The Brazos Box, in my opinion one of the most scenic spots in Northern New Mexico.<br /><br />Likes Most About Guiding: Teaching and sharing my passion of the outdoors and creating an exciting experience for fishermen of all skill levels.<br /><br />Likes Most About Fly-fishing: Every fly-fishing experience is different and one has the ability to truly live in the moment.<br /><br /><br /></span></span></span></span></span></span></span></span></span></span><span><span class="text-header-brown2"><span><span><span><span><span><span class="text-common"><span><span><span><span><span><span><span><span class="text-header-orange2"><span>Ivan''s vast experience and knowledge of the streams in Northern New Mexico makes him not only a great teacher, but a client favorite.&nbsp;</span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></span></p>', 'employees-6-ivan-valdez.jpg', 'guided person', '', 1, 1, '0000-00-00 00:00:00'),
(8, 'Peter Mosey', '', '<p><span>Years Fly Fishing: 8&nbsp; &nbsp; Years Guiding: 7</span><br /><br /><span>Other Occupation/Interests: Organic gardener/farmer, Fly tyier</span><br /><br /><span>Favorite Water: Every Water has thier own strengths. I enjoy solving the challenges that each river poses. Fighting a large Chama brown is just as rewarding as delicately landing a dry fly to a spooky&nbsp; wild trout sipping emergers.&nbsp;</span><br /><br /><span>Favorite Species: Browns. But, being a trout bum I enjoy all species.</span><br /><br /><span>Most Exotic Trip: Still to be determined</span><br /><br /><span>Likes Most About Guiding: I enjoy teaching people. There is nothing more rewarding than seeing the seed of passion being planted. For me, Fly fishing is so much more than catching a trout. It is a lifestyle, A philosophy.&nbsp;</span><br /><br /><span>Likes Most About Fly Fishing: I enjoy the solitude and problem solving. We all live such busy lives. Walking up a desolate river can replenish some of the things that our day to day lives strips away.&nbsp; Every day is different. Every river is different. I enjoy solving those problems. Whether it is using the time tested techniques of our fore fathers or the cutting edge techniques and materials of today. Every problem has a solution. All it normally takes is the will to listen and desire to learn.</span></p>', 'employees-8-peter-mosey.jpg', '', '', 2, 1, '0000-00-00 00:00:00'),
(9, 'Nick Streit', '', '<p>Nick Streit owns Taos Fly Shop and The Reel Life has been guiding since he was a teenager. When he was 17 he was on the Junior US Fly Fishing team that placed 2nd in Europe.&nbsp; Nick has guided and fished all over the world, cutting his teeth on rivers from Alaska to Argentina, not to mention the salt flats of the Caribbean.&nbsp; He continues to guide his home waters of Northern New Mexico and teach fly fishing through courses at the University of NM.&nbsp; When not fishing or running his shop, Nick is usually wandering around the woods trying to shoot critters, either with his bow or his camera.</p>', 'employees-9-nick-streit.jpg', '', '', 2, 1, '0000-00-00 00:00:00'),
(12, 'Carlos Montoya', '', '<p>Years Fly Fishing: 31&nbsp; &nbsp; Years Guiding: 9 years with the reel life and 3 years privately<br /> <br /> Other Occupation/Interests: Currently in school for Neuro-psychology, EMT, father, husband, artist, conservationist, work with wounded veterans and bionic warriors, and a TU member since 2005<br /> <br /> Favorite Water: Rio Costilla. I love small intimate waters with the potential of catching an unsuspecting wild cutthroat, brown, brook or rainbow trout on a dry dropper set up. I also like the challenge of fishing tail waters using size 24 to 32 midge patterns on light tippet and any tail water in Colorado, my favorite hangout, Eleven Mile Canyon. <br /> <br /> Favorite Species: Cutthroats are my favorite species to catch, but the beauty of any other trout species that pleasantly graces me with its presence is well accepted and appreciated. It&rsquo;s the thrill of the hunt and being where you are at that moment and time. Blending with your surroundings and becoming one with the ecosystem, that puts a smile on my face and clarity in my soul.<br /> <br /> Most Exotic Trip: To date would have to be the Henry&rsquo;s fork in Idaho, the Provo River in Utah and the Lower Snake River in Pocatello, Idaho. &nbsp;Catching big fish with the most delicate of presentation on the ranch of the Henry&rsquo;s fork or pulling large streamers on the Snake River.<br /> <br /> Like Most About Guiding: I enjoy teaching people and passing on my passion about the sport, as well as educating people about the ecosystem they are visiting. There is nothing more rewarding than seeing someone catch their first fish on a fly with the excitement of a child on Christmas day, Priceless. Not only do I also enjoy the chance to pass on my knowledge, but I also enjoy learning something new from all my clients; whether it&rsquo;s how to adapt to their needs, how to be successful, or just listening to stories of fishing on other rivers. There is always something to learn and many other ways to do something and get the same results. Guiding for me has become just as much of a passion if not more than me fishing by myself. I am an ambassador of the rivers, lakes and streams.&nbsp;<br /> <br /> Likes Most About Fly Fishing: The serenity, peace, solitude and the spiritual closeness to nature. Fly fishing has always been my escape from the everyday rat race. It&rsquo;s how I find God and where my church is. It&rsquo;s been therapy and a blanket of comfort, and has produced a number of answers to my everyday problems by listening to the flow of the water, the sway of the trees in the wind, the pounding of the rain and the gentle sizzle of snowflakes as they fall on the water. It has always been a passion of respect for what purifies my total well-being. Mimicking what nature does as best as we can to fool an unsuspecting trout, then releasing it back. Such as life, we make mistakes and if we are lucky we are given a second chance, sometimes many.</p>\r\n<p>&nbsp;</p>', 'employees-12-carlos-montoya.jpg', '', '', 4, 1, '0000-00-00 00:00:00'),
(13, 'Chris Vigil', '', '<p>Years Fly Fishing:&nbsp; 35<br /> Years guiding:&nbsp; 3<br /> &nbsp;<br /> Other Occupation/Interests:&nbsp; Family, Fly Tying, Snow Skiing, Golf, Information Technology, Mathematics<br /> &nbsp;<br /> Favorite Water:&nbsp; For private ranches I love Cow Creek Ranch for its quality fish and well manicured grounds.&nbsp; Corkins Lodge is also a treat.&nbsp; <br /> On public waters I love the Jemez area streams during the stonefly hatch, the Rio Chama when the flows are good&nbsp;and of the San Juan for its catch and release waters.<br /> &nbsp;<br /> Favorite species:&nbsp; Rio Grande cutthroat trout.&nbsp; You don''t catch too many of these guys but they are a treat to see.<br /> &nbsp;<br /> Most Exotic Trip:&nbsp; Floating on&nbsp;a pontoon boat in a high alpine lake looking for big cutthroat trout during a full moon.<br /> &nbsp;<br /> Likes most about guiding:&nbsp; Watching other people fly fish.&nbsp; I have learned a lot about fly fishing by not fishing.<br /> &nbsp;<br /> Likes most about fly fishing:&nbsp; The journey.&nbsp; Fly fishing trips are an adventure and each day has a trophy worthy of a good story.&nbsp; Figuring out what the fish are feeding on is very rewarding once you''ve got it. <br /> &nbsp;<br /> Chris has been fishing the lakes and streams of northern New Mexico since 1976.</p>', 'employees-13-chris-vigil.jpg', '', '', 6, 1, '0000-00-00 00:00:00'),
(17, 'Dan Castellano', '', '<p><span>YRS FLY FISHING- 30 &nbsp; &nbsp; YRS GUIDING- 6 &nbsp; &nbsp; &nbsp; &nbsp;OTHER OCC/INTERESTS- journeyman electrician, fly fishing, fly tying, the outdoors, playing music. &nbsp;FAVORITE WATER- The waters of the Santa fe National forest because I grew up fishing all the lakes, streams, and rivers it has to offer. Next would be the Rio Grande for its diversity of fish. MOST EXOTIC FISHING TRIP- Catching striped bass, weak fish, and blue fish with an awesome guide near Staten Island. &nbsp;FAVORITE THING ABOUT GUIDING- Being able to offer family, friends, and clients the knowledge and skills I have retained to make their fly fishing experience a learning and more enjoyable one. &nbsp;FAVORITE THING ABOUT FLY FISHING- Knowing that i triggered &nbsp;the reaction that fooled that wary fish to feed. The complexity of the actions from the cast to the fight. Being able to catch fish in places that gear anglers only wish they could. FAVORITE SPECIES- My heart and soul will forever be devoted to the pursuit of trout. Although stripping a popper, making it "glug" just right and seeing &nbsp;it disappear in a sudden splash. Or watching that toothy esox stalk your baitfish pattern, holding back, entertained like it was a marionette putting on a show for them until you move that fly like it said something about that mean fishes nice mother.</span></p>', 'employees-17-dan-castellano.jpg', '', '', 8, 1, '0000-00-00 00:00:00'),
(16, 'Gregg Flores', '', '<p>&nbsp;</p>\r\n<p>Gregg&rsquo;s father introduced him to the fabled waters of the Rio Grande, the San Juan, and Santa Fe National Forest at the ripe old age of 3 and so he has the privilege of calling himself a lifelong angler and outdoorsman.&nbsp; Fly fishing to him is pure joy and being able to provide others with a few tools to increase their success and enjoyment of the sport is why he guides.&nbsp; Gregg also has a passion for the classroom and in 2010 he graduated with honors with a Bachelors of Science in Civil Engineering from the University of New Mexico. &nbsp;In 2011 he graduated with honors with a Master of Science in Structural Engineering and Structural Mechanics from the University of Colorado at Boulder.&nbsp; Gregg works as a Mechanical Engineer when not on the water.</p>', 'employees-16-gregg-flores.jpg', '', '', 6, 1, '0000-00-00 00:00:00'),
(18, 'Ricardo Salazar', '', '<p>Favorite waters: Fly Fishing Backountry in Colorados San Juan wilderness is my favorite place to fish because it is so wild and unspoiled. Knowing you are fishing in a place so deep in the backcountry it is relatively untouched by humans is quite a humbling experience&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Favorite Species: There are very few things that can get my adrenaline going like watching a monster northern pike rise from the depths to eat a fly that is the size of the fish we are used to catching but the best feeling in the world is catching a big brown trout. Why? Big brown trout are the most elusive living things in New Mexico almost to the point that they seem non existant. We know that monster browns lurk the depths of our rivers but it remains a mystery on how to catch them but when the conditions are just right and it all comes together there is no better feeling than being able to admire one of these trout as they catch their breathe inside of your net.</p>\r\n<p>&nbsp;</p>\r\n<p>Most Exotic Trip: Hiking 14 miles in search of an untouched meadow stream with big beautiful browns in the backcountry.</p>\r\n<p>&nbsp;</p>\r\n<p>Likes Most About Guiding: Its so incredible to be able to share this passion with other people and to be able to stand back and watch the spark turn into an unrelenting fire of happiness and knowing that I was able to help them find something they will love and enjoy forever.</p>\r\n<p>&nbsp;</p>\r\n<p>Likes Most About Fishing: Fly Fishing is not something I do on the weekends or something I plan to do on my next day off. I am a passionate fly fisherman and I anticipate every free minute I have to make time to allow it to be a part of my life every single day whether it is fishing or tying flies. I can fish when I end a guide trip and I can fish when I leave the fly shop and although its not for as long as I would like I can still do it untill it is to dark to see my fly and when I say last cast ill always be sure to make 10 more.&nbsp;</p>', 'employees-18-ricardo-salazar.jpg', '', '', 4, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `local_water`
--

CREATE TABLE IF NOT EXISTS `local_water` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `fishing_report` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `image2` varchar(250) NOT NULL,
  `image3` varchar(250) NOT NULL,
  `stream_flow` varchar(250) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sortby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `alt1` varchar(200) NOT NULL,
  `alt2` varchar(200) NOT NULL,
  `alt3` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `local_water`
--

INSERT INTO `local_water` (`id`, `title`, `description`, `fishing_report`, `image`, `image2`, `image3`, `stream_flow`, `status`, `sortby`, `created`, `alt1`, `alt2`, `alt3`) VALUES
(1, 'Pecos River', '<p><span>The Pecos River provides consistent dry fly and nymph fishing from the end of snowmelt until early November.&nbsp; It is the perfect place for families with busy vacation schedules; due to its proximity to Santa Fe, good fishing can be had with plenty of time to get back to town for evening activities.&nbsp; For the backpacking angler, the Pecos Wilderness offers endless opportunities to catch streamborn trout from alpine lakes and creeks.&nbsp; Some of these fish won''t see five anglers in a year.</span><br /><span>In the years ahead, the Pecos will be the focus of regional stream rehabilitation efforts.&nbsp; Translation: Great fishing is certain to get even better.</span></p>', '', 'LOCALWATER-1-pecos-river.jpg', '', '', 'http://waterdata.usgs.gov/nm/nwis/uv/?site_no=08378500&PARAmeter_cd=00065,00060', 1, 1, '0000-00-00 00:00:00', 'jisfhisf', '', ''),
(3, 'Jemez area streams', '<p><span>The Jemez area is where you''ll hone your&nbsp;meadow creek stalking skills on creeks such as the San Antonio, Cebolla, de las Vacas and the forks of the Jemez.&nbsp; Farther south towards Albuquerque, you''ll strike the Guadalupe and one of the heaviest giant stonefly hatches in the New Mexico.</span></p>', '', 'LOCALWATER-3-local-water-2.jpg', '', '', 'http://waterdata.usgs.gov/nm/nwis/uv/?site_no=08324000&PARAmeter_cd=00065,00060', 1, 2, '0000-00-00 00:00:00', 'lkjlk', '', ''),
(4, 'Rio Chama', '<p><span>The Chama is possibly our prettiest stream, running from its source in the southern Colorado high country to its junction with the Rio Grande among the red and orange mesas made famous by Georgia O''Keefe.&nbsp; In spite of its relatively small size, there are big fish in the Chama, primarily browns and rainbows.&nbsp; These fish will fall for the well-fished dry fly or nymph, but they can be extremely predatory too, chasing down a streamer or blowing up a mouse pattern.</span></p>', '', 'LOCALWATER-4-rio-chama.jpg', '', '', 'http://waterdata.usgs.gov/nm/nwis/uv/?site_no=08286500&PARAmeter_cd=00065,00060', 1, 3, '0000-00-00 00:00:00', '', '', ''),
(5, 'Rio Grande', '<p><span>The Rio Grande is one of the nation''s best trout streams.&nbsp;&nbsp;Over&nbsp;approximately 70 miles of river, there is always a chance that the trout of a lifetime will tackle your fly, and that the pike of a lifetime will eat it.&nbsp; We''ve seen browns and cuttbows over ten pounds in the Rio and plenty of 40 inch northerns.&nbsp; The Red River, Rio Pueblo, and Rio Embudo are some of the Rio''s most productive tribs, and anglers come from everywhere to fish its Mother''s Day caddis hatch.&nbsp; It can be a tough river to fish; nevertheless, few anglers leave disappointed.</span></p>', '', 'LOCALWATER-5-rio-grande.jpg', '', '', 'http://waterdata.usgs.gov/nm/nwis/uv/?site_no=08263500&PARAmeter_cd=00065,00060', 1, 4, '0000-00-00 00:00:00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `manager_admin`
--

CREATE TABLE IF NOT EXISTS `manager_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE latin1_german2_ci DEFAULT NULL,
  `password` varchar(100) COLLATE latin1_german2_ci DEFAULT NULL,
  `secret_ans` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `secret_ans2` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `secret_ans3` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `totalans` int(11) NOT NULL,
  `blocked` datetime NOT NULL,
  `blocked_ip` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `datetime_block` datetime NOT NULL,
  `admin_firstname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `admin_lastname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `admin_email` varchar(150) COLLATE latin1_german2_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `site_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `paypal_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `site_url` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `redirect_url_1` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `redirect_url_2` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `sales_tax_rate` float NOT NULL,
  `smtp_user` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_password` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `status` int(1) NOT NULL,
  `smtp_path` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_port` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `database_backup_frequency` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `title_meta` varchar(200) CHARACTER SET utf8 NOT NULL,
  `keywords_meta` text CHARACTER SET utf8 NOT NULL,
  `description_meta` text CHARACTER SET utf8 NOT NULL,
  `heading` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `content` text COLLATE latin1_german2_ci NOT NULL,
  `image` text COLLATE latin1_german2_ci NOT NULL,
  `alt` varchar(200) COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `manager_admin`
--

INSERT INTO `manager_admin` (`id`, `username`, `password`, `secret_ans`, `secret_ans2`, `secret_ans3`, `totalans`, `blocked`, `blocked_ip`, `datetime_block`, `admin_firstname`, `admin_lastname`, `admin_email`, `site_name`, `site_email`, `paypal_email`, `site_url`, `redirect_url_1`, `redirect_url_2`, `sales_tax_rate`, `smtp_user`, `smtp_password`, `status`, `smtp_path`, `smtp_port`, `database_backup_frequency`, `title_meta`, `keywords_meta`, `description_meta`, `heading`, `content`, `image`, `alt`) VALUES
(1, 'admin', '9be3047e18b685088cfc2736fbde002e', 'Marks', 'Fiat', 'Stella', 3, '0000-00-00 00:00:00', '189.11.01.2', '2016-05-03 10:20:08', 'Ivan', 'Valdez', 'info@thereellife.com', 'Reel Life', 'info@thereellife.com', 'programmer@wolfwebdevelopment.com', 'http://thereellife.com/', 'http://wolfwebdevelopment.com', 'http://wolfwebdevelopment.com', 0, 'programmer@wolfwebdevelopment.com', 'aaaaaa', 0, 'wolfwebdevelopment.com', '25', 'monthly', 'Welcome to Reel Life', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'We provide a complete experience  for the fly fisherman. From beginners to experts, we have what you need to enjoy all the New Mexico has to offer.', 'Heading Here', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s,', 'admin-1-.png', 'Banner'),
(2, 'Reel Life', 'Brown Trout', 'Marks', 'Fiat', 'Stella', 3, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 'Ivan', 'Valdez', 'zain@wolfwebdevelopment.com', 'Reel Life', 'info@thereellife.com', '', 'http://thereellife.com/', '', 'http://wolfwebdevelopment.com', 0, 'programmer@wolfwebdevelopment.com', '', 0, 'wolfwebdevelopment.com', '25', 'monthly', 'Welcome to Reel Life', 'Fly fishing Santa Fe,Santa Fe fly shop,Pecos fly fishing,Santa Fe fly fishing guide,Fly fishing new Mexico,Rio grande fly fishing,Los Alamos fly shop,New Mexico fly shop,fly fishing pecos river,Santa Fe fly fishing classes,Santa Fe orvis,Fly fishing Chama,Brazos river fly fishing,Santa Fe fishing store,Santa Fe yeti coolers,San Juan river fly fishing,Santa Fe fly fishing,New Mexico fishing guides,Fishing guides Santa Fe,Fly fishing school Santa Fe', 'We provide a complete experience  for the fly fisherman. From beginners to experts, we have what you need to enjoy all the New Mexico has to offer.', 'Heading Here', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s,', 'admin-1-.png', 'Banner');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `alt1` varchar(100) NOT NULL,
  `created` date NOT NULL,
  `sortorder` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `image`, `alt1`, `created`, `sortorder`) VALUES
(2, 'The Reel Life moves and expands store!', '&lt;p&gt;We&#039;ve moved! &amp;nbsp;Come see the new shop at 526 North Guadalupe (inside the De Vargas Mall). &amp;nbsp;&lt;/p&gt;', '', '', '2014-04-13', 5),
(9, 'Fly tying classes', '&lt;p&gt;Free fly tying classes 6-8pm Thursdays. Learn the art or improve your skills. Limited vices and tools available.&amp;nbsp;&lt;/p&gt;', '', '', '2014-06-26', 4),
(14, 'Reel Life becomes Simms Dealer!', '&lt;p&gt;Once again The Reel Life is happy to offer its customers unbeatable boots, waders, aparrell and accesories from Simms.&lt;/p&gt;', '', '', '2015-11-10', 2),
(15, 'Now guiding the San Juan', '&lt;p&gt;The Reel Life is now offering full day wade and float trips on the world famous San Juan river! &amp;nbsp;This tailwater fishes great in the fall and winter, so if your looking to get some big fish in the net this season, give us a call.&lt;/p&gt;', '', '', '2015-11-10', 2),
(17, 'The Reel Life HOURS:', '&lt;p&gt;The fly shop will be open 9am til 6pm Monday thru Saturday and 9am til 5pm Sunday!&lt;/p&gt;', '', '', '2016-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `picture_gallery`
--

CREATE TABLE IF NOT EXISTS `picture_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `page_link` text NOT NULL,
  `file` varchar(250) NOT NULL,
  `page` varchar(10) NOT NULL,
  `date` varchar(100) NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `picture_gallery`
--

INSERT INTO `picture_gallery` (`id`, `name`, `page_link`, `file`, `page`, `date`, `sortby`) VALUES
(39, 'Dry Fly Fishing', 'http://taosflyshop.com/thereellife/localwater.php', '1398280756IMG_0432.jpg', '', '', 0),
(44, 'Rio Grande near Pilar', '', '1398280510-2.jpg', '', '', 0),
(41, 'Small Stream Fishing', '', '1398281379-4734.jpg', '', '', 0),
(37, 'Winter Fishing', '', '139828071320131105-IMG_7850.jpg', '', '', 1),
(43, 'Brookie', '', '1398280341-5096.jpg', '', '', 0),
(45, 'Brazos at Corkins', '', '1398280510-5138.jpg', '', '', 7),
(46, 'San Juan', '', '1398280510-7148.jpg', '', '', 8),
(47, 'Brazos River Ranch', '', '1398280510-051.jpg', '', '', 0),
(48, 'Lunker!', '', '1398280510-1646.jpg', '', '', 10),
(49, 'Pike on the Fly!', '', '139828051020120514--0569.jpg', '', '', 0),
(50, 'Fall Colors', '', '1398281348-6332.jpg', '', '', 0),
(51, 'Big Brown', '', '139828134820110502--01-3.jpg', '', '', 0),
(52, 'Rainbow', '', '1398281348P3180639-2.jpg', '', '', 0),
(53, 'San Juan Rainbow', '', '1398281348-7327.jpg', '', '', 0),
(54, 'Muley on the Chama', '', '139828134820131106-IMG_7984.jpg', '', '', 0),
(55, 'Last Cast', '', '1398281348-7184.jpg', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sample_module_posts`
--

CREATE TABLE IF NOT EXISTS `sample_module_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `max_patients` int(11) NOT NULL,
  `price` float NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_attendees_posts`
--

CREATE TABLE IF NOT EXISTS `site_attendees_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `dates` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `number_of_people` int(11) NOT NULL,
  `sub_total` float NOT NULL,
  `sales_tax` float NOT NULL,
  `total_price` float NOT NULL,
  `amount_deposited` float NOT NULL,
  `status` int(11) NOT NULL,
  `customer_vault_id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_classes_posts`
--

CREATE TABLE IF NOT EXISTS `site_classes_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `dates` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_short` text NOT NULL,
  `price` float NOT NULL,
  `status` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `free_class` int(1) NOT NULL,
  `sortby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `site_classes_posts`
--

INSERT INTO `site_classes_posts` (`id`, `title`, `dates`, `description`, `description_short`, `price`, `status`, `image`, `free_class`, `sortby`, `created`) VALUES
(19, 'Fly Tying', 'Every Thursday', '&lt;p&gt;Every Thursday night from 6pm to 8pm, join us for a free fly tying class. Come and meet new friends, learn new patterns and expand your skill set. Experienced and novice tyers are welcomed. Bring your vise, materials and tools, limited vices and tools are available for those who are curious in trying out this new art.&lt;/p&gt;', 'Every Thursday from 6pm to 8pm.', 0, 1, '', 0, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sys_admin`
--

CREATE TABLE IF NOT EXISTS `sys_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_email` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(255) DEFAULT NULL,
  `admin_firstname` varchar(25) DEFAULT NULL,
  `admin_lastname` varchar(25) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `blocked` datetime NOT NULL,
  `blocked_ip` varchar(100) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `redirect_url_1` varchar(255) NOT NULL,
  `redirect_url_2` varchar(255) NOT NULL,
  `sales_tax_rate` float NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `smtp_path` varchar(255) NOT NULL,
  `smtp_port` varchar(255) NOT NULL,
  `database_backup_frequency` varchar(50) NOT NULL,
  `gateway_mode` varchar(10) NOT NULL,
  `title_meta` text NOT NULL,
  `keywords_meta` text NOT NULL,
  `description_meta` text NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(25) DEFAULT NULL,
  `state` varchar(25) DEFAULT NULL,
  `country` varchar(25) DEFAULT NULL,
  `postcode` varchar(25) DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `registration_date` date NOT NULL,
  `admin_type` int(12) NOT NULL COMMENT '1=super_admin, 2=minor_admin',
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `sys_admin`
--

INSERT INTO `sys_admin` (`admin_id`, `admin_email`, `username`, `admin_firstname`, `admin_lastname`, `password`, `blocked`, `blocked_ip`, `site_name`, `site_email`, `site_url`, `redirect_url_1`, `redirect_url_2`, `sales_tax_rate`, `smtp_user`, `smtp_password`, `smtp_path`, `smtp_port`, `database_backup_frequency`, `gateway_mode`, `title_meta`, `keywords_meta`, `description_meta`, `phone`, `address1`, `address2`, `city`, `state`, `country`, `postcode`, `status`, `company`, `registration_date`, `admin_type`) VALUES
(55, 'marc@rezosystems.com', NULL, 'Marc', 'Harrell', 'marc', '0000-00-00 00:00:00', '189.11.01.2', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 1),
(59, 'emilyroley@gmail.com', NULL, 'Emily', 'Roley', 'Brown Trout', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 1),
(60, 'ivanjvaldez@yahoo.com', NULL, 'Ivan', 'Valdez', '1990', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 1),
(61, 'moseysmosca@gmail.com', NULL, 'Peter', 'Mosey', '1234', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 1),
(62, 'nik22010@live.com', NULL, 'Nik', 'Adler', '5675', '0000-00-00 00:00:00', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE IF NOT EXISTS `travel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `image_2` varchar(250) NOT NULL,
  `image_3` varchar(250) NOT NULL,
  `image_4` text CHARACTER SET utf8 NOT NULL,
  `image_5` text CHARACTER SET utf8 NOT NULL,
  `sortby` int(11) NOT NULL,
  `image_6` text CHARACTER SET utf8 NOT NULL,
  `alt1` varchar(255) NOT NULL,
  `alt2` varchar(255) NOT NULL,
  `alt3` varchar(255) NOT NULL,
  `alt4` varchar(255) NOT NULL,
  `alt5` varchar(255) NOT NULL,
  `alt6` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id`, `title`, `description`, `image`, `image_2`, `image_3`, `image_4`, `image_5`, `sortby`, `image_6`, `alt1`, `alt2`, `alt3`, `alt4`, `alt5`, `alt6`) VALUES
(15, 'Argentina', '<p>Every winter The Reel Life Crew heads south to the wilds of Patagonia to fish for big, wild trout in the streams, river and lakes that drain the Andes. We are not affiliated with any particular lodge, but rather customize our trips for our clients. With decades of expirience in Argentina, we can tailor the trip to our clients specific desires for accomidations, types of fishing (dry fly, streamer, wading, floating, etc.) Email <a title="nick@taosflyshop.com" href="mailto:nick@taosflyshop.com">nick@thereellife.com</a> for more information.</p>\r\n<p>&nbsp;</p>', 'travel-15-argentina.jpg', 'travel--15-argentina.jpg', 'travel---15-argentina.jpg', 'travel----15-argentina.jpg', '', 1, 'travel------15-argentina.jpg', 'image 1', 'image 2', 'image 2', 'image 2', 'image 2', 'image 2'),
(17, 'Alaska', '<p>We look forward to another grand adventure to Alaska this summer. Join us July 7 - 14, 2016 for another unforgettable excursion to Dave and Kim Egdorf''s Camp on the Nushagak River, in the heart of Alaska''s Bristol Bay region. We will be fishing light tackle to large rainbows, dolly varden, grayling as they seek their sustenance from one of the largest runs of salmon known to humankind. This is a true Alaska wilderness experience, replete with wildlife, spectacular views, professional and friendly service from the camp''s highly-skilled staff and, of course, stupendous fishing. Email<a title="toner@thereellife.com" href="mailto:toner@thereellife.com"> toner@thereellife.com</a> for more information.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Depending on the time of year, your fish will be vulnerable to a well-fished sculpin, mouse, egg, or flesh pattern. Compared to what we encounter in the lower 48, an Alaska-grown rainbow is unique in its beauty and power; we fish 6 weight rods, mainly because these fish can easily break anything lighter.</p>\r\n<p>&nbsp;</p>\r\n<p>Price: $5,195.00 (a bargain for Alaska). Includes all meals, 6 days guided fishing, and an experience you couldn''t forget if you tried.</p>', 'LOCALWATER-17-alaska.jpg', 'travel--17-alaska.jpg', 'travel---17-alaska.jpg', 'travel----17-alaska.jpg', 'travel-----17-alaska.jpg', 2, 'travel------17-alaska.jpg', '', '', '', '', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
