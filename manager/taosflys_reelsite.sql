-- phpMyAdmin SQL Dump
-- version 4.1.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 14, 2014 at 12:41 AM
-- Server version: 5.5.36-cll
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `taosflys_reelsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `alt1` varchar(100) NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `description`, `image`, `alt1`, `sortby`) VALUES
(7, 'Banner 1', '<p>Banner 1</p>', 'employees-7-banner1.jpg', 'Banner 1', 1),
(8, 'Banner 2', '<p>Banner 2</p>', 'employees-8-banner-2.jpg', 'Banner 2', 2),
(9, 'Banner 3', '<p>Banner 3</p>', 'employees-9-banner-3.jpg', 'Banner 3', 3);

-- --------------------------------------------------------

--
-- Table structure for table `carriers_posts`
--

CREATE TABLE IF NOT EXISTS `carriers_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `carriers_posts`
--

INSERT INTO `carriers_posts` (`id`, `title`, `domain`, `sortby`) VALUES
(1, '------ USA ------', '0', 1),
(2, '7-11 Speakout', '@cingularme.com', 2),
(3, 'Alltel', '@message.alltel.com', 3),
(4, 'Ameritech', '@paging.acswireless.com', 4),
(5, 'AT&T', '@txt.att.net', 5),
(6, 'Bellsouth', '@sms.bellsouth.com', 6),
(7, 'Boost', '@myboostmobile.com', 0),
(8, 'CellularOne', '@mobile.celloneusa.com', 0),
(9, 'Cellular South', '@csouth1.com', 0),
(10, 'Centennial', '@cwemail.com', 0),
(11, 'Cincinnati Bell', '@gocbw.com', 0),
(12, 'Cingular', '@mobile.mycingular.com', 0),
(13, 'Cricket', '@mycricket.com', 0),
(14, 'Edge Wireless', '@sms.edgewireless.com', 0),
(15, 'Metro PCS', '@mymetropcs.com', 0),
(16, 'Nextel', '@messaging.nextel.com', 0),
(17, 'O2', '@mobile.celloneusa.com', 0),
(18, 'Orange', '@mobile.celloneusa.com', 0),
(19, 'Quest', '@qwestmp.com', 0),
(20, 'Rogers Wireless', '@pcs.rogers.com', 0),
(21, 'Sprint', '@messaging.sprintpcs.com', 0),
(22, 'T-Mobile USA', '@tmomail.com', 0),
(23, 'Teleflip', '@teleflip.com', 0),
(24, 'Telus Mobility', '@msg.telus.com', 0),
(25, 'US Cellular', '@email.uscc.net', 0),
(26, 'Verizon Wireless', '@vtext.com', 0),
(27, 'Virgin Mobile', '@vmobl.com', 0),
(28, '------ CANADA ------', '0', 0),
(29, 'Aliant', '@wirefree.informe.ca', 0),
(30, 'Bell Mobility', '@txt.bellmobility.ca', 0),
(31, 'Fido', '@fido.ca', 0),
(32, 'Koodo Mobile', '@msg.koodomobile.com', 0),
(33, 'MTS Mobility', '@text.mtsmobility.com', 0),
(34, 'Presidents Choice', '@mobiletxt.ca', 0),
(35, 'Rogers Wireless', '@pcs.rogers.com', 0),
(36, 'Sasktel Mobility', '@pcs.sasktelmobility.com', 0),
(37, 'Telus', '@msg.telus.com', 0),
(38, 'Virgin Mobile', '@vmobile.ca', 0),
(39, '------ United Kingdom ------', '0', 0),
(40, 'Orange', '@orange.net', 0),
(41, 'T-Mobile', '@t-mobile.uk.net', 0),
(42, 'Virgin Mobile', '@vxtras.com', 0),
(43, 'Vodafone', '@vodafone.net', 0),
(44, '------ Germany ------', '0', 0),
(45, 'T-Mobile', '@t-d1-sms.de', 0),
(46, 'Vodafone', '@vodafone-sms.de', 0),
(47, 'O2', '@o2online.de', 0),
(48, 'E-Plus', '@smsmail.eplus.de', 0),
(49, '------ Australia ------', '0', 0),
(50, 'T-Mobile/Optus Zoo', '@optusmobile.com.au', 0),
(51, 'SL Interactive', '@slinteractive.com.au', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_posts`
--

CREATE TABLE IF NOT EXISTS `cms_posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `contents` text,
  `title_meta` text NOT NULL,
  `keywords_meta` text NOT NULL,
  `description_meta` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `cms_posts`
--

INSERT INTO `cms_posts` (`id`, `title`, `contents`, `title_meta`, `keywords_meta`, `description_meta`, `image`, `image_2`, `image_3`) VALUES
(1, 'Home', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#039;s standard &lt;br /&gt; dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen &lt;br /&gt; book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially &lt;br /&gt; unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, &lt;br /&gt; and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. &lt;br /&gt; Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the &lt;br /&gt; industry&#039;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type &lt;br /&gt; and scrambled it to make a type specimen book. It has survived not only five centuries, but also the &lt;br /&gt; leap into electronic typesetting,&lt;/p&gt;', 'Home Page', 'Home Page', 'Home Page', NULL, '', ''),
(9, 'Guided Profile', NULL, 'Guided Profile', 'Guided Profile', 'Guided Profile', NULL, '', ''),
(10, 'Photo Gallery', NULL, 'Photo Gallery', 'Photo Gallery', 'Photo Gallery', NULL, '', ''),
(11, 'Local Water', NULL, 'Local Water', 'Local Water', 'Local Water', NULL, '', ''),
(4, 'Flyshop', '&lt;p&gt;Flyshop&lt;/p&gt;', 'Flyshop', 'Flyshop', 'Flyshop', NULL, '', ''),
(8, 'Guided Trips', '&lt;p&gt;Guided Trips&lt;/p&gt;', 'Guided Trips', 'Guided Trips', 'Guided Trips', NULL, '', ''),
(3, 'Contact', '&lt;p&gt;Contact&lt;/p&gt;', 'Contact', 'Contact', 'Contact', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_posts`
--

CREATE TABLE IF NOT EXISTS `email_templates_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '1',
  `my_display_name` varchar(255) NOT NULL,
  `my_display_email` varchar(255) NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `smtp_server` varchar(255) NOT NULL,
  `smtp_port` varchar(15) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_body_fixed` text NOT NULL,
  `email_body` text NOT NULL,
  `sms_body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employees_posts`
--

CREATE TABLE IF NOT EXISTS `employees_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `alt1` varchar(25) NOT NULL,
  `url` varchar(250) NOT NULL,
  `sortby` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `employees_posts`
--

INSERT INTO `employees_posts` (`id`, `title`, `short_description`, `description`, `image`, `alt1`, `url`, `sortby`, `status`, `created`) VALUES
(6, 'Guided Profile', '', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard&nbsp;<br />dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen&nbsp;<br />book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially&nbsp;<br />unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,&nbsp;<br />and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.&nbsp;<br />Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>', 'employees-6-guidedprofile.jpg', 'guided person', '', 1, 1, '0000-00-00 00:00:00'),
(8, 'Guided Profile 2', '', '<p>Guided Profile Description</p>', 'employees-8-guided-profile-2.jpg', 'jkbkjbnkj', '', 2, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `local_water`
--

CREATE TABLE IF NOT EXISTS `local_water` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `fishing_report` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `image2` varchar(250) NOT NULL,
  `image3` varchar(250) NOT NULL,
  `stream_flow` varchar(250) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sortby` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `alt1` varchar(200) NOT NULL,
  `alt2` varchar(200) NOT NULL,
  `alt3` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `local_water`
--

INSERT INTO `local_water` (`id`, `title`, `description`, `fishing_report`, `image`, `image2`, `image3`, `stream_flow`, `status`, `sortby`, `created`, `alt1`, `alt2`, `alt3`) VALUES
(1, 'Local Water', '<p>LOcal Water description</p>', '', 'LOCALWATER-1-local-water.jpg', '', '', 'http://taosflyshop.com/thereellife/', 1, 1, '0000-00-00 00:00:00', 'jisfhisf', '', ''),
(3, 'Local Water 2', '<p>Local Water Dewsclkjnk</p>', '', 'LOCALWATER-3-local-water-2.jpg', '', '', 'http://taosflyshop.com/thereellife', 1, 2, '0000-00-00 00:00:00', 'lkjlk', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `manager_admin`
--

CREATE TABLE IF NOT EXISTS `manager_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE latin1_german2_ci DEFAULT NULL,
  `password` varchar(30) COLLATE latin1_german2_ci DEFAULT NULL,
  `secret_ans` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `secret_ans2` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `secret_ans3` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `totalans` int(11) NOT NULL,
  `blocked` datetime NOT NULL,
  `admin_firstname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `admin_lastname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `admin_email` varchar(150) COLLATE latin1_german2_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `site_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `paypal_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `site_url` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `redirect_url_1` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `redirect_url_2` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `sales_tax_rate` float NOT NULL,
  `smtp_user` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_password` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_path` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_port` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `database_backup_frequency` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `title_meta` varchar(200) CHARACTER SET utf8 NOT NULL,
  `keywords_meta` text CHARACTER SET utf8 NOT NULL,
  `description_meta` text CHARACTER SET utf8 NOT NULL,
  `heading` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `content` text COLLATE latin1_german2_ci NOT NULL,
  `image` text COLLATE latin1_german2_ci NOT NULL,
  `alt` varchar(200) COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `manager_admin`
--

INSERT INTO `manager_admin` (`id`, `username`, `password`, `secret_ans`, `secret_ans2`, `secret_ans3`, `totalans`, `blocked`, `admin_firstname`, `admin_lastname`, `admin_email`, `site_name`, `site_email`, `paypal_email`, `site_url`, `redirect_url_1`, `redirect_url_2`, `sales_tax_rate`, `smtp_user`, `smtp_password`, `smtp_path`, `smtp_port`, `database_backup_frequency`, `title_meta`, `keywords_meta`, `description_meta`, `heading`, `content`, `image`, `alt`) VALUES
(1, 'admin', 'admin', 'Marks', 'Fiat', 'Stella', 3, '0000-00-00 00:00:00', 'Marc', 'Harrell', 'zain@wolfwebdevelopment.com', 'Reel Life', 'zain@wolfwebdevelopment.com', 'programmer@wolfwebdevelopment.com', 'http://taosflyshop.com/thereellife/', 'http://wolfwebdevelopment.com', 'http://wolfwebdevelopment.com', 0, 'programmer@wolfwebdevelopment.com', 'aaaaaa', 'wolfwebdevelopment.com', '25', 'monthly', 'Welcome to reel life', 'reel life', 'reel life', 'Heading Here', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s,', 'admin-1-.png', 'Banner');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `alt1` varchar(100) NOT NULL,
  `created` date NOT NULL,
  `sortorder` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `image`, `alt1`, `created`, `sortorder`) VALUES
(2, 'News 1', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&lt;/p&gt;', '', '', '2014-04-13', 1),
(3, 'News 2', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&lt;/p&gt;', '', '', '2014-04-13', 2),
(4, 'News 3', '&lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&lt;/p&gt;', '', '', '2014-04-13', 3);

-- --------------------------------------------------------

--
-- Table structure for table `picture_gallery`
--

CREATE TABLE IF NOT EXISTS `picture_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `page_link` text NOT NULL,
  `file` varchar(250) NOT NULL,
  `page` varchar(10) NOT NULL,
  `date` varchar(100) NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `picture_gallery`
--

INSERT INTO `picture_gallery` (`id`, `name`, `page_link`, `file`, `page`, `date`, `sortby`) VALUES
(39, 'pic 3', '', '1397455219fr4.jpg', '', '', 3),
(37, 'pic 1', '', '1397455219fr2.jpg', '', '', 1),
(38, 'pic 2', '', '1397455219fr3.jpg', '', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sample_module_posts`
--

CREATE TABLE IF NOT EXISTS `sample_module_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `max_patients` int(11) NOT NULL,
  `price` float NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
