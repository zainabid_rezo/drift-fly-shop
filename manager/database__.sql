
CREATE TABLE IF NOT EXISTS `carriers_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `carriers_posts`
--

INSERT INTO `carriers_posts` (`id`, `title`, `domain`, `sortby`) VALUES
(1, '------ USA ------', '0', 1),
(2, '7-11 Speakout', '@cingularme.com', 2),
(3, 'Alltel', '@message.alltel.com', 3),
(4, 'Ameritech', '@paging.acswireless.com', 4),
(5, 'AT&T', '@txt.att.net', 5),
(6, 'Bellsouth', '@sms.bellsouth.com', 6),
(7, 'Boost', '@myboostmobile.com', 0),
(8, 'CellularOne', '@mobile.celloneusa.com', 0),
(9, 'Cellular South', '@csouth1.com', 0),
(10, 'Centennial', '@cwemail.com', 0),
(11, 'Cincinnati Bell', '@gocbw.com', 0),
(12, 'Cingular', '@mobile.mycingular.com', 0),
(13, 'Cricket', '@mycricket.com', 0),
(14, 'Edge Wireless', '@sms.edgewireless.com', 0),
(15, 'Metro PCS', '@mymetropcs.com', 0),
(16, 'Nextel', '@messaging.nextel.com', 0),
(17, 'O2', '@mobile.celloneusa.com', 0),
(18, 'Orange', '@mobile.celloneusa.com', 0),
(19, 'Quest', '@qwestmp.com', 0),
(20, 'Rogers Wireless', '@pcs.rogers.com', 0),
(21, 'Sprint', '@messaging.sprintpcs.com', 0),
(22, 'T-Mobile USA', '@tmomail.com', 0),
(23, 'Teleflip', '@teleflip.com', 0),
(24, 'Telus Mobility', '@msg.telus.com', 0),
(25, 'US Cellular', '@email.uscc.net', 0),
(26, 'Verizon Wireless', '@vtext.com', 0),
(27, 'Virgin Mobile', '@vmobl.com', 0),
(28, '------ CANADA ------', '0', 0),
(29, 'Aliant', '@wirefree.informe.ca', 0),
(30, 'Bell Mobility', '@txt.bellmobility.ca', 0),
(31, 'Fido', '@fido.ca', 0),
(32, 'Koodo Mobile', '@msg.koodomobile.com', 0),
(33, 'MTS Mobility', '@text.mtsmobility.com', 0),
(34, 'Presidents Choice', '@mobiletxt.ca', 0),
(35, 'Rogers Wireless', '@pcs.rogers.com', 0),
(36, 'Sasktel Mobility', '@pcs.sasktelmobility.com', 0),
(37, 'Telus', '@msg.telus.com', 0),
(38, 'Virgin Mobile', '@vmobile.ca', 0),
(39, '------ United Kingdom ------', '0', 0),
(40, 'Orange', '@orange.net', 0),
(41, 'T-Mobile', '@t-mobile.uk.net', 0),
(42, 'Virgin Mobile', '@vxtras.com', 0),
(43, 'Vodafone', '@vodafone.net', 0),
(44, '------ Germany ------', '0', 0),
(45, 'T-Mobile', '@t-d1-sms.de', 0),
(46, 'Vodafone', '@vodafone-sms.de', 0),
(47, 'O2', '@o2online.de', 0),
(48, 'E-Plus', '@smsmail.eplus.de', 0),
(49, '------ Australia ------', '0', 0),
(50, 'T-Mobile/Optus Zoo', '@optusmobile.com.au', 0),
(51, 'SL Interactive', '@slinteractive.com.au', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_posts`
--

CREATE TABLE IF NOT EXISTS `cms_posts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `contents` text,
  `title_meta` text NOT NULL,
  `keywords_meta` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) NOT NULL,
  `image_3` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_posts`
--

CREATE TABLE IF NOT EXISTS `email_templates_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '1',
  `my_display_name` varchar(255) NOT NULL,
  `my_display_email` varchar(255) NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_password` varchar(255) NOT NULL,
  `smtp_server` varchar(255) NOT NULL,
  `smtp_port` varchar(15) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_body_fixed` text NOT NULL,
  `email_body` text NOT NULL,
  `sms_body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_admin`
--

CREATE TABLE IF NOT EXISTS `manager_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE latin1_german2_ci DEFAULT NULL,
  `getmein` varchar(30) COLLATE latin1_german2_ci DEFAULT NULL,
  `blocked` datetime NOT NULL,
  `admin_firstname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `admin_lastname` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `admin_email` varchar(150) COLLATE latin1_german2_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `site_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `paypal_email` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `site_url` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `redirect_url_1` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `redirect_url_2` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `sales_tax_rate` float NOT NULL,
  `smtp_user` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_password` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_path` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `smtp_port` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  `database_backup_frequency` varchar(255) COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_german2_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `manager_admin`
--

INSERT INTO `manager_admin` (`id`, `username`, `getmein`, `admin_firstname`, `admin_lastname`, `admin_email`, `site_name`, `site_email`, `paypal_email`, `site_url`, `redirect_url_1`, `redirect_url_2`, `sales_tax_rate`, `smtp_user`, `smtp_password`, `smtp_path`, `smtp_port`, `database_backup_frequency`) VALUES
(1, 'admin', 'admin', 'Marc', 'Harrell', 'programmer@wolfwebdevelopment.com', 'programmer@wolfwebdevelopment.com', 'programmer@wolfwebdevelopment.com', 'programmer@wolfwebdevelopment.com', 'http://www.wolfwebdevelopment.com/', 'http://wolfwebdevelopment.com', 'http://wolfwebdevelopment.com', 0, 'programmer@wolfwebdevelopment.com', 'aaaaaa', 'wolfwebdevelopment.com', '25', 'daily');

-- --------------------------------------------------------

--
-- Table structure for table `sample_module_posts`
--

CREATE TABLE IF NOT EXISTS `sample_module_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `max_patients` int(11) NOT NULL,
  `price` float NOT NULL,
  `sortby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;