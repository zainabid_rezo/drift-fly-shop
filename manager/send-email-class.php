<?php
include_once("phpmailer/class.phpmailer.php");
?>
<?php
function smtpmailerDynamic($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false) {
	/*		$headers = "MIME-Version: 1.0" . "\r\n";
 			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= "From: $from_name <$from>" . "\r\n";
			mail($to,$subject,$body,$headers);
			return true;
	*/
	global $emailwarning;
	global $emailsuccess;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->IsHTML($ishtml);
	$mail->SMTPAuth = true; 
	if ($is_gmail) {
		$mail->SMTPSecure = 'ssl'; 
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 465;  
		$mail->Username = 'wolfwebdevelopment@gmail.com';  
		$mail->Password = 'password';   
	} else {
		$mail->Host = $_SESSION['SMTPSERVER'];
		$mail->Username = $_SESSION['SMTPUSER'];  
		$mail->Password = $_SESSION['SMTPPWD'];
		$mail->Port = $_SESSION['SMTPPORT'];  
	}        
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->ClearAddresses();
	$mail->AddAddress($to);
	if(!$mail->Send()) {
		$emailwarning .= 'Mail error: '.$mail->ErrorInfo;
		return false;
	} else {
		$emailsuccess .= 'Email Message sent!';
		return true;
	}
}

function get_email_template($id){
	$q_email="SELECT * FROM email_templates_posts WHERE id = '$id' AND active = '1'";
	$sql_email= @mysql_query($q_email);
	if(!$sql_email)
	die('<p>Error performing query: ' . mysql_error() .'</p>');
			while($row_email=mysql_fetch_array($sql_email))
			{
		
			$_SESSION['SMTPUSER'] = $row_email['smtp_user']; 
			$_SESSION['SMTPPWD'] = $row_email['smtp_password']; 
			$_SESSION['SMTPSERVER'] = $row_email['smtp_server']; 
			$_SESSION['SMTPPORT'] = $row_email['smtp_port']; 
			
			$_SESSION['EMAIL_FROM'] = $row_email['my_display_email'];
			$_SESSION['EMAIL_FROM_NAME'] = $row_email['my_display_name']; 
			$_SESSION['EMAIL_SUBJECT'] = $row_email['email_subject']; 
			
			$_SESSION['EMAIL_BODY_FIXED'] = $row_email['email_body_fixed']; 
			$_SESSION['EMAIL_BODY'] = html_entity_decode($row_email['email_body']); 
			
			}
		}
?>