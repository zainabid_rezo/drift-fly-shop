<div id="top">
  <!-- Logo -->
  <div class="logo">
    <h1>
      <a href="index.php" style="color:#FFFFFF; text-decoration:none"><?=SITE_NAME?></a>
    </h1>
  </div>
  <!-- End of Logo -->
  <!-- Meta information -->
  <div class="meta">
    <p>Welcome,
      <?=ADMIN_FIRSTNAME?>
      <?=ADMIN_LASTNAME?>
      !
      <?php if(isset($num_system_messages) && $num_system_messages>"0"){?>
      <a href="home.php" title="<?=$num_system_messages?> new private messages from System!" class="tooltip">
      <?=$num_system_messages?>
      new messages!</a></p>
    <?php } ?>
    <ul>
      <li><a href="logout.php" title="" class="tooltip"><span class="ui-icon ui-icon-power"></span>Logout</a></li>
      <li><a href="settings.php" title="" class="tooltip"><span class="ui-icon ui-icon-wrench"></span>Site Settings</a></li>
      <!--<li><a href="account.php" title="" class="tooltip"><span class="ui-icon ui-icon-person"></span>My account</a></li>-->
    </ul>
  </div>
  <!-- End of Meta information -->
</div>
