Information about manager panel is as follows

1) all functions are stored under the folder manager/functions

a) you need to create a file foo.php and put foo() under it... it will auto include everywhere in the manager and on the frontend of the website by only include the functions.php which is mother file.

2) config.php can be changed for connection settings

3) site-details.php also works as common include file.

4) forgot-password.php is also a default file of the manager panel.

5) settings-body.php, this file can be changed according to customer requirements, for example if a customer want to save a tax value (constant for the whole site)

a) you will add a new column in the database table ADMIN for tax

and to create a CONSTANT for tax, you will update site-details.php

for including js,css, files, you can update head-include.php

account-body.php is for managing the details of admin email and password.

footer.php is a default file.

messages-display.php is a default file

header-right.php is a default file.

login, authenticate, etc are default files.

 

How to add a new module to manager panel

You can edit navigation.php and put your own links.

tinymce (cms) pages are already included in this admin panel.

email templates are also already included.

And i have more modules arleady written that i can share with you.

activities
cms
employees
export-csv
faq
headers
pdf
sponsors

etc

etc.

For most of the projects, we already deliver you the code.

We believe that you are an excellent programmer and can write very good code, but we still strive for keeping all the basic modules in only 1 repository/vault.

Writing code seperately causes troubles.

I appreciate the programmers who mention OOP, FRAMEWORK in their CVs, but we are still keeping wolfweb's projects in linear php.

for creating a new module (for example employees) you can follow the following simple steps.

1) edit navigation.php and add a tab for employees

2) put a file employees.php under the folder manager (this file is used as template file)

3) put another include file employees-body.php, and put your php work in it.
a) write php to insert/update database
b) write html for form elements (input/text/select/radio etc)

4) create database table for employees
a) a post fix of _posts like employees_posts represents it to be a master table

 

5) update db-tables.php for the new table employees, create a constant for table name

and rest of the

 

 

Don�t forget to get tips for how to merge two admins on one client's website.