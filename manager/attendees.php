<?php

include_once("conn.php");

include_once("db-tables.php");

include_once("site-details.php");

include_once("functions.php");

include_once("authenticate.php");

?>

<!DOCTYPE html>

<html>

<head>

<?php include"head-include.php";?>
<script type="text/javascript">
function updateDates()
{
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	var dates = xmlhttp.responseText;
    document.getElementById("dates").value=dates;

    }
  }

var class_id = document.getElementById("class_id").options[document.getElementById("class_id").selectedIndex].value;

xmlhttp.open("GET","attendees-ajax-dates.php?class_id="+class_id,true);
xmlhttp.send();
}
function updatePrice()
{
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	var price = xmlhttp.responseText;
	var subTotal = price*1;
    document.getElementById("subtotal").value= subTotal.toFixed(2);
	var taxRate = <?=SALES_TAX_RATE?>;
	var taxValue = price*taxRate/100;
	document.getElementById("sales_tax").value=taxValue.toFixed(2);
	var totalPrice = +price + +taxValue;
    document.getElementById("total_price").value=totalPrice.toFixed(2);

    }
  }

var number_of_people = document.getElementById("number_of_people").value;
var class_id = document.getElementById("class_id").options[document.getElementById("class_id").selectedIndex].value;

xmlhttp.open("GET","attendees-ajax-price.php?number_of_people="+number_of_people+"&class_id="+class_id,true);
xmlhttp.send();
}
</script>
</head>

<body id="top">

<!-- Container -->

<div id="container">

  <!-- Header -->

  <div id="header">

    <!-- Top -->

    <?php include"header-right.php";?>

    <!-- End of Top-->

    <!-- The navigation bar -->

    <div id="navbar">

      <?php include"navigation.php";?>

    </div>

    <!-- End of navigation bar" -->

  </div>

  <!-- End of Header -->

  <!-- Background wrapper -->

  <div id="bgwrap">

    <!-- Main Content -->

    <div id="content">

      <div id="main">

        <?php

			include"attendees-body.php";

		?>

      </div>

    </div>

    <!-- End of Main Content -->

  </div>

  <!-- End of bgwrap -->

</div>

<!-- End of Container -->

<!-- Footer -->

<div id="footer">

  <?php include"footer.php";?>

</div>

<!-- End of Footer -->

</body>

</html>

