<?php
// image_functions.php

ini_set('max_execution_time', '600');
ini_set('max_input_time', '600');
ini_set('post_max_size','80M');
ini_set('upload_max_filesize','20M');
ini_set('memory_limit', '160M');

//resize the pic
function picResize($width, $height, $target) 
{ 
if ($width > $height) 
{ 
	$percentage = ($target / $width); 
} 
else 
{ 
	$percentage = ($target / $height); 
} 
$width = round($width * $percentage); 
$height = round($height * $percentage); 
return array($width,$height); 
} 

//function to create thumbnail of image
function createThumbnail($srcFolder,$srcFile,$dest,$thmbWidth,$thmbHeight,$base,$new_name_of_file='')
{
//$src = source 
//$dest = destination
//$thmbWidth = thumbnail width
//$thmbHeight = thumbnail height
//$base = height OR weigth that will be used for proportional resizing
//$new_name_of_file = new name of thumbnail that will be created, just pass the filename with no extension
	
		 
	//allowed extensions
	$extension_array[] = "jpg";
	$extension_array[] = "jpeg";
	$extension_array[] = "gif";
	$extension_array[] = "png";
	$extension_array[] = "bmp";
	
	//check if source exists
	if((!file_exists($srcFolder.'/'.$srcFile)) || (!is_file($srcFolder.'/'.$srcFile) == true))
	{
		return "Source doesn't exists";
	}
	//get file extension
	$ext = getfileExtension($srcFile);
	//check if souce is valid for thumbnail creation
	if(!in_array(strtolower($ext),$extension_array))
	{
		return "Only image files are allowed";
	}
	//check if destination exists
	if(!file_exists($dest))
	{
		return "Destination doesn't exists";
	}
	//check if destination is writable
	if(!is_writable($dest))
	{
		return "Destination is not writable";
	}
	//check if width is numeric
	if(!is_numeric($thmbWidth))
	{
		return "thumb width should be numeric";
	}
	//check if height is numeric
	if(!is_numeric($thmbHeight))
	{
		return "thumb height should be numeric";
	}
	//get the image with and height of main image
	$mysock = getimagesize($srcFolder.'/'.$srcFile);
	//resize image
	if($mysock[0] > $thmbWidth || $mysock[1] > $thmbHeight)
	{
		if($base == "w")
		{
		$resizeBy = $thmbWidth;
		}
		elseif($base == "h")
		{
		$resizeBy = $thmbHeight;
		}
		else
		{
			if($mysock[0] > $mysock[1])
			{
				$resizeBy = $thmbWidth;
			}
			else
			{
				$resizeBy = $thmbHeight;
			}
		}
		$new_size=picResize($mysock[0],$mysock[1], $resizeBy);
		$newWidth = $new_size[0];
		$newHeight = $new_size[1];
		
	}
	else
	{
		$newWidth=$mysock[0];
		$newHeight=$mysock[1];
	}
	
	//create thumbnail
	if(empty($new_name_of_file))
	{
	$thName = $dest."/".$srcFile;
	}
	else
	{
	$thName = $dest."/".$new_name_of_file.'.'.$ext;
	}
	
	if($ext == 'gif')
	{
	$mainimage = imagecreatefromgif($srcFolder.'/'.$srcFile);
	}
	elseif($ext == 'png')
	{
	$mainimage = imagecreatefrompng($srcFolder.'/'.$srcFile);
	}
	elseif($ext == 'bmp')
	{
	$mainimage = imagecreatefrombmp($srcFolder.'/'.$srcFile);
	}
	else
	{
	$mainimage = imagecreatefromjpeg($srcFolder.'/'.$srcFile);
	}
	
	$mythumbnail = imagecreatetruecolor($newWidth,$newHeight);
	$mainwidth = imagesx($mainimage);
	$mainheight = imagesy($mainimage);
	imagecopyresampled($mythumbnail,$mainimage,0,0,0,0,$newWidth,$newHeight,$mainwidth,$mainheight);
	if($ext == 'gif')
	{
	ImageGIF($mythumbnail,$thName,75);
	}
	else
	{
	ImageJPEG($mythumbnail,$thName,75);
	}
	
	imagedestroy($mythumbnail);
	imagedestroy($mainimage);	
	
	// return 1 on success
	return 1;

}
function imagecreatefrombmp($filename) {
 $tmp_name = tempnam("/tmp", "GD");
 if(ConvertBMP2GD($filename, $tmp_name)) {
  $img = imagecreatefromgd($tmp_name);
  unlink($tmp_name);
  return $img;
 } return false;
} 
function ConvertBMP2GD($src, $dest = false) {
 if(!($src_f = fopen($src, "rb"))) {
  return false;
 }
 if(!($dest_f = fopen($dest, "wb"))) {
  return false;
 }
 $header = unpack("vtype/Vsize/v2reserved/Voffset", fread($src_f,
14));
 $info = unpack("Vsize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vncolor/Vimportant",
fread($src_f, 40));
 
 extract($info);
 extract($header);

 if($type != 0x4D42) { // signature "BM"
  return false;
 }

 $palette_size = $offset - 54;
 $ncolor = $palette_size / 4;
 $gd_header = "";
 // true-color vs. palette
 $gd_header .= ($palette_size == 0) ? "\xFF\xFE" : "\xFF\xFF";
 $gd_header .= pack("n2", $width, $height);
 $gd_header .= ($palette_size == 0) ? "\x01" : "\x00";
 if($palette_size) {
  $gd_header .= pack("n", $ncolor);
 }
 // no transparency
 $gd_header .= "\xFF\xFF\xFF\xFF";

 fwrite($dest_f, $gd_header);

 if($palette_size) {
  $palette = fread($src_f, $palette_size);
  $gd_palette = "";
  $j = 0;
  while($j < $palette_size) {
   $b = $palette{$j++};
   $g = $palette{$j++};
   $r = $palette{$j++};
   $a = $palette{$j++};
   $gd_palette .= "$r$g$b$a";
  }
  $gd_palette .= str_repeat("\x00\x00\x00\x00", 256 - $ncolor);
  fwrite($dest_f, $gd_palette);
 }

 $scan_line_size = (($bits * $width) + 7) >> 3;
 $scan_line_align = ($scan_line_size & 0x03) ? 4 - ($scan_line_size &
0x03) : 0;

 for($i = 0, $l = $height - 1; $i < $height; $i++, $l--) {
  // BMP stores scan lines starting from bottom
  fseek($src_f, $offset + (($scan_line_size + $scan_line_align) *
$l));
  $scan_line = fread($src_f, $scan_line_size);
  if($bits == 24) {
   $gd_scan_line = "";
   $j = 0;
   while($j < $scan_line_size) {
    $b = $scan_line{$j++};
    $g = $scan_line{$j++};
    $r = $scan_line{$j++};
    $gd_scan_line .= "\x00$r$g$b";
   }
  }
  else if($bits == 8) {
   $gd_scan_line = $scan_line;
  }
  else if($bits == 4) {
   $gd_scan_line = "";
   $j = 0;
   while($j < $scan_line_size) {
    $byte = ord($scan_line{$j++});
    $p1 = chr($byte >> 4);
    $p2 = chr($byte & 0x0F);
    $gd_scan_line .= "$p1$p2";
   } $gd_scan_line = substr($gd_scan_line, 0, $width);
  }
  else if($bits == 1) {
   $gd_scan_line = "";
   $j = 0;
   while($j < $scan_line_size) {
    $byte = ord($scan_line{$j++});
    $p1 = chr((int) (($byte & 0x80) != 0));
    $p2 = chr((int) (($byte & 0x40) != 0));
    $p3 = chr((int) (($byte & 0x20) != 0));
    $p4 = chr((int) (($byte & 0x10) != 0));
    $p5 = chr((int) (($byte & 0x08) != 0));
    $p6 = chr((int) (($byte & 0x04) != 0));
    $p7 = chr((int) (($byte & 0x02) != 0));
    $p8 = chr((int) (($byte & 0x01) != 0));
    $gd_scan_line .= "$p1$p2$p3$p4$p5$p6$p7$p8";
   } $gd_scan_line = substr($gd_scan_line, 0, $width);
  }
    
  fwrite($dest_f, $gd_scan_line);
 }
 fclose($src_f);
 fclose($dest_f);
 return true;
} 
// thumb nail function ends here

//display image
function displayImage($folder,$file,$alt='',$class='',$url='',$extra='')
{
$str="";
	if((!empty($folder)) && (!empty($file)))
	{
		if(file_exists($folder."/".$file) && strlen($file) > 2) 
		{
			if(!empty($url))
			{
			$str = $str .'<a href="'.$url.'"';
			}
			if(!empty($extra))
			{
			$str = $str . " $extra ";
			}
			if(!empty($url))
			{
			$str = $str .'>';
			}
			$str = $str .'<img src="'.$folder."/".$file.'"';
			if(!empty($alt))
			{
			$str = $str .' alt="'.$alt.'"';
			}
			if(!empty($class))
			{
			$str = $str .' class="'.$class.'"';
			}
			$str = $str .'>';
			if(!empty($url))
			{
			$str = $str .'</a>';
			}
			return $str; 
					
		}
	}	
}
?>