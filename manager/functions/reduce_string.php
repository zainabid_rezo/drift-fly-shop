<?php
function reduce_string($string, $length='100'){
	if(strlen($string)>$length){
		return substr($string, 0, $length).'...';
	}else{
		return $string;
	}
}
?>